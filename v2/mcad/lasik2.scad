use <batholder.scad>
use <detector.scad>
$fn = 50;

pcb_length = 86.8;
pcb_width = 28;
pcb_thickness = 2.4;

module board() {
    color("darkgreen")
    cube([pcb_length,pcb_width,pcb_thickness]);
    translate([5,2.6,pcb_thickness])
    batholder();
}
module sensor_part() {
    color("lightgray")
    translate([0,-3.5,0]) rotate([90,0,-90]) mirror([1,0,0]) detector();
}


module case_front(){
    difference() {
        translate([-21.5,-3,0]) cube([pcb_length + 22.5, pcb_width + 6 , 28]);

        translate([0,0,1]) {
            sensor_part();
            linear_extrude(height=13) projection() sensor_part();

            translate([0,0,6]) board(); 

            translate([-22,1.5,0])
            cube([pcb_length + 22,pcb_width-3,26]);

            translate([pcb_length - 5,1.5,0])
            cube([10,pcb_width-3,6]);
        }

        for (x = [5, 20, 35, 50, 65]) {
            translate([x,-2,-2])
            translate([0,-3,0]) sphere(r = 5);

            translate([x,2,-2])
            translate([0,pcb_width+3,0]) sphere(r = 5);

            translate([x,-2,+2])
            translate([0,-3,28]) sphere(r = 5);

            translate([x,2,+2])
            translate([0,pcb_width+3,28]) sphere(r = 5);
        }
    }
}




/*
translate([0,0,1]) {
    translate([0,0,6]) board();
    sensor_part();
}
case_front();

translate([0,100,1]) {
    translate([0,0,6]) board();
    sensor_part();
}
*/

translate([0,-100,0])
case_front();
