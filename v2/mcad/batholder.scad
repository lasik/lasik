$fn = 50;

module batholder() {
    width = 20.8;
    height = 14.95;
    length = 76.95;

    soldertab_w = 6.5;
    soldertab_h = 0.4;
    soldertab_l = 4.5;

    cell_r = 18.2 / 2;
    cell_l = 65;

    color("gray")
    difference(){
        cube([length, width, height]);

        translate([length/2 - 26.5/2,-1,7]) cube([26.5, 30, 20]);

        translate([4,-1,7]) cube([8.5, 30, 20]);
        translate([-1,(width - 3.8)/2,-1]) cube([6, 3.8, 30]);

        translate([length,0,0]) mirror([1,0,0]) {
            translate([4,-1,7]) cube([8.5, 30, 20]);
            translate([-1,(width - 3.8)/2,-1]) cube([6, 3.8, 30]);
        }

        translate([-1, -12.5, 0]) rotate([-15,0,0]) cube([length + 2, 10, 30]);

        mirror([0,1,0]) translate([0,-width,0])
        translate([-1, -12.5, 0]) rotate([-15,0,0]) cube([length + 2, 10, 30]);

        translate([0,width/2,0])
        translate([(length - cell_l)/2,0,cell_r]) rotate([0,90,0])
        cylinder(h=cell_l, r = cell_r + 0.1);

        translate([(length - cell_l)/2,(width-8)/2,-1])
        cube([cell_l, 8, 30]);
    }

    color("lightgray") {
        translate([0,width/2,0])
        translate([(length - cell_l)/2,0,cell_r]) rotate([0,90,0])
        cylinder(h=cell_l, r = cell_r);
    }

    color("gold") {
        translate([-soldertab_l, (width - soldertab_w)/2, 0])
        cube([soldertab_l+5, soldertab_w, soldertab_h]);

        translate([4,(width - soldertab_w)/2,0])
        cube([1, soldertab_w, height]);

        translate([length,0,0]) mirror([1,0,0]) {
            translate([-soldertab_l, (width - soldertab_w)/2, 0])
            cube([soldertab_l+5, soldertab_w, soldertab_h]);
            translate([4,(width - soldertab_w)/2,0])
            cube([1, soldertab_w, height]);
        }
    }
}

batholder();
