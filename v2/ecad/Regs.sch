EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4300 3650 0    50   Input ~ 0
V_analog
$Comp
L Regulator_Linear:TLV75533PDBV U1201
U 1 1 5C608E61
P 5050 3750
F 0 "U1201" H 5050 4092 50  0000 C CNN
F 1 "MIC94310" H 5050 4001 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5050 4050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MIC94310.pdf" H 5050 3750 50  0001 C CNN
	1    5050 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6550 3650 6250 3650
Wire Wire Line
	6250 3600 6250 3650
Connection ~ 6250 3650
Wire Wire Line
	6250 3650 5850 3650
$Comp
L Device:C_Small C?
U 1 1 5C609713
P 7350 3850
AR Path="/5C5E4922/5C609713" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609713" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609713" Ref="C?"  Part="1" 
AR Path="/5C95D17D/5C609713" Ref="C1204"  Part="1" 
F 0 "C1204" H 7050 3950 50  0000 L CNN
F 1 "4u7" H 7150 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7350 3850 50  0001 C CNN
F 3 "~" H 7350 3850 50  0001 C CNN
F 4 "" H 2550 650 50  0001 C CNN "Note"
	1    7350 3850
	-1   0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5C60971A
P 6250 4150
AR Path="/5C5E4922/5C60971A" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C60971A" Ref="#PWR?"  Part="1" 
AR Path="/5C5FC5C6/5C60971A" Ref="#PWR?"  Part="1" 
AR Path="/5C95D17D/5C60971A" Ref="#PWR01202"  Part="1" 
F 0 "#PWR01202" H 6250 3900 50  0001 C CNN
F 1 "Earth" H 6250 4000 50  0001 C CNN
F 2 "" H 6250 4150 50  0001 C CNN
F 3 "~" H 6250 4150 50  0001 C CNN
	1    6250 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 3750 7350 3650
$Comp
L Device:C_Small C?
U 1 1 5C609CA5
P 6250 3850
AR Path="/5C5E4922/5C609CA5" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609CA5" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609CA5" Ref="C?"  Part="1" 
AR Path="/5C95D17D/5C609CA5" Ref="C1203"  Part="1" 
F 0 "C1203" H 5950 3950 50  0000 L CNN
F 1 "4u7" H 6050 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6250 3850 50  0001 C CNN
F 3 "~" H 6250 3850 50  0001 C CNN
F 4 "" H 400 650 50  0001 C CNN "Note"
	1    6250 3850
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C609CED
P 4700 3850
AR Path="/5C5E4922/5C609CED" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609CED" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609CED" Ref="C?"  Part="1" 
AR Path="/5C95D17D/5C609CED" Ref="C1201"  Part="1" 
F 0 "C1201" H 4450 3750 50  0000 L CNN
F 1 "4u7" H 4500 3950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4700 3850 50  0001 C CNN
F 3 "~" H 4700 3850 50  0001 C CNN
F 4 "" H -1900 600 50  0001 C CNN "Note"
	1    4700 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	6250 3750 6250 3650
Text HLabel 7800 3650 2    50   Input ~ 0
V_in
Wire Wire Line
	4300 3650 4700 3650
Wire Wire Line
	4700 4100 4700 3950
Wire Wire Line
	4700 3750 4700 3650
Connection ~ 4700 3650
Wire Wire Line
	4700 3650 4750 3650
Wire Wire Line
	7350 3650 7800 3650
Connection ~ 7350 3650
$Comp
L Device:C_Small C?
U 1 1 5C68B5E3
P 5850 3850
AR Path="/5C5E4922/5C68B5E3" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C68B5E3" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C68B5E3" Ref="C?"  Part="1" 
AR Path="/5C95D17D/5C68B5E3" Ref="C1202"  Part="1" 
F 0 "C1202" H 5550 3950 50  0000 L CNN
F 1 "4u7" H 5650 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5850 3850 50  0001 C CNN
F 3 "~" H 5850 3850 50  0001 C CNN
F 4 "" H 0   650 50  0001 C CNN "Note"
	1    5850 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 3950 5850 4100
Wire Wire Line
	5850 3750 5850 3650
Wire Wire Line
	5850 4100 6250 4100
$Comp
L power:+3V3 #PWR01201
U 1 1 5C95D93C
P 6250 3600
F 0 "#PWR01201" H 6250 3450 50  0001 C CNN
F 1 "+3V3" H 6265 3773 50  0000 C CNN
F 2 "" H 6250 3600 50  0001 C CNN
F 3 "" H 6250 3600 50  0001 C CNN
	1    6250 3600
	-1   0    0    -1  
$EndComp
Connection ~ 5850 3650
Text HLabel 5400 3750 2    50   Input ~ 0
V_An_EN
$Comp
L Regulator_Linear:TLV75533PDBV U1202
U 1 1 5C608D05
P 6850 3750
F 0 "U1202" H 6850 4092 50  0000 C CNN
F 1 "TLV75533PDBV" H 6850 4001 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6850 4050 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv755p.pdf" H 6850 3750 50  0001 C CNN
	1    6850 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 3950 7350 4100
Wire Wire Line
	4700 4100 5050 4100
Wire Wire Line
	6250 4100 6850 4100
Connection ~ 6250 4100
Wire Wire Line
	6850 4050 6850 4100
Wire Wire Line
	6850 4100 7350 4100
Wire Wire Line
	5050 4050 5050 4100
Connection ~ 5050 4100
Wire Wire Line
	5050 4100 5850 4100
Wire Wire Line
	6250 3950 6250 4100
Wire Wire Line
	6250 4150 6250 4100
Connection ~ 6850 4100
Connection ~ 5850 4100
Wire Wire Line
	5400 3750 5350 3750
Wire Wire Line
	7150 3650 7200 3650
Wire Wire Line
	7200 3750 7150 3750
Wire Wire Line
	7200 3750 7200 3650
Connection ~ 7200 3650
Wire Wire Line
	7200 3650 7350 3650
Wire Wire Line
	5350 3650 5850 3650
$EndSCHEMATC
