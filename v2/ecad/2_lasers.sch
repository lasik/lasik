EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5600 3400 850  300 
U 5C959ED4
F0 "Laser1" 50
F1 "Laser.sch" 50
F2 "LD+" I R 6450 3500 50 
F3 "LD-" I R 6450 3600 50 
F4 "~laser" I L 5600 3550 50 
$EndSheet
Text HLabel 5500 3550 0    50   Input ~ 0
~laser1
Wire Wire Line
	5500 3550 5600 3550
$Sheet
S 5600 3900 850  300 
U 5C95A471
F0 "Laser2" 50
F1 "Laser.sch" 50
F2 "~laser" I L 5600 4050 50 
F3 "LD+" I R 6450 4000 50 
F4 "LD-" I R 6450 4100 50 
$EndSheet
Text HLabel 5500 4050 0    50   Input ~ 0
~laser2
Wire Wire Line
	5500 4050 5600 4050
Text HLabel 6550 3500 2    50   Input ~ 0
LD1+
Text HLabel 6550 3600 2    50   Input ~ 0
LD1-
Text HLabel 6550 4000 2    50   Input ~ 0
LD2+
Text HLabel 6550 4100 2    50   Input ~ 0
LD2-
Wire Wire Line
	6550 3500 6450 3500
Wire Wire Line
	6550 3600 6450 3600
Wire Wire Line
	6550 4000 6450 4000
Wire Wire Line
	6550 4100 6450 4100
$EndSCHEMATC
