EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J?
U 1 1 5C67BA90
P 3750 3600
AR Path="/5C5FC5C6/5C67BA90" Ref="J?"  Part="1" 
AR Path="/5C67B796/5C67BA90" Ref="J?"  Part="1" 
AR Path="/5C95ED49/5C67BA90" Ref="J901"  Part="1" 
F 0 "J901" H 3550 4100 50  0000 L CNN
F 1 "USB_B_Micro" H 3550 4000 50  0000 L CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 3900 3550 50  0001 C CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/1050170001_IO_CONNECTORS.pdf" H 3900 3550 50  0001 C CNN
	1    3750 3600
	1    0    0    -1  
$EndComp
Text HLabel 7400 3750 2    50   Input ~ 0
V_in
$Comp
L Battery_Management:MCP73831-2-OT U901
U 1 1 5C680F4D
P 5600 3850
F 0 "U901" H 5400 4250 50  0000 R CNN
F 1 "MCP73831" H 5500 4150 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5650 3600 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 5450 3800 50  0001 C CNN
	1    5600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4200 5600 4150
Wire Wire Line
	5600 3550 5600 3400
$Comp
L Device:R_Small R?
U 1 1 5C6816E8
P 5150 4050
AR Path="/5C5FAFFD/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C696261/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C95ED49/5C6816E8" Ref="R901"  Part="1" 
F 0 "R901" H 5000 4050 50  0000 R CNN
F 1 "2K2" H 5000 4150 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5150 4050 50  0001 C CNN
F 3 "~" H 5150 4050 50  0001 C CNN
	1    5150 4050
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C6816F9
P 5050 3800
AR Path="/5C696261/5C6816F9" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C6816F9" Ref="C?"  Part="1" 
AR Path="/5C95ED49/5C6816F9" Ref="C901"  Part="1" 
F 0 "C901" H 4700 3850 50  0000 L CNN
F 1 "4u7" H 4700 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5050 3800 50  0001 C CNN
F 3 "~" H 5050 3800 50  0001 C CNN
F 4 "" H 750 150 50  0001 C CNN "Note"
	1    5050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4200 5150 4150
Connection ~ 5150 4200
Wire Wire Line
	5150 4200 5600 4200
Wire Wire Line
	5200 3950 5150 3950
$Comp
L power:Earth #PWR0901
U 1 1 5C681C94
P 5600 4250
F 0 "#PWR0901" H 5600 4000 50  0001 C CNN
F 1 "Earth" H 5600 4100 50  0001 C CNN
F 2 "" H 5600 4250 50  0001 C CNN
F 3 "~" H 5600 4250 50  0001 C CNN
	1    5600 4250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5600 4200 5600 4250
Connection ~ 5600 4200
$Comp
L Device:Thermistor_PTC TH901
U 1 1 5C682570
P 4800 3400
F 0 "TH901" V 4500 3300 50  0000 R CNN
F 1 "Thermistor_PTC" V 4600 3600 50  0000 R CNN
F 2 "Fuse:Fuse_0805_2012Metric" H 4850 3200 50  0001 L CNN
F 3 "https://belfuse.com/resources/Datasheets/CircuitProtection/ds-CP-0zck-series.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	0    1    1    0   
$EndComp
$Comp
L Device:LED D901
U 1 1 5C682962
P 5800 3400
F 0 "D901" H 5600 3100 50  0000 C CNN
F 1 "LED" H 5550 3200 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5800 3400 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5178061/LH%20R974_EN.pdf" H 5800 3400 50  0001 C CNN
F 4 "preferably amber" H 750 150 50  0001 C CNN "Note"
	1    5800 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C682C2F
P 6050 3550
AR Path="/5C5FAFFD/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C696261/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C95ED49/5C682C2F" Ref="R902"  Part="1" 
F 0 "R902" H 6000 3600 50  0000 R CNN
F 1 "470R" H 6000 3500 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6050 3550 50  0001 C CNN
F 3 "~" H 6050 3550 50  0001 C CNN
	1    6050 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6050 3450 6050 3400
Wire Wire Line
	6050 3400 5950 3400
$Comp
L Device:C_Small C?
U 1 1 5C6836EF
P 6200 3900
AR Path="/5C696261/5C6836EF" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C6836EF" Ref="C?"  Part="1" 
AR Path="/5C95ED49/5C6836EF" Ref="C902"  Part="1" 
F 0 "C902" H 6400 4150 50  0000 L CNN
F 1 "4u7" H 6250 4050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6200 3900 50  0001 C CNN
F 3 "~" H 6200 3900 50  0001 C CNN
F 4 "" H 750 150 50  0001 C CNN "Note"
	1    6200 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 3800 6200 3750
Connection ~ 6200 3750
Wire Wire Line
	6200 3750 6000 3750
$Comp
L Device:D_Schottky D902
U 1 1 5C686FD9
P 6950 3250
F 0 "D902" H 6800 3050 50  0000 C CNN
F 1 "D_Schottky" H 6750 3150 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6950 3250 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NSR0340H-D.PDF" H 6950 3250 50  0001 C CNN
	1    6950 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 3450 6650 3250
Wire Wire Line
	6650 3250 6800 3250
Wire Wire Line
	7100 3250 7250 3250
Wire Wire Line
	7250 3250 7250 3750
Wire Wire Line
	7250 3750 6850 3750
Wire Wire Line
	7400 3750 7250 3750
Connection ~ 7250 3750
Connection ~ 6650 3250
$Comp
L Device:R_Small R?
U 1 1 5C689860
P 6650 3050
AR Path="/5C5FAFFD/5C689860" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C689860" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C689860" Ref="R?"  Part="1" 
AR Path="/5C696261/5C689860" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C689860" Ref="R?"  Part="1" 
AR Path="/5C95ED49/5C689860" Ref="R903"  Part="1" 
F 0 "R903" H 6550 3100 50  0000 R CNN
F 1 "4K7" H 6550 3000 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6650 3050 50  0001 C CNN
F 3 "~" H 6650 3050 50  0001 C CNN
	1    6650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3400 5050 3400
Wire Wire Line
	4650 3400 4200 3400
Wire Wire Line
	3750 4000 3750 4050
Wire Wire Line
	3750 4200 4550 4200
$Sheet
S 6200 4300 200  950 
U 5C68A220
F0 "Protected_cell" 50
F1 "Protected_cell.sch" 50
F2 "V_bat" I T 6300 4300 50 
$EndSheet
Wire Wire Line
	6200 3750 6300 3750
Wire Wire Line
	5600 4200 6200 4200
Wire Wire Line
	6200 4000 6200 4200
Wire Wire Line
	6300 3750 6300 4200
Connection ~ 6300 3750
Wire Wire Line
	6300 3750 6450 3750
$Comp
L Device:Q_PMOS_GSD Q901
U 1 1 5C69506F
P 6650 3650
F 0 "Q901" V 6900 3800 50  0000 C CNN
F 1 "DMP3099L-7" V 7000 3650 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6850 3750 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMP3099L.pdf" H 6650 3650 50  0001 C CNN
	1    6650 3650
	0    -1   1    0   
$EndComp
Wire Wire Line
	5050 3700 5050 3400
Connection ~ 5050 3400
Wire Wire Line
	5050 3400 5600 3400
Wire Wire Line
	5050 3900 5050 4200
Connection ~ 5050 4200
Wire Wire Line
	5050 4200 5150 4200
Wire Wire Line
	3650 4000 3650 4050
Wire Wire Line
	3650 4050 3750 4050
Connection ~ 3750 4050
Wire Wire Line
	3750 4050 3750 4200
Wire Wire Line
	5600 3400 5600 3250
Connection ~ 5600 3400
Wire Wire Line
	5650 3400 5600 3400
Wire Wire Line
	6000 3950 6050 3950
Wire Wire Line
	5600 3250 6650 3250
$Comp
L power:Earth #PWR0902
U 1 1 5C6A4A03
P 6800 2950
F 0 "#PWR0902" H 6800 2700 50  0001 C CNN
F 1 "Earth" H 6800 2800 50  0001 C CNN
F 2 "" H 6800 2950 50  0001 C CNN
F 3 "~" H 6800 2950 50  0001 C CNN
	1    6800 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6650 2950 6650 2900
Wire Wire Line
	6650 2900 6800 2900
Wire Wire Line
	6800 2900 6800 2950
Wire Wire Line
	6650 3150 6650 3250
Wire Wire Line
	6050 3650 6050 3950
NoConn ~ 4050 3800
NoConn ~ 4050 3700
NoConn ~ 4050 3600
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5CCFAD4D
P 4550 3800
AR Path="/5C67B796/5C68A220/5CCFAD4D" Ref="J?"  Part="1" 
AR Path="/5C67B796/5CCFAD4D" Ref="J?"  Part="1" 
AR Path="/5C95ED49/5CCFAD4D" Ref="J902"  Part="1" 
F 0 "J902" V 4750 3200 50  0000 C CNN
F 1 "Conn_01x02_Male" V 4850 3400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 3800 50  0001 C CNN
F 3 "~" H 4550 3800 50  0001 C CNN
	1    4550 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 4000 4550 4200
Connection ~ 4550 4200
Wire Wire Line
	4550 4200 5050 4200
Wire Wire Line
	4450 4000 4450 4050
Wire Wire Line
	4450 4050 4200 4050
Wire Wire Line
	4200 4050 4200 3400
Connection ~ 4200 3400
Wire Wire Line
	4200 3400 4050 3400
Text Notes 4550 2650 0    50   ~ 0
Caution!\nthe marking for the red LED is the reverse of the green model!
$Comp
L Device:R_Small R?
U 1 1 5C961367
P 7250 4350
AR Path="/5C5FAFFD/5C961367" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C961367" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C961367" Ref="R?"  Part="1" 
AR Path="/5C696261/5C961367" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C961367" Ref="R?"  Part="1" 
AR Path="/5C95ED49/5C961367" Ref="R904"  Part="1" 
F 0 "R904" H 7200 4400 50  0000 R CNN
F 1 "47K" H 7200 4300 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7250 4350 50  0001 C CNN
F 3 "~" H 7250 4350 50  0001 C CNN
	1    7250 4350
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C961443
P 7250 4650
AR Path="/5C5FAFFD/5C961443" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C961443" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C961443" Ref="R?"  Part="1" 
AR Path="/5C696261/5C961443" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C961443" Ref="R?"  Part="1" 
AR Path="/5C95ED49/5C961443" Ref="R905"  Part="1" 
F 0 "R905" H 7200 4700 50  0000 R CNN
F 1 "47K" H 7200 4600 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7250 4650 50  0001 C CNN
F 3 "~" H 7250 4650 50  0001 C CNN
	1    7250 4650
	1    0    0    1   
$EndComp
$Comp
L power:Earth #PWR0903
U 1 1 5C96147B
P 7250 4800
F 0 "#PWR0903" H 7250 4550 50  0001 C CNN
F 1 "Earth" H 7250 4650 50  0001 C CNN
F 2 "" H 7250 4800 50  0001 C CNN
F 3 "~" H 7250 4800 50  0001 C CNN
	1    7250 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7250 4550 7250 4500
Text HLabel 7400 4500 2    50   Input ~ 0
V_sense
Wire Wire Line
	7400 4500 7250 4500
Connection ~ 7250 4500
Wire Wire Line
	7250 4500 7250 4450
Wire Wire Line
	7250 4750 7250 4800
Wire Wire Line
	7250 4250 7250 4200
Wire Wire Line
	7250 4200 6300 4200
Connection ~ 6300 4200
Wire Wire Line
	6300 4200 6300 4300
$EndSCHEMATC
