EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5C6DF1A5
P 5050 3650
AR Path="/5C5E4922/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C96617D/5C6DF1A5" Ref="R1101"  Part="1" 
F 0 "R1101" H 5250 3650 50  0000 C CNN
F 1 "470R" H 5250 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5050 3650 50  0001 C CNN
F 3 "~" H 5050 3650 50  0001 C CNN
	1    5050 3650
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF1AD
P 5050 3950
AR Path="/5C6DF1AD" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF1AD" Ref="D?"  Part="1" 
AR Path="/5C96617D/5C6DF1AD" Ref="D1101"  Part="1" 
F 0 "D1101" V 5088 3832 50  0000 R CNN
F 1 "LED" V 4997 3832 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5050 3950 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 5050 3950 50  0001 C CNN
F 4 "preferably amber" H -1100 -650 50  0001 C CNN "Note"
	1    5050 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 3800 5050 3750
$Comp
L Device:R_Small R?
U 1 1 5C6DF221
P 5550 3650
AR Path="/5C5E4922/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C96617D/5C6DF221" Ref="R1102"  Part="1" 
F 0 "R1102" H 5750 3650 50  0000 C CNN
F 1 "470R" H 5750 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5550 3650 50  0001 C CNN
F 3 "~" H 5550 3650 50  0001 C CNN
	1    5550 3650
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF228
P 5550 3950
AR Path="/5C6DF228" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF228" Ref="D?"  Part="1" 
AR Path="/5C96617D/5C6DF228" Ref="D1102"  Part="1" 
F 0 "D1102" V 5588 3832 50  0000 R CNN
F 1 "LED" V 5497 3832 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5550 3950 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 5550 3950 50  0001 C CNN
F 4 "preferably amber" H -600 -650 50  0001 C CNN "Note"
	1    5550 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3800 5550 3750
$Comp
L Device:R_Small R?
U 1 1 5C6DF32A
P 6050 3650
AR Path="/5C5E4922/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C96617D/5C6DF32A" Ref="R1103"  Part="1" 
F 0 "R1103" H 6250 3650 50  0000 C CNN
F 1 "470R" H 6250 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6050 3650 50  0001 C CNN
F 3 "~" H 6050 3650 50  0001 C CNN
	1    6050 3650
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF331
P 6050 3950
AR Path="/5C6DF331" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF331" Ref="D?"  Part="1" 
AR Path="/5C96617D/5C6DF331" Ref="D1103"  Part="1" 
F 0 "D1103" V 6088 3832 50  0000 R CNN
F 1 "LED" V 5997 3832 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6050 3950 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 6050 3950 50  0001 C CNN
F 4 "preferably amber" H -100 -650 50  0001 C CNN "Note"
	1    6050 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3800 6050 3750
$Comp
L Device:R_Small R?
U 1 1 5C6DF338
P 6550 3650
AR Path="/5C5E4922/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C96617D/5C6DF338" Ref="R1104"  Part="1" 
F 0 "R1104" H 6750 3650 50  0000 C CNN
F 1 "470R" H 6750 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6550 3650 50  0001 C CNN
F 3 "~" H 6550 3650 50  0001 C CNN
	1    6550 3650
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF33F
P 6550 3950
AR Path="/5C6DF33F" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF33F" Ref="D?"  Part="1" 
AR Path="/5C96617D/5C6DF33F" Ref="D1104"  Part="1" 
F 0 "D1104" V 6588 3832 50  0000 R CNN
F 1 "LED" V 6497 3832 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6550 3950 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 6550 3950 50  0001 C CNN
F 4 "preferably amber" H 400 -650 50  0001 C CNN "Note"
	1    6550 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 3800 6550 3750
Wire Wire Line
	6550 4100 6550 4200
Wire Wire Line
	6050 4100 6050 4200
Connection ~ 6050 4200
Wire Wire Line
	6050 4200 6550 4200
Wire Wire Line
	5550 4100 5550 4200
Connection ~ 5550 4200
Wire Wire Line
	5550 4200 5800 4200
Wire Wire Line
	5050 4100 5050 4200
Wire Wire Line
	5050 4200 5550 4200
Text HLabel 6550 3500 1    50   Input ~ 0
LED4
Text HLabel 6050 3500 1    50   Input ~ 0
LED3
Text HLabel 5550 3500 1    50   Input ~ 0
LED2
Text HLabel 5050 3500 1    50   Input ~ 0
LED1
Wire Wire Line
	5050 3500 5050 3550
Wire Wire Line
	5550 3500 5550 3550
Wire Wire Line
	6050 3500 6050 3550
Wire Wire Line
	6550 3500 6550 3550
Text Notes 4750 4500 0    50   ~ 0
Caution!\nthe marking for the green LEDs are the reverse of the red model!.
$Comp
L power:Earth #PWR01101
U 1 1 5C966357
P 5800 4250
F 0 "#PWR01101" H 5800 4000 50  0001 C CNN
F 1 "Earth" H 5800 4100 50  0001 C CNN
F 2 "" H 5800 4250 50  0001 C CNN
F 3 "~" H 5800 4250 50  0001 C CNN
	1    5800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4250 5800 4200
Connection ~ 5800 4200
Wire Wire Line
	5800 4200 6050 4200
$EndSCHEMATC
