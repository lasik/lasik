EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4300 3450 4250 3450
$Comp
L Device:Opamp_Dual_Generic U?
U 2 1 5C635271
P 6150 3800
AR Path="/5C5FAFFD/5C635271" Ref="U?"  Part="2" 
AR Path="/5C5E4922/5C635271" Ref="U?"  Part="2" 
AR Path="/5C62C3B9/5C635271" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DA8/5C635271" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DAE/5C635271" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662123/5C635271" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C666787/5C662123/5C635271" Ref="U?"  Part="2" 
AR Path="/5C666787/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C689EFA/5C662123/5C635271" Ref="U?"  Part="2" 
AR Path="/5C689EFA/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C635271" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A301/5C662123/5C635271" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A301/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C94BC35/5C662123/5C635271" Ref="U301"  Part="2" 
AR Path="/5C94BC35/5C662129/5C635271" Ref="U?"  Part="2" 
AR Path="/5C94BC35/5C9582F9/5C635271" Ref="U401"  Part="2" 
F 0 "U301" H 6250 4000 50  0000 C CNN
F 1 "TLV9062" H 6250 4100 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6150 3800 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 6150 3800 50  0001 C CNN
	2    6150 3800
	1    0    0    1   
$EndComp
$Comp
L Device:Opamp_Dual_Generic U?
U 1 1 5C63526A
P 4650 3900
AR Path="/5C5FAFFD/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C5E4922/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C62C3B9/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C662123/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C666787/5C662123/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C666787/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C63526A" Ref="U301"  Part="1" 
AR Path="/5C94BC35/5C662129/5C63526A" Ref="U?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C63526A" Ref="U401"  Part="1" 
F 0 "U301" H 4700 4100 50  0000 C CNN
F 1 "TLV9062" H 4700 4200 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4650 3900 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 4650 3900 50  0001 C CNN
	1    4650 3900
	1    0    0    1   
$EndComp
Wire Wire Line
	7750 3050 7750 3100
Wire Wire Line
	5850 3350 5700 3350
Wire Wire Line
	5850 3350 5850 3700
Connection ~ 5850 3350
Wire Wire Line
	6100 3350 5850 3350
Wire Wire Line
	6500 3800 6600 3800
$Comp
L Device:R_Small R?
U 1 1 5C648148
P 5600 3350
AR Path="/5C5E4922/5C648148" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C648148" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C648148" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C648148" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C648148" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C648148" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C648148" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C648148" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C648148" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C648148" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C648148" Ref="R304"  Part="1" 
AR Path="/5C94BC35/5C662129/5C648148" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C648148" Ref="R404"  Part="1" 
F 0 "R304" V 5500 3350 50  0000 C CNN
F 1 "4K7" V 5700 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5600 3350 50  0001 C CNN
F 3 "~" H 5600 3350 50  0001 C CNN
	1    5600 3350
	0    -1   1    0   
$EndComp
Wire Wire Line
	5250 3900 5350 3900
Wire Wire Line
	7750 3350 7750 3300
Wire Wire Line
	7100 3350 7100 3600
Wire Wire Line
	7750 3350 7100 3350
Wire Wire Line
	7750 3700 7850 3700
Wire Wire Line
	7750 3600 7750 3700
Connection ~ 7750 3350
Wire Wire Line
	7750 3400 7750 3350
$Comp
L Device:R_Small R?
U 1 1 5C625DBF
P 7750 3200
AR Path="/5C5E4922/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C625DBF" Ref="R307"  Part="1" 
AR Path="/5C94BC35/5C662129/5C625DBF" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C625DBF" Ref="R407"  Part="1" 
F 0 "R307" H 7950 3250 50  0000 C CNN
F 1 "820R" H 7950 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7750 3200 50  0001 C CNN
F 3 "~" H 7750 3200 50  0001 C CNN
	1    7750 3200
	1    0    0    -1  
$EndComp
Connection ~ 6500 3800
Wire Wire Line
	6450 3800 6500 3800
Connection ~ 7750 3700
Wire Wire Line
	7700 3700 7750 3700
$Comp
L Comparator:LM393 U?
U 2 1 5C5F2F61
P 7400 3700
AR Path="/5C5EEFD0/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C5E4922/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C60014F/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C62C3B9/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DA8/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DAE/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662123/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C666787/5C662123/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C666787/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5F2F61" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A301/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5F2F61" Ref="U201"  Part="2" 
AR Path="/5C94BC35/5C662129/5C5F2F61" Ref="U?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5F2F61" Ref="U201"  Part="1" 
F 0 "U201" H 7400 3950 50  0000 C CNN
F 1 "TLV3202" H 7400 4150 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7400 3700 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv3201.pdf" H 7400 3700 50  0001 C CNN
	2    7400 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4400 4250 4450
$Comp
L power:VAA #PWR?
U 1 1 5C5EAE93
P 4250 4500
AR Path="/5C5E4922/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C62C3B9/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662123/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662123/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5EAE93" Ref="#PWR0301"  Part="1" 
AR Path="/5C94BC35/5C662129/5C5EAE93" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5EAE93" Ref="#PWR0401"  Part="1" 
F 0 "#PWR0301" H 4250 4350 50  0001 C CNN
F 1 "VAA" H 4268 4673 50  0000 C CNN
F 2 "" H 4250 4500 50  0001 C CNN
F 3 "" H 4250 4500 50  0001 C CNN
	1    4250 4500
	-1   0    0    1   
$EndComp
Text HLabel 7850 3700 2    50   Input ~ 0
~3-stage_triggered
$Comp
L power:VAA #PWR?
U 1 1 5C6063EE
P 7750 3050
AR Path="/5C5E4922/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C62C3B9/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063EE" Ref="#PWR0305"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063EE" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063EE" Ref="#PWR0405"  Part="1" 
F 0 "#PWR0305" H 7750 2900 50  0001 C CNN
F 1 "VAA" H 7850 3150 50  0000 L CNN
F 2 "" H 7750 3050 50  0001 C CNN
F 3 "" H 7750 3050 50  0001 C CNN
	1    7750 3050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C5E51A3
P 7750 3500
AR Path="/5C5E4922/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5E51A3" Ref="R308"  Part="1" 
AR Path="/5C94BC35/5C662129/5C5E51A3" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5E51A3" Ref="R408"  Part="1" 
F 0 "R308" H 7950 3550 50  0000 C CNN
F 1 "47K" H 7950 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7750 3500 50  0001 C CNN
F 3 "~" H 7750 3500 50  0001 C CNN
	1    7750 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C5E5199
P 6900 3550
AR Path="/5C5E4922/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C62C3B9/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662123/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662123/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5E5199" Ref="C304"  Part="1" 
AR Path="/5C94BC35/5C662129/5C5E5199" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5E5199" Ref="C404"  Part="1" 
F 0 "C304" H 7000 3450 50  0000 L CNN
F 1 "22p" H 6950 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6900 3550 50  0001 C CNN
F 3 "~" H 6900 3550 50  0001 C CNN
F 4 "" H 250 -400 50  0001 C CNN "Note"
	1    6900 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C5E5191
P 6700 3800
AR Path="/5C5E4922/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5E5191" Ref="R306"  Part="1" 
AR Path="/5C94BC35/5C662129/5C5E5191" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5E5191" Ref="R406"  Part="1" 
F 0 "R306" V 6600 3800 50  0000 C CNN
F 1 "4K7" V 6500 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6700 3800 50  0001 C CNN
F 3 "~" H 6700 3800 50  0001 C CNN
	1    6700 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C5E5186
P 6200 3350
AR Path="/5C5E4922/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C5E5186" Ref="R305"  Part="1" 
AR Path="/5C94BC35/5C662129/5C5E5186" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C5E5186" Ref="R405"  Part="1" 
F 0 "R305" V 6300 3350 50  0000 C CNN
F 1 "47K" V 6100 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6200 3350 50  0001 C CNN
F 3 "~" H 6200 3350 50  0001 C CNN
	1    6200 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C6063E4
P 5350 3600
AR Path="/5C5E4922/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063E4" Ref="R303"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063E4" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063E4" Ref="R403"  Part="1" 
F 0 "R303" H 5550 3650 50  0000 C CNN
F 1 "47K" H 5500 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5350 3600 50  0001 C CNN
F 3 "~" H 5350 3600 50  0001 C CNN
	1    5350 3600
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C6063E2
P 5150 3900
AR Path="/5C5E4922/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C62C3B9/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063E2" Ref="C303"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063E2" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063E2" Ref="C403"  Part="1" 
F 0 "C303" V 5000 3800 50  0000 L CNN
F 1 "22p" V 4900 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5150 3900 50  0001 C CNN
F 3 "~" H 5150 3900 50  0001 C CNN
F 4 "" H 350 -50 50  0001 C CNN "Note"
	1    5150 3900
	0    -1   -1   0   
$EndComp
Connection ~ 4950 3900
Wire Wire Line
	4950 3450 4950 3900
Wire Wire Line
	4500 3450 4550 3450
$Comp
L Device:R_Small R?
U 1 1 5C6063E1
P 4400 3600
AR Path="/5C5E4922/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063E1" Ref="R302"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063E1" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063E1" Ref="R402"  Part="1" 
F 0 "R302" V 4450 3300 50  0000 C CNN
F 1 "4K7" V 4350 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4400 3600 50  0001 C CNN
F 3 "~" H 4400 3600 50  0001 C CNN
	1    4400 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C6063E0
P 4400 3450
AR Path="/5C5E4922/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C62C3B9/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063E0" Ref="C301"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063E0" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063E0" Ref="C401"  Part="1" 
F 0 "C301" V 4650 3350 50  0000 L CNN
F 1 "22p" V 4550 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4400 3450 50  0001 C CNN
F 3 "~" H 4400 3450 50  0001 C CNN
F 4 "" H 450 -50 50  0001 C CNN "Note"
	1    4400 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C6063DF
P 4250 4300
AR Path="/5C5E4922/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063DF" Ref="R301"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063DF" Ref="R?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063DF" Ref="R401"  Part="1" 
F 0 "R301" H 4050 4250 50  0000 C CNN
F 1 "4K7" H 4000 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4250 4300 50  0001 C CNN
F 3 "~" H 4250 4300 50  0001 C CNN
	1    4250 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C6063DE
P 4400 4300
AR Path="/5C5E4922/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C62C3B9/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662123/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C666787/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C6063DE" Ref="C302"  Part="1" 
AR Path="/5C94BC35/5C662129/5C6063DE" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C6063DE" Ref="C402"  Part="1" 
F 0 "C302" H 4500 4250 50  0000 L CNN
F 1 "22p" H 4500 4150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4400 4300 50  0001 C CNN
F 3 "~" H 4400 4300 50  0001 C CNN
F 4 "" H 450 -200 50  0001 C CNN "Note"
	1    4400 4300
	1    0    0    -1  
$EndComp
Text HLabel 7850 3950 2    50   Input ~ 0
V_analog
$Comp
L power:Earth #PWR?
U 1 1 5C65DB4A
P 7550 4600
AR Path="/5C5E4922/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C62C3B9/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662123/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662123/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C65DB4A" Ref="#PWR0304"  Part="1" 
AR Path="/5C94BC35/5C662129/5C65DB4A" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C65DB4A" Ref="#PWR0404"  Part="1" 
F 0 "#PWR0304" H 7550 4350 50  0001 C CNN
F 1 "Earth" H 7550 4450 50  0001 C CNN
F 2 "" H 7550 4600 50  0001 C CNN
F 3 "~" H 7550 4600 50  0001 C CNN
	1    7550 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Dual_Generic U?
U 3 1 5C635278
P 7450 4250
AR Path="/5C5FAFFD/5C635278" Ref="U?"  Part="3" 
AR Path="/5C5E4922/5C635278" Ref="U?"  Part="3" 
AR Path="/5C62C3B9/5C635278" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C661DA8/5C635278" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C661DAE/5C635278" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C662123/5C635278" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C666787/5C662123/5C635278" Ref="U?"  Part="3" 
AR Path="/5C666787/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C689EFA/5C662123/5C635278" Ref="U?"  Part="3" 
AR Path="/5C689EFA/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C635278" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A301/5C662123/5C635278" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A301/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C94BC35/5C662123/5C635278" Ref="U301"  Part="3" 
AR Path="/5C94BC35/5C662129/5C635278" Ref="U?"  Part="3" 
AR Path="/5C94BC35/5C9582F9/5C635278" Ref="U401"  Part="3" 
F 0 "U301" H 7750 4300 50  0000 R CNN
F 1 "TLV9062" H 7750 4200 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7450 4250 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 7450 4250 50  0001 C CNN
	3    7450 4250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7550 3950 7750 3950
Wire Wire Line
	7550 4550 7750 4550
$Comp
L Device:C_Small C?
U 1 1 5C63AD67
P 7750 4250
AR Path="/5C68A269/5C68A2E1/5C662129/5C63AD67" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C63AD67" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C63AD67" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C63AD67" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C63AD67" Ref="C305"  Part="1" 
AR Path="/5C94BC35/5C662129/5C63AD67" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C63AD67" Ref="C405"  Part="1" 
F 0 "C305" H 7850 4250 50  0000 L CNN
F 1 "100n" H 7800 4150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7750 4250 50  0001 C CNN
F 3 "~" H 7750 4250 50  0001 C CNN
F 4 "" H 700 -200 50  0001 C CNN "Note"
	1    7750 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4350 7750 4550
Wire Wire Line
	7750 4150 7750 3950
Connection ~ 7750 3950
$Comp
L power:VAA #PWR?
U 1 1 5C643C1A
P 6900 3050
AR Path="/5C5E4922/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C62C3B9/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662123/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662123/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C643C1A" Ref="#PWR0303"  Part="1" 
AR Path="/5C94BC35/5C662129/5C643C1A" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C643C1A" Ref="#PWR0403"  Part="1" 
F 0 "#PWR0303" H 6900 2900 50  0001 C CNN
F 1 "VAA" H 7050 3150 50  0000 C CNN
F 2 "" H 6900 3050 50  0001 C CNN
F 3 "" H 6900 3050 50  0001 C CNN
	1    6900 3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 3450 4250 3600
Wire Wire Line
	4300 3600 4250 3600
Wire Wire Line
	7750 3950 7850 3950
Text HLabel 3750 4000 0    50   Input ~ 0
PV+
Text HLabel 3750 3800 0    50   Input ~ 0
PV-
Wire Wire Line
	5350 3700 5350 3900
Connection ~ 5350 3900
$Comp
L power:VAA #PWR?
U 1 1 5C8C0E98
P 5350 3050
AR Path="/5C5E4922/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C62C3B9/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662123/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662123/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C666787/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C662123/5C8C0E98" Ref="#PWR0302"  Part="1" 
AR Path="/5C94BC35/5C662129/5C8C0E98" Ref="#PWR?"  Part="1" 
AR Path="/5C94BC35/5C9582F9/5C8C0E98" Ref="#PWR0402"  Part="1" 
F 0 "#PWR0302" H 5350 2900 50  0001 C CNN
F 1 "VAA" H 5200 3150 50  0000 C CNN
F 2 "" H 5350 3050 50  0001 C CNN
F 3 "" H 5350 3050 50  0001 C CNN
	1    5350 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3050 5350 3350
Wire Wire Line
	5350 3350 5500 3350
Connection ~ 5350 3350
Wire Wire Line
	5350 3350 5350 3500
Wire Wire Line
	7550 4600 7550 4550
Connection ~ 7550 4550
Wire Wire Line
	6500 3350 6500 3800
Wire Wire Line
	6500 3350 6300 3350
Wire Wire Line
	5350 3900 5850 3900
Wire Wire Line
	6900 3450 6900 3050
Wire Wire Line
	6900 3650 6900 3800
Wire Wire Line
	6800 3800 6900 3800
Connection ~ 6900 3800
Wire Wire Line
	6900 3800 7100 3800
Wire Wire Line
	4250 3600 4250 3800
Connection ~ 4250 3600
Connection ~ 4250 3800
Wire Wire Line
	4250 3800 4350 3800
Wire Wire Line
	4250 4200 4250 4150
Connection ~ 4250 4000
Wire Wire Line
	4250 4000 4350 4000
Wire Wire Line
	4400 4200 4400 4150
Wire Wire Line
	4400 4150 4250 4150
Connection ~ 4250 4150
Wire Wire Line
	4250 4150 4250 4000
Wire Wire Line
	4400 4400 4400 4450
Wire Wire Line
	4400 4450 4250 4450
Connection ~ 4250 4450
Wire Wire Line
	4250 4450 4250 4500
Wire Wire Line
	4500 3600 4550 3600
Wire Wire Line
	4550 3600 4550 3450
Connection ~ 4550 3450
Wire Wire Line
	4550 3450 4950 3450
Wire Wire Line
	4950 3900 5050 3900
Wire Wire Line
	3750 3800 4250 3800
Wire Wire Line
	3750 4000 4250 4000
$EndSCHEMATC
