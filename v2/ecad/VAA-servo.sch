EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5C5FB07B
P 5400 3650
AR Path="/5C5FAFFD/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C696261/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C95C086/5C5FB07B" Ref="R802"  Part="1" 
F 0 "R802" H 5341 3604 50  0000 R CNN
F 1 "4K7" H 5341 3695 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5400 3650 50  0001 C CNN
F 3 "~" H 5400 3650 50  0001 C CNN
	1    5400 3650
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C5FB16A
P 5400 3350
AR Path="/5C5FAFFD/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C696261/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C95C086/5C5FB16A" Ref="R801"  Part="1" 
F 0 "R801" H 5341 3304 50  0000 R CNN
F 1 "4K7" H 5341 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5400 3350 50  0001 C CNN
F 3 "~" H 5400 3350 50  0001 C CNN
	1    5400 3350
	1    0    0    1   
$EndComp
Wire Wire Line
	5600 3700 5600 3850
Wire Wire Line
	5600 3850 6200 3850
Wire Wire Line
	6200 3850 6200 3600
Wire Wire Line
	5600 3500 5400 3500
Wire Wire Line
	5400 3500 5400 3550
Wire Wire Line
	5400 3450 5400 3500
Connection ~ 5400 3500
$Comp
L power:Earth #PWR?
U 1 1 5C5FB3EB
P 5400 4150
AR Path="/5C5FAFFD/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C696261/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C95C086/5C5FB3EB" Ref="#PWR0801"  Part="1" 
F 0 "#PWR0801" H 5400 3900 50  0001 C CNN
F 1 "Earth" H 5400 4000 50  0001 C CNN
F 2 "" H 5400 4150 50  0001 C CNN
F 3 "~" H 5400 4150 50  0001 C CNN
	1    5400 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3150 5400 3250
Text HLabel 5100 3150 0    50   Input ~ 0
V_analog
Connection ~ 5400 3150
$Comp
L Device:Opamp_Dual_Generic U?
U 1 1 5C633015
P 5900 3600
AR Path="/5C5FAFFD/5C633015" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C66211D/5C633015" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A2E7/5C633015" Ref="U?"  Part="1" 
AR Path="/5C696261/5C633015" Ref="U?"  Part="1" 
AR Path="/5C95C086/5C633015" Ref="U801"  Part="1" 
F 0 "U801" H 5900 3967 50  0000 C CNN
F 1 "TLV9062" H 5900 3876 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5900 3600 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 5900 3600 50  0001 C CNN
	1    5900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Dual_Generic U?
U 2 1 5C633372
P 6150 4200
AR Path="/5C5FAFFD/5C633372" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C66211D/5C633372" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E7/5C633372" Ref="U?"  Part="2" 
AR Path="/5C696261/5C633372" Ref="U?"  Part="2" 
AR Path="/5C95C086/5C633372" Ref="U801"  Part="2" 
F 0 "U801" H 6250 3850 50  0000 C CNN
F 1 "TLV9062" H 6250 3750 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6150 4200 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 6150 4200 50  0001 C CNN
F 4 "butterpass" H 6150 4200 50  0001 C CNN "Note"
	2    6150 4200
	1    0    0    -1  
$EndComp
Connection ~ 6200 3600
Wire Wire Line
	5850 4100 5750 4100
Wire Wire Line
	6450 4450 6450 4200
Wire Wire Line
	5850 4300 5850 4450
Wire Wire Line
	5850 4450 6450 4450
$Comp
L Device:Opamp_Dual_Generic U?
U 3 1 5C6333F1
P 7050 3550
AR Path="/5C5FAFFD/5C6333F1" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C6333F1" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C6333F1" Ref="U?"  Part="3" 
AR Path="/5C696261/5C6333F1" Ref="U?"  Part="3" 
AR Path="/5C95C086/5C6333F1" Ref="U801"  Part="3" 
F 0 "U801" H 7350 3600 50  0000 R CNN
F 1 "TLV9062" H 7350 3500 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7050 3550 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 7050 3550 50  0001 C CNN
	3    7050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3150 5400 3150
Wire Wire Line
	6950 3950 6700 3950
Wire Wire Line
	5750 3950 5750 4100
Wire Wire Line
	6200 3600 6300 3600
Wire Wire Line
	6950 3250 6950 3150
Wire Wire Line
	6950 3150 6700 3150
Wire Wire Line
	6950 3850 6950 3950
$Comp
L Device:C_Small C801
U 1 1 5C63F419
P 6700 3350
F 0 "C801" H 6300 3400 50  0000 L CNN
F 1 "100n" H 6300 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6700 3350 50  0001 C CNN
F 3 "~" H 6700 3350 50  0001 C CNN
F 4 "" H 1100 200 50  0001 C CNN "Note"
	1    6700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3250 6700 3150
Connection ~ 6700 3150
Wire Wire Line
	6700 3150 5400 3150
Wire Wire Line
	6700 3450 6700 3950
Connection ~ 6700 3950
Wire Wire Line
	6700 3950 5750 3950
$Comp
L power:VAA #PWR0802
U 1 1 5C95C42B
P 6300 3600
F 0 "#PWR0802" H 6300 3450 50  0001 C CNN
F 1 "VAA" V 6317 3728 50  0000 L CNN
F 2 "" H 6300 3600 50  0001 C CNN
F 3 "" H 6300 3600 50  0001 C CNN
	1    6300 3600
	0    1    1    0   
$EndComp
Connection ~ 5750 4100
Wire Wire Line
	5750 4100 5400 4100
Wire Wire Line
	5400 3750 5400 4100
Connection ~ 5400 4100
Wire Wire Line
	5400 4100 5400 4150
$EndSCHEMATC
