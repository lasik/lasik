EESchema Schematic File Version 4
LIBS:lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5550 3900 1000 700 
U 5C662123
F0 "A2" 50
F1 "3-stage.sch" 50
F2 "V_analog" I R 6550 4000 50 
F3 "PV+" I R 6550 4350 50 
F4 "PV-" I R 6550 4500 50 
F5 "~3-stage_triggered" I R 6550 4150 50 
$EndSheet
Text HLabel 6750 3100 2    50   Input ~ 0
V_analog
Text HLabel 6750 3250 2    50   Input ~ 0
~trig1
Text HLabel 6750 4150 2    50   Input ~ 0
~trig2
Wire Wire Line
	6750 4150 6550 4150
$Comp
L Comparator:LM393 U?
U 3 1 5C68804A
P 5250 3800
AR Path="/5C5EEFD0/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C5E4922/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C60014F/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C62C3B9/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DA8/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DAE/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662123/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662129/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C5FAFFD/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C666787/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C689EFA/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E1/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A301/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C94BC35/5C68804A" Ref="U201"  Part="3" 
F 0 "U201" H 5350 3850 50  0000 C CNN
F 1 "TLV3202" H 5350 3750 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5250 3800 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv3201.pdf" H 5250 3800 50  0001 C CNN
F 4 "" H 1000 2300 50  0001 C CNN "Note"
	3    5250 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5350 4150 5350 4100
Wire Wire Line
	5350 3500 4900 3500
$Comp
L Device:C_Small C?
U 1 1 5C638DE9
P 4900 3800
AR Path="/5C68A269/5C68A2E1/5C638DE9" Ref="C?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C638DE9" Ref="C?"  Part="1" 
AR Path="/5C94BC35/5C638DE9" Ref="C201"  Part="1" 
F 0 "C201" H 5250 3850 50  0000 R CNN
F 1 "100n" H 5300 3750 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4900 3800 50  0001 C CNN
F 3 "~" H 4900 3800 50  0001 C CNN
F 4 "" H 1000 2300 50  0001 C CNN "Note"
	1    4900 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 3900 4900 4150
Wire Wire Line
	4900 4150 5350 4150
Wire Wire Line
	4900 3700 4900 3500
Text HLabel 6750 4350 2    50   Input ~ 0
in2+
Text HLabel 6750 4500 2    50   Input ~ 0
in2-
Wire Wire Line
	6750 4350 6550 4350
Wire Wire Line
	6550 4500 6750 4500
$Comp
L power:Earth #PWR0201
U 1 1 5C955ED1
P 5350 4200
F 0 "#PWR0201" H 5350 3950 50  0001 C CNN
F 1 "Earth" H 5350 4050 50  0001 C CNN
F 2 "" H 5350 4200 50  0001 C CNN
F 3 "~" H 5350 4200 50  0001 C CNN
	1    5350 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4200 5350 4150
Connection ~ 5350 4150
Wire Wire Line
	6650 4000 6550 4000
Wire Wire Line
	6650 3100 6650 4000
Text HLabel 4750 3500 0    50   Input ~ 0
V_analog
Wire Wire Line
	6750 3100 6650 3100
$Sheet
S 5550 3000 1000 700 
U 5C9582F9
F0 "A1" 50
F1 "3-stage.sch" 50
F2 "V_analog" I R 6550 3100 50 
F3 "PV+" I R 6550 3450 50 
F4 "PV-" I R 6550 3600 50 
F5 "~3-stage_triggered" I R 6550 3250 50 
$EndSheet
Wire Wire Line
	6650 3100 6550 3100
Connection ~ 6650 3100
Wire Wire Line
	6750 3250 6550 3250
Text HLabel 6750 3450 2    50   Input ~ 0
in1+
Text HLabel 6750 3600 2    50   Input ~ 0
in1-
Wire Wire Line
	6750 3600 6550 3600
Wire Wire Line
	6750 3450 6550 3450
Wire Wire Line
	4750 3500 4900 3500
Connection ~ 4900 3500
$EndSCHEMATC
