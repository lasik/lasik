#ifndef LASER_H__
#define LASER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */

typedef struct
{
    uint16_t    value;
    uint16_t    repeats;

} laser_pwm_segment_t;


typedef struct
{
    laser_pwm_segment_t * const     segments;
    uint32_t                        length;

} laser_pwm_sequence_t;

/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

void laser_init(void);
void laser_set_pwm(uint8_t laser_index, uint32_t new_value);
void laser_set_seq_fromdata(uint8_t laser_index, const uint8_t * data, uint32_t len);

/*
 * Send some bytes over a laser
 * WARNING: data must not be located in flash (for the uart peripheral)
 */
void laser_send_data(uint8_t laser_pin, const uint8_t * data, uint32_t len);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // LASER_H__
