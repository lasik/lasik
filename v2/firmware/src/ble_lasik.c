#include "sdk_common.h"
#include "ble_lasik.h"
#include "ble_srv_common.h"
#include "nrf_log.h"

#include "bluetooth.h"

/* =============================== CONSTANTS ================================ */

#define MAX_PWM_SEQ_SEGMENTS    100
#define PWM_SEQ_SEGMENT_SIZE    ( 2 * sizeof(uint16_t) )
#define MAX_PWM_SEQ_DATA_SIZE   ( MAX_PWM_SEQ_SEGMENTS * PWM_SEQ_SEGMENT_SIZE )

static const uint16_t laserpwm_uuids[N_LASERS] = {
    LASIK_UUID_LASERPWM0_CHAR,
    LASIK_UUID_LASERPWM1_CHAR
};

static const uint16_t send_probe_uuids[N_LASERS] = {
    LASIK_UUID_SEND_PROBE0_CHAR,
    LASIK_UUID_SEND_PROBE1_CHAR
};

static const uint16_t beamstate_uuids[N_LASERS] = {
    LASIK_UUID_BEAMSTATE0_CHAR,
    LASIK_UUID_BEAMSTATE1_CHAR
};

static const uint16_t seen_probe_uuids[N_LASERS] = {
    LASIK_UUID_SEEN_PROBE0_CHAR,
    LASIK_UUID_SEEN_PROBE1_CHAR
};

static uint8_t user_description_string_laser1[] = "Laser 1";
static uint8_t user_description_string_laser2[] = "Laser 2";
static uint8_t user_description_string_send_probe1[] = "send probe 1";
static uint8_t user_description_string_send_probe2[] = "send probe 2";
static uint8_t user_description_string_sensor1[] = "Trigger 1";
static uint8_t user_description_string_sensor2[] = "Trigger 2";
static uint8_t user_description_string_seen_probe1[] = "seen probe 1";
static uint8_t user_description_string_seen_probe2[] = "seen probe 2";
static uint8_t user_description_string_v_bat[] = "Battery voltage";

static ble_add_char_user_desc_t user_descriptor_template = {
    .max_size           = 0,      // must be set before use
    .size               = 0,      // must be set before use
    .p_char_user_desc   = NULL,   // must be set before use
    .char_props.read    = 1,
    .read_access        = SEC_OPEN,
};

static const ble_add_char_params_t laserpwm_char_params = {
    .init_len              = PWM_SEQ_SEGMENT_SIZE,
    .max_len               = MAX_PWM_SEQ_DATA_SIZE,
    .char_props.read       = 1,     // TODO needed?
    .char_props.write      = 1,
    .char_props.write_wo_resp = 1,
    .read_access           = SEC_OPEN,
    .write_access          = SEC_OPEN,
    .p_presentation_format = NULL,
    .p_user_descr          = NULL,
};

static const ble_add_char_params_t send_probe_char_params = {
    .init_len              = sizeof(uint8_t),
    .max_len               = sizeof(uint8_t),
    .char_props.write      = 1,
    .char_props.write_wo_resp = 1,  // TODO needed?
    .read_access           = SEC_OPEN,
    .write_access          = SEC_OPEN,
    .p_presentation_format = NULL,
    .p_user_descr          = NULL,
};

static const ble_add_char_params_t beamstate_char_params = {
    .init_len              = sizeof(uint8_t),
    .max_len               = sizeof(uint8_t),
    .char_props.read       = 1,     // TODO needed?
    .char_props.notify     = 1,
    .read_access           = SEC_OPEN,
    .cccd_write_access     = SEC_OPEN,
    .p_presentation_format = NULL,
    .p_user_descr          = NULL,
};

static const ble_add_char_params_t seen_probe_char_params = {
    .init_len              = sizeof(uint8_t),
    .max_len               = sizeof(uint8_t),
    .char_props.read       = 1,
    .char_props.notify     = 1,
    .read_access           = SEC_OPEN,
    .cccd_write_access     = SEC_OPEN,
    .p_presentation_format = NULL,
    .p_user_descr          = NULL,
};

static const ble_add_char_params_t v_bat_char_params = {
    .init_len              = sizeof(uint16_t),
    .max_len               = sizeof(uint16_t),
    .char_props.read       = 1,
    .char_props.notify     = 1,
    .read_access           = SEC_OPEN,
    .cccd_write_access     = SEC_OPEN,
    .p_presentation_format = NULL,
    .p_user_descr          = NULL,
};

/* ============================== DATA TYPES ================================ */
/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

static void on_write_event( ble_lasik_t *                   p_lasik,
                            ble_gatts_evt_write_t const *   p_evt_write );

static void on_write_to_laser( uint16_t                         laser_index,
                               ble_lasik_t *                    p_lasik,
                               ble_gatts_evt_write_t const *    p_evt_write );

static void on_write_to_send_probe( uint16_t                      laser_index,
                                    ble_lasik_t *                 p_lasik,
                                    ble_gatts_evt_write_t const * p_evt_write );

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

void _ble_lasik_on_ble_evt(ble_evt_t const * p_event, void * p_context)
{
    ble_lasik_t * p_lasik = (ble_lasik_t *)p_context;

    switch (p_event->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            on_write_event(p_lasik, &p_event->evt.gatts_evt.params.write);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_lasik_init(
        ble_lasik_t *                           p_lasik,
        const ble_lasik_laserpwm_write_h_t      laserpwm_write_h,
        const ble_lasik_send_probe_write_h_t    send_probe_write_h)
{
    uint32_t              err_code;
    ble_uuid_t            ble_uuid;
    ble_add_char_params_t add_char_params;
    ble_add_char_user_desc_t user_descriptor;

    // Initialize service structure.
    p_lasik->laserpwm_write_h   = laserpwm_write_h;
    p_lasik->send_probe_write_h = send_probe_write_h;

    // Add service.
    ble_uuid128_t base_uuid = {LASIK_UUID_BASE};
    err_code = sd_ble_uuid_vs_add( &base_uuid, &p_lasik->uuid_type );
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_lasik->uuid_type;
    ble_uuid.uuid = LASIK_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add( BLE_GATTS_SRVC_TYPE_PRIMARY,
                                         &ble_uuid,
                                         &p_lasik->service_handle );
    VERIFY_SUCCESS(err_code);

    // Add beamstate characteristics.
    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        add_char_params = beamstate_char_params; // sets params besides uuid(_type)
        add_char_params.uuid = beamstate_uuids[i];
        add_char_params.uuid_type = p_lasik->uuid_type;

        user_descriptor = user_descriptor_template;
        if(i == 0)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_sensor1);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_sensor1);
            user_descriptor.p_char_user_desc =    user_description_string_sensor1;
        }
        else if (i == 1)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_sensor2);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_sensor2);
            user_descriptor.p_char_user_desc =    user_description_string_sensor2;

        }
        else ASSERT(0);

        add_char_params.p_user_descr = &user_descriptor;

        err_code = characteristic_add( p_lasik->service_handle,
                                       &add_char_params,
                                       &p_lasik->beamstate_char_handles[i] );
        VERIFY_SUCCESS(err_code);
    }

    // Add 'seen_probe' characteristics.
    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        add_char_params = seen_probe_char_params; // sets params besides uuid(_type)
        add_char_params.uuid = seen_probe_uuids[i];
        add_char_params.uuid_type = p_lasik->uuid_type;

        user_descriptor = user_descriptor_template;
        if(i == 0)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_seen_probe1);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_seen_probe1);
            user_descriptor.p_char_user_desc =    user_description_string_seen_probe1;
        }
        else if (i == 1)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_seen_probe2);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_seen_probe2);
            user_descriptor.p_char_user_desc =    user_description_string_seen_probe2;

        }
        else ASSERT(0);

        add_char_params.p_user_descr = &user_descriptor;

        err_code = characteristic_add( p_lasik->service_handle,
                                       &add_char_params,
                                       &p_lasik->seen_probe_char_handles[i] );
        VERIFY_SUCCESS(err_code);
    }

    // Add LASER characteristics.
    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        add_char_params = laserpwm_char_params; // sets params besides uuid(_type)
        add_char_params.uuid = laserpwm_uuids[i];
        add_char_params.uuid_type = p_lasik->uuid_type;

        user_descriptor = user_descriptor_template;
        if(i == 0)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_laser1);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_laser1);
            user_descriptor.p_char_user_desc =    user_description_string_laser1;
        }
        else if (i == 1)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_laser2);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_laser2);
            user_descriptor.p_char_user_desc =    user_description_string_laser2;

        }
        else ASSERT(0);

        add_char_params.p_user_descr = &user_descriptor;

        err_code = characteristic_add( p_lasik->service_handle,
                                       &add_char_params,
                                       &p_lasik->laserpwm_char_handles[i] );
        VERIFY_SUCCESS(err_code);
    }

    // Add 'send_probe' characteristics.
    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        add_char_params = send_probe_char_params; // sets params besides uuid(_type)
        add_char_params.uuid = send_probe_uuids[i];
        add_char_params.uuid_type = p_lasik->uuid_type;

        user_descriptor = user_descriptor_template;
        if(i == 0)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_send_probe1);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_send_probe1);
            user_descriptor.p_char_user_desc =    user_description_string_send_probe1;
        }
        else if (i == 1)
        {
            user_descriptor.max_size = ARRAY_SIZE(user_description_string_send_probe2);
            user_descriptor.size =     ARRAY_SIZE(user_description_string_send_probe2);
            user_descriptor.p_char_user_desc =    user_description_string_send_probe2;

        }
        else ASSERT(0);

        add_char_params.p_user_descr = &user_descriptor;

        err_code = characteristic_add( p_lasik->service_handle,
                                       &add_char_params,
                                       &p_lasik->send_probe_char_handles[i] );
        VERIFY_SUCCESS(err_code);
    }

    // Add V_BAT characteristic
    {
        add_char_params = v_bat_char_params; // sets params besides uuid(_type)
        add_char_params.uuid = LASIK_UUID_V_BAT_CHAR;
        add_char_params.uuid_type = p_lasik->uuid_type;

        user_descriptor = user_descriptor_template;
        user_descriptor.max_size = ARRAY_SIZE(user_description_string_v_bat);
        user_descriptor.size =     ARRAY_SIZE(user_description_string_v_bat);
        user_descriptor.p_char_user_desc =    user_description_string_v_bat;
        add_char_params.p_user_descr = &user_descriptor;

        err_code = characteristic_add( p_lasik->service_handle,
                                       &add_char_params,
                                       &p_lasik->v_bat_char_handle );
        VERIFY_SUCCESS(err_code);
    }

    return NRF_SUCCESS;
}


void ble_lasik_beamdetect_handler( uint8_t sensor_index,
                                   uint8_t beamstate,
                                   void *  context )
{
    uint16_t conn_handle = bluetooth_get_connection();
    // TODO handle INVALID value for no connection? also see other handlers
    // TODO deduplicate this function

    ble_lasik_t* p_lasik = (ble_lasik_t*) context;

    uint16_t len = sizeof(beamstate);
    ble_gatts_hvx_params_t params =
    {
        .type = BLE_GATT_HVX_NOTIFICATION,
        .handle = p_lasik->beamstate_char_handles[sensor_index].value_handle,
        .p_data = &beamstate,
        .p_len = &len,
    };

    sd_ble_gatts_hvx(conn_handle, &params); // ignoring the return value
}


void ble_lasik_seen_probe_handler( uint8_t sensor_index,
                                   uint8_t seen_probe,
                                   void *  context )
{
    uint16_t conn_handle = bluetooth_get_connection();
    // will return BLE_CONN_HANDLE_INVALID for no connection

    ble_lasik_t* p_lasik = (ble_lasik_t*) context;

    uint16_t len = sizeof(seen_probe);
    ble_gatts_hvx_params_t params =
    {
        .type = BLE_GATT_HVX_NOTIFICATION,
        .handle = p_lasik->seen_probe_char_handles[sensor_index].value_handle,
        .p_data = &seen_probe,
        .p_len = &len,
    };

    sd_ble_gatts_hvx(conn_handle, &params); // ignoring the return value
}


void ble_lasik_set_v_bat( uint16_t voltage_mv, void* context)
{
    uint16_t conn_handle = bluetooth_get_connection();
    // will return BLE_CONN_HANDLE_INVALID for no connection

    ble_lasik_t* p_lasik = (ble_lasik_t*) context;

    uint16_t len = sizeof(voltage_mv);
    ble_gatts_hvx_params_t params =
    {
        .type = BLE_GATT_HVX_NOTIFICATION,
        .handle = p_lasik->v_bat_char_handle.value_handle,
        .p_data = (uint8_t*) &voltage_mv,
        .p_len = &len,
    };

    sd_ble_gatts_hvx(conn_handle, &params);
    // ignoring the return value, when conn_handle is BLE_CONN_HANDLE_INVALID
    // then it will return BLE_ERROR_INVALID_CONN_HANDLE but update the attr.
    // anyway
    // TODO: when there is a valid connection, then it returns
    // BLE_ERROR_GATTS_SYS_ATTR_MISSING, not sure what to do with this one

}

/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

// find the characteristic and forward the event to the right handler
static void on_write_event( ble_lasik_t *                   p_lasik,
                            ble_gatts_evt_write_t const *   p_evt_write )
{
    NRF_LOG_DEBUG("Write event");

    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        uint16_t value_handle = p_lasik->laserpwm_char_handles[i].value_handle;
        if ( p_evt_write->handle == value_handle )
        {
            return on_write_to_laser(i, p_lasik, p_evt_write);
        }
    }

    for(uint8_t i = 0; i < N_LASERS; i++)
    {
        uint16_t value_handle = p_lasik->send_probe_char_handles[i].value_handle;
        if ( p_evt_write->handle == value_handle )
        {
            return on_write_to_send_probe(i, p_lasik, p_evt_write);
        }
    }

    NRF_LOG_WARNING("Unhandled write event");
}

static void on_write_to_laser( uint16_t                         laser_index,
                               ble_lasik_t *                    p_lasik,
                               ble_gatts_evt_write_t const *    p_evt_write )
{
    bool success = true;

    if ( p_evt_write->len % 4 != 0)
    {
        NRF_LOG_WARNING("laser write event data has invalid length.");
        success = false;
    }

    if ( p_lasik->laserpwm_write_h == NULL )
    {
        NRF_LOG_WARNING("(%s) no handler available", __func__);
        success = false;
    }

    if ( success )
    {
        NRF_LOG_DEBUG("writing to to LASER%d characteristic", laser_index);

        const uint8_t * data = p_evt_write->data;
        uint32_t len = p_evt_write->len;
        p_lasik->laserpwm_write_h(laser_index, data, len);
    }
}

static void on_write_to_send_probe( uint16_t                      laser_index,
                                    ble_lasik_t *                 p_lasik,
                                    ble_gatts_evt_write_t const * p_evt_write )
{
    bool success = true;

    if ( p_evt_write->len != sizeof(uint8_t))
    {
        NRF_LOG_WARNING("'send_probe' write event data has invalid length.");
        success = false;
    }

    uint8_t send_probe = *p_evt_write->data;
    if ( send_probe != 0 && send_probe != 1 )
    {
        NRF_LOG_WARNING("'send_probe' write event data has invalid value: %d",
                        send_probe );
        success = false;
    }

    if ( p_lasik->send_probe_write_h == NULL )
    {
        NRF_LOG_WARNING("(%s) no handler available", __func__);
        success = false;
    }

    if ( success )
    {
        if(send_probe) {
            NRF_LOG_DEBUG("enabling probe on laser%d", laser_index);
        } else {
            NRF_LOG_DEBUG("disabling probe on laser%d", laser_index);
        }
        p_lasik->send_probe_write_h(laser_index, send_probe);
    }
}

/* ========================================================================== */
