#ifndef BLE_LASIK_H__
#define BLE_LASIK_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "nrf_sdh_ble.h"

#include "battery.h"
#include "beamdetect.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */

#define N_LASERS                    2

#define LASIK_UUID_BASE             { 0xe2, 0x4e, 0x07, 0x92, \
                                      0x11, 0x97, 0x4f, 0xc6, \
                                      0xbc, 0x2d, 0x3e, 0x0d, \
                                      0x00, 0x00, 0x00, 0x00 }

#define LASIK_UUID_SERVICE          0x0000

#define LASIK_UUID_BEAMSTATE0_CHAR  0x1000
#define LASIK_UUID_BEAMSTATE1_CHAR  0x1001

#define LASIK_UUID_LASERPWM0_CHAR   0x2000
#define LASIK_UUID_LASERPWM1_CHAR   0x2001

#define LASIK_UUID_V_BAT_CHAR       0x3000

#define LASIK_UUID_SEND_PROBE0_CHAR   0x4000
#define LASIK_UUID_SEND_PROBE1_CHAR   0x4001

#define LASIK_UUID_SEEN_PROBE0_CHAR   0x5000
#define LASIK_UUID_SEEN_PROBE1_CHAR   0x5001


/* Macro for defining a lasik ble service instance */
#define BLE_LASIK_DEF(_name)                                              \
static ble_lasik_t _name;                                                 \
NRF_SDH_BLE_OBSERVER(_name ## _obs, 2, _ble_lasik_on_ble_evt, &_name)

/* ============================== DATA TYPES ================================ */

typedef void (*ble_lasik_laserpwm_write_h_t) ( uint8_t          laser_index,
                                               const uint8_t *  data,
                                               uint32_t         len );

typedef void (*ble_lasik_send_probe_write_h_t) ( uint8_t        laser_index,
                                                 uint8_t        send_probe );

typedef struct
{
    uint16_t                            service_handle;
    ble_gatts_char_handles_t            laserpwm_char_handles[N_LASERS];
    ble_gatts_char_handles_t            beamstate_char_handles[N_LASERS];
    ble_gatts_char_handles_t            send_probe_char_handles[N_LASERS];
    ble_gatts_char_handles_t            seen_probe_char_handles[N_LASERS];
    ble_gatts_char_handles_t            v_bat_char_handle;
    uint8_t                             uuid_type;
    ble_lasik_laserpwm_write_h_t        laserpwm_write_h;   /* _h for handler */
    ble_lasik_send_probe_write_h_t      send_probe_write_h; /* _h for handler */

} ble_lasik_t;

/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

/* initialise the Lasik Service. */
uint32_t ble_lasik_init( ble_lasik_t *                  p_lasik,
                         ble_lasik_laserpwm_write_h_t   laserpwm_write_h,
                         ble_lasik_send_probe_write_h_t send_probe_write_h);

/* handle a BLE event.
 * 
 * this function is registered as a ble event observer through the
 * BLE_LASIK_DEF() macro. The name starts with _ since it should not be called
 * except through this macro.
 *
 * p_ble_evt    the event received from the BLE stack.
 * p_context    the lasik ble service instance.
 */
void _ble_lasik_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

/* send a beamstate change notification to the right characteristic. */
beamdetect_change_handler_t ble_lasik_beamdetect_handler;

/* update the value of the right 'seen_probe' characteristic */
beamdetect_seen_probe_handler_t ble_lasik_seen_probe_handler;

/* update the battery voltage published by the corresponding attribute */
battery_v_change_handler_t ble_lasik_set_v_bat;

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // BLE_LASIK_H__
