#include <stdint.h>
#include <string.h>

#include "nrf_gpio.h"

#include "pinout.h"

#include "leds.h"

/* =============================== CONSTANTS ================================ */

static const uint32_t led2pin_map[] = {
    LED1_PIN,
    LED2_PIN,
    LED3_PIN,
    LED4_PIN,
};

/* ============================== DATA TYPES ================================ */
/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

static uint32_t led2pin(enum leds led);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

void leds_init(void)
{
    nrf_gpio_cfg_output(LED1_PIN);
    nrf_gpio_cfg_output(LED2_PIN);
    nrf_gpio_cfg_output(LED3_PIN);
    nrf_gpio_cfg_output(LED4_PIN);

    nrf_gpio_pin_write(LED1_PIN, 0);
    nrf_gpio_pin_write(LED2_PIN, 0);
    nrf_gpio_pin_write(LED3_PIN, 0);
    nrf_gpio_pin_write(LED4_PIN, 0);
}


void leds_write(enum leds led, int value)
{
    nrf_gpio_pin_write(led2pin(led), value);
}


void leds_toggle(enum leds led)
{
    nrf_gpio_pin_toggle(led2pin(led));
}

/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

uint32_t led2pin(enum leds led)
{
    ASSERT(led <= LED4);
    return led2pin_map[led];
}
