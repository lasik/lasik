#ifndef BLUETOOTH_H__
#define BLUETOOTH_H__

#include "ble.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */
/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

void bluetooth_init(void);
void bluetooth_start(ble_uuid_t* adv_uuids, uint8_t uuid_cnt);
uint16_t bluetooth_get_connection(void);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // BLUETOOTH_H__
