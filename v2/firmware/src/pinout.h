#ifndef PINOUT_H__
#define PINOUT_H__

#include "nrf_saadc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */

#define LED1_PIN                        9   // blinks on run
#define LED2_PIN                        10  // connected
#define LED3_PIN                        11  // <-> Beam 1
#define LED4_PIN                        12  // <-> Beam 2

#define LASER1_PIN                      6
#define LASER2_PIN                      5

#define TRIG1_PIN                       8
#define TRIG2_PIN                       7

#define V_AN_EN_PIN                     4   // set HI to power opamps

#define V_SENSE_AIN                     NRF_SAADC_INPUT_AIN1

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // PINOUT_H__
