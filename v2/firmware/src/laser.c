#include <stdint.h>
#include <string.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_pwm.h"
#include "nrfx_pwm.h"
#include "nrf_gpio.h"

#include "pinout.h"
#include "uart.h"

#include "laser.h"

/* =============================== CONSTANTS ================================ */

#define LASER_PWM_SEQ_MAX_LEN 100

/* ============================== DATA TYPES ================================ */

typedef struct
{
    uint32_t position;
    uint32_t period_cnt;

} sequence_state_t;


struct laser
{
    uint16_t                        laser_duty_cycles[NRF_PWM_CHANNEL_COUNT];
    const nrfx_pwm_t                pwm;
    laser_pwm_sequence_t            sequences[2];
    sequence_state_t                sequence_states[2];
};

/* ============================== GLOBAL VARS =============================== */

laser_pwm_segment_t _buffer1[LASER_PWM_SEQ_MAX_LEN] = {{.value = 0, .repeats = 1}};
laser_pwm_segment_t _buffer2[LASER_PWM_SEQ_MAX_LEN] = {{.value = 0, .repeats = 1}};

static struct laser laser = {
    .laser_duty_cycles = {0},
    .pwm = NRFX_PWM_INSTANCE(0),
    .sequences = {
        { .segments = _buffer1, .length = 1 },
        { .segments = _buffer2, .length = 1 } },
    .sequence_states = {
        { .position = 0, .period_cnt = 0 },
        { .position = 0, .period_cnt = 0 } }
};

/* ========================== FUNCTION PROTOTYPES =========================== */

static void pwm_handler(nrfx_pwm_evt_type_t event_type);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

void laser_init(void)
{
    // setup pwm
    const uint8_t pwm_pins[4] = { LASER1_PIN, LASER2_PIN,
                                  (uint8_t)NRF_PWM_PIN_NOT_CONNECTED,
                                  (uint8_t)NRF_PWM_PIN_NOT_CONNECTED };
    nrfx_pwm_config_t pwm_config = NRFX_PWM_DEFAULT_CONFIG;
    memcpy(pwm_config.output_pins, pwm_pins, sizeof(pwm_pins));

    nrf_pwm_sequence_t sequence = {
      .values = laser.laser_duty_cycles,
      .length = NRF_PWM_CHANNEL_COUNT,
      .repeats = 0,
      .end_delay = 0
    };

    nrfx_pwm_init(&laser.pwm, &pwm_config, pwm_handler);
    nrfx_pwm_simple_playback(&laser.pwm, &sequence, 1, NRFX_PWM_FLAG_LOOP);

    // config output pins
    nrf_gpio_cfg_output(LASER1_PIN);
    nrf_gpio_cfg_output(LASER2_PIN);
    nrf_gpio_pin_write(LASER1_PIN, 1);  // 1 is laser off
    nrf_gpio_pin_write(LASER2_PIN, 1);
}


// currently not in use
void laser_set_pwm(uint8_t laser_index, uint32_t new_value)
{
    NRF_LOG_INFO("Setting LASER%d to %d", laser_index + 1, new_value);

    laser_pwm_sequence_t *  sequence =
        &(laser.sequences[laser_index]);
    sequence->length = 1;
    sequence->segments[0].value = new_value;
    sequence->segments[0].repeats = 1;

    // reset the state
    sequence_state_t *      state =
        &(laser.sequence_states[laser_index]);
    state->position = 0;
    state->period_cnt = 0;
}


void laser_send_data(uint8_t laser_pin, const uint8_t * data, uint32_t len)
{
    volatile uint32_t * pwm_pin_select_reg = NULL;
    switch(laser_pin) {
        case LASER1_PIN:
            pwm_pin_select_reg = &laser.pwm.p_registers->PSEL.OUT[0];
            break;
        case LASER2_PIN:
            pwm_pin_select_reg = &laser.pwm.p_registers->PSEL.OUT[1];
            break;
        default:
            ASSERT(0);
    }

    // disconnect pin from pwm and connect it to the uart peripheral
    uart_set_tx_pin(laser_pin);
    *pwm_pin_select_reg = NRF_PWM_PIN_NOT_CONNECTED;

    // transmit
    uart_transmit(data, len);

    // disconnect pin from uart and reconnect it to the pwm peripheral
    *pwm_pin_select_reg = laser_pin;
    uart_disconnect_tx_pin();
}


// read 'len' bytes from data and parse it as a laser pwm sequence
void laser_set_seq_fromdata(uint8_t laser_index, const uint8_t * data, uint32_t len)
{
    NRF_LOG_DEBUG("Setting Sequence for LASER%d", laser_index + 1);
    ASSERT(len % 4 == 0);
    laser_pwm_sequence_t *  sequence =
        &(laser.sequences[laser_index]);

    // copy the sequence into this module
    sequence->length = len / 4;
    for(uint32_t i = 0; i < len; i+= 4)
    {
        // interpret 4 bytes as a value and a repeat
        laser_pwm_segment_t segment = *(laser_pwm_segment_t*)(&data[i]);
        sequence->segments[i / 4] = segment;
    }
    // reset the state so it starts at the beginning next time
    sequence_state_t *      state =
        &(laser.sequence_states[laser_index]);
    state->position = sequence->length - 1;
    state->period_cnt = sequence->segments[state->position].repeats - 1;
}

/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

static void pwm_handler(nrfx_pwm_evt_type_t event_type)
// TODO this get executed from the interrupt handler, any possible issues?
{
    if (event_type == NRFX_PWM_EVT_FINISHED)
    {
        for(uint32_t index = 0; index < 2; index++)
        {
            sequence_state_t *      state =
                &(laser.sequence_states[index]);
            laser_pwm_sequence_t *  sequence =
                &(laser.sequences[index]);
            laser_pwm_segment_t *   current_segment =
                &(sequence->segments[state->position]);

            (state->period_cnt)++;
            if (state->period_cnt == current_segment->repeats)
            {
                state->period_cnt = 0;
                state->position = (state->position + 1) % sequence->length;
                laser_pwm_segment_t * new_segment =
                    &(sequence->segments[state->position]);
                // set new pwm value
                laser.laser_duty_cycles[index] = new_segment->value;
            }
        }
    }
}
