#ifndef LEDS_H__
#define LEDS_H__

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */

enum leds { LED1, LED2, LED3, LED4 };

/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

void leds_init(void);
void leds_write(enum leds led, int value);
void leds_toggle(enum leds led);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // LEDS_H__
