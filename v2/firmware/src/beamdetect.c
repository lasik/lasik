#include <stdint.h>
#include <string.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "leds.h"
#include "uart.h"
#include "laser.h"
#include "pinout.h"

#include "beamdetect.h"

/* This module controls data transmission over the lasers and the the uarts
 * to figure out what the beam status is for all sensors.
 *
 * Currently it is not very robust with regards to false positives and
 * negatives due to noise and analog waveform. Possible improvements include:
 * a kind of preamble to condition the analogs, a longer code to match, allow
 * missing n signals before considering the beam off, ...
 */

/* =============================== CONSTANTS ================================ */

#define pulse_period                10      // send pulses every x ms
#define trig_switch_period          12      // change listening sensor period
#define trig_switch_dead             1      // ignore x ms after switch

// must NOT be made const, uarte peripheral needs data from ram.
// the first byte should be 0b11100000 for signal shaping
// each byte is tranmitted LSB first, 0 is laser on
// the last byte of each is what we look for so it should only appear once
uint8_t magic_data[] = { 0b11100000, 0b00110110 };
// this is another signal to be used for matching lasers to sensors
uint8_t probe_data[] = { 0b11100000, 0b10110110 };

/* ============================== DATA TYPES ================================ */

struct beamdetect {
    uint32_t beam_detected;
    uint32_t probe_seen;
    uint8_t send_probe[2];
    beamdetect_change_handler_t *       beam_change_handler;
    beamdetect_seen_probe_handler_t *   seen_probe_handler;
    void* handler_context;
};

/* ============================== GLOBAL VARS =============================== */

static struct beamdetect beamdetect;

/* ========================== FUNCTION PROTOTYPES =========================== */

void on_beam_result(uint32_t beam_index, uint32_t detected, uint32_t probe_seen);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */


void beamdetect_init()
{
    uart_set_rx_pin(TRIG1_PIN);
}

void beamdetect_ms_tick(uint32_t cnt)
{
    STATIC_ASSERT( pulse_period < trig_switch_period - trig_switch_dead,
                   "Sensor listening time to short");

    // send data pulses
    if((cnt % pulse_period) == 0)
    {
        uint8_t * data;
        uint8_t len;

        data = beamdetect.send_probe[0] ? probe_data : magic_data;
        len  = beamdetect.send_probe[0] ? sizeof(probe_data) : sizeof(magic_data);
        laser_send_data(LASER1_PIN, data, len);

        data = beamdetect.send_probe[1] ? probe_data : magic_data;
        len  = beamdetect.send_probe[1] ? sizeof(probe_data) : sizeof(magic_data);
        laser_send_data(LASER2_PIN, data, len);
    }

    // end of sensor detection window
    if((cnt % trig_switch_period) == 0)
    {
        // toggle sensor to listen on
        if(((cnt / trig_switch_period) % 2 ) == 0) {
            on_beam_result(1, beamdetect.beam_detected, beamdetect.probe_seen);
            uart_set_rx_pin(TRIG1_PIN);
        } else {
            on_beam_result(0, beamdetect.beam_detected, beamdetect.probe_seen);
            uart_set_rx_pin(TRIG2_PIN);
        }
    }

    // start of sensor detection window
    if(((cnt - trig_switch_dead) % trig_switch_period) == 0) {
        // make sure receive buffers have no stale data
        beamdetect_service_data();
        // ignore any pulses received between sensor switch and now
        beamdetect.beam_detected = 0;
        beamdetect.probe_seen = 0;
    }
}

/* This function is called on the main loop and handles bytes received on
 * the sensor that is currently listening.
 */
void beamdetect_service_data()
{
    uint32_t c;
    while((c = uart_getc()) != UART_EOF)
    {
        STATIC_ASSERT(sizeof(magic_data) == sizeof(probe_data));
        static const int len = sizeof(magic_data) / sizeof(magic_data[0]);
        // Simply looks for the last byte in the magic/probe sequence
        if(c == magic_data[len - 1]) {
            beamdetect.beam_detected = 1;
        } else if (c == probe_data[len - 1]) {
            beamdetect.probe_seen = 1;
        }
    }
}

void beamdetect_set_handlers( beamdetect_change_handler_t*      beam_handler,
                              beamdetect_seen_probe_handler_t*  probe_handler,
                              void* context)
{
    beamdetect.handler_context = context;
    beamdetect.beam_change_handler = beam_handler;
    beamdetect.seen_probe_handler = probe_handler;
}

void beamdetect_send_probe(uint8_t laser_index, uint8_t send_probe)
{
    beamdetect.send_probe[laser_index] = send_probe;
}


/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

void on_beam_result(uint32_t beam_index, uint32_t detected, uint32_t probe_seen)
{
    static uint32_t last_beam_states[2] = {0xFFFFFFFF, 0xFFFFFFFF};
    static uint32_t last_probe_seen[2] =  {0xFFFFFFFF, 0xFFFFFFFF};

    if(detected != last_beam_states[beam_index]) {
        last_beam_states[beam_index] = detected;
        leds_write(beam_index == 0 ? LED3 : LED4, detected);
        beamdetect.beam_change_handler(beam_index, detected, beamdetect.handler_context);
    }

    if(probe_seen != last_probe_seen[beam_index]) {
        last_probe_seen[beam_index] = probe_seen;
        beamdetect.seen_probe_handler(beam_index, probe_seen, beamdetect.handler_context);
    }

}
