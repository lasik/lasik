#ifndef UART_H__
#define UART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */

#define UART_EOF                        0xFFFFFFFF

/* ============================== GLOBAL VARS =============================== */
/* ============================== DATA TYPES ================================ */
/* ========================== FUNCTION PROTOTYPES =========================== */

void UARTE0_UART0_IRQHandler(void);

void uart_init(void);

void uart_set_tx_pin(uint8_t pin);
void uart_disconnect_tx_pin(void);
/*
 * Send some bytes over the uart
 * WARNING: data must not be located in flash
 */
void uart_transmit(const uint8_t * data, uint32_t len);

void uart_set_rx_pin(uint8_t pin);
uint32_t uart_getc(void);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // UART_H__
