#ifndef BATTERY_H__
#define BATTERY_H__

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */

typedef void battery_v_change_handler_t(uint16_t voltage_mv, void* context);

/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

void battery_init(void);
void battery_set_handler(battery_v_change_handler_t* handler, void* context);

void battery_s_tick(uint32_t cnt);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // BATTERY_H__
