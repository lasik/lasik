#include <stdint.h>
#include <string.h>

#include "nrf.h"
#include "nrf_drv_saadc.h"

#include "app_error.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "pinout.h"

#include "battery.h"

/* this code was hacked together from some democode without deep understanding
 * of what's going on exactly. Seems to work :)
 */

/* =============================== CONSTANTS ================================ */

#define V_BAT_CALIBRATION 7.016     // Vbat_mV / adc_count
#define SAMPLES_IN_BUFFER 5         // report after every n samples

/* ============================== DATA TYPES ================================ */

struct battery {
    nrf_saadc_value_t               m_buffer_pool[2][SAMPLES_IN_BUFFER];
    battery_v_change_handler_t *    voltage_change_handler;
    void *                          voltage_change_context;
};

/* ============================== GLOBAL VARS =============================== */

static struct battery battery;

/* ========================== FUNCTION PROTOTYPES =========================== */

void saadc_callback(nrf_drv_saadc_evt_t const * p_event);
void saadc_init(void);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

void battery_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(V_SENSE_AIN);

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(battery.m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(battery.m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
}

void battery_set_handler(battery_v_change_handler_t* handler, void* context)
{
    battery.voltage_change_context = context;
    battery.voltage_change_handler = handler;
}

void battery_s_tick(uint32_t cnt)
{
    (void)cnt;
    nrf_drv_saadc_sample();
}

/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer,
                                                SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        uint16_t sum = 0;
        for (int i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
            sum += p_event->data.done.p_buffer[i];
        }
        uint16_t voltage_mV = V_BAT_CALIBRATION * sum / SAMPLES_IN_BUFFER;
        NRF_LOG_INFO("Battery voltage = %dmV", voltage_mV);

        battery.voltage_change_handler(voltage_mV, battery.voltage_change_context);
    }
}
