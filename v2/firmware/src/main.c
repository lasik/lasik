#include <stdint.h>
#include <string.h>
#include "nrf.h"

#include "app_timer.h"

#include "app_error.h"

#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_gpio.h"

#include "pinout.h"
#include "leds.h"
#include "uart.h"
#include "laser.h"
#include "beamdetect.h"
#include "bluetooth.h"
#include "ble_lasik.h"
#include "battery.h"

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */
/* ============================== GLOBAL VARS =============================== */

BLE_LASIK_DEF(m_lasik); // Define Lasik Service instance.

APP_TIMER_DEF(ms_tick_timer);

/* ========================== FUNCTION PROTOTYPES =========================== */

static void log_init(void);
static void analog_init(void);
static void timer_init(void);
static void lasik_service_init(void);
static void power_management_init(void);

static void idle_state_handle(void);
static void ms_tick_handle(void* p);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

int main(void)
{
    // Initialize.
    log_init();
    NRF_LOG_INFO("Initialised log");

    analog_init();
    leds_init();
    battery_init();
    battery_set_handler(ble_lasik_set_v_bat, (void*)&m_lasik);

    uart_init();
    laser_init();
    beamdetect_init();
    beamdetect_set_handlers( ble_lasik_beamdetect_handler,
                             ble_lasik_seen_probe_handler,
                             (void*)&m_lasik);

    timer_init();
    power_management_init();

    bluetooth_init();
    lasik_service_init();
    ble_uuid_t adv_uuids[] = {{LASIK_UUID_SERVICE, m_lasik.uuid_type}};
    bluetooth_start(adv_uuids, ARRAY_SIZE(adv_uuids));

    NRF_LOG_INFO("Completed initialisation");
    NRF_LOG_INFO("Entering main loop");

    // Enter main loop.
    for (;;)
    {
        beamdetect_service_data();
        idle_state_handle();
    }
}


/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */


static void analog_init(void)
{
    // enable power to the analog section
    nrf_gpio_cfg_output(V_AN_EN_PIN);
    nrf_gpio_pin_write(V_AN_EN_PIN, 1);
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timer_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // initialise the tick timer
    app_timer_create( &ms_tick_timer, APP_TIMER_MODE_REPEATED, ms_tick_handle);
    app_timer_start( ms_tick_timer, APP_TIMER_TICKS(1), NULL);
}


static void lasik_service_init(void)
{
    ret_code_t         err_code;

    err_code = ble_lasik_init( &m_lasik,
                               laser_set_seq_fromdata,
                               beamdetect_send_probe );
    APP_ERROR_CHECK(err_code);
}


static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next event.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static void ms_tick_handle(void* p) {
    static uint32_t cnt;
    (void)p;
    cnt++; // overflow in 2^32 ms = 50 days

    beamdetect_ms_tick(cnt);

    if((cnt % 500) == 0)
    {
        leds_toggle(LED1);
    }

    if((cnt % 1000) == 0)
    {
        battery_s_tick(cnt);
    }
}
