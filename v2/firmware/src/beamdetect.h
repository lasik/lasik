#ifndef BEAMDETECT_H__
#define BEAMDETECT_H__

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== CONSTANTS ================================ */
/* ============================== DATA TYPES ================================ */

typedef void beamdetect_change_handler_t( uint8_t sensor_index,
                                          uint8_t beamstate,
                                          void *  context);

typedef void beamdetect_seen_probe_handler_t( uint8_t sensor_index,
                                              uint8_t seen_probe,
                                              void *  context);

/* ============================== GLOBAL VARS =============================== */
/* ========================== FUNCTION PROTOTYPES =========================== */

void beamdetect_init(void);
void beamdetect_set_handlers( beamdetect_change_handler_t *     beam_handler,
                              beamdetect_seen_probe_handler_t * probe_handler,
                              void* context);
void beamdetect_ms_tick(uint32_t cnt);
void beamdetect_service_data(void);
void beamdetect_send_probe(uint8_t laser_index, uint8_t send_probe);

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif // BEAMDETECT_H__
