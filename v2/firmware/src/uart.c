#include <stdint.h>
#include <string.h>
#include "nrf.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_uarte.h"

#include "pinout.h"

#include "uart.h"

/* =============================== CONSTANTS ================================ */

#define UARTE_IRQ_NUMBER                (uint8_t)((uint32_t)(NRF_UARTE0) >> 12)
#define RX_BUF_LEN                      16

// readable register aliases
static volatile uint32_t * const uart_tx_pin_select_reg = &NRF_UARTE0->PSEL.TXD;
static volatile uint32_t * const uart_rx_pin_select_reg = &NRF_UARTE0->PSEL.RXD;

/* ============================== DATA TYPES ================================ */

typedef struct {
    uint8_t buffer[RX_BUF_LEN + 1];
    uint32_t next_write;
    uint32_t last_read;
} stream_t;

struct uart {
    uint8_t rx_bufferA[RX_BUF_LEN];  // used by the peripheral
    uint8_t rx_bufferB[RX_BUF_LEN];  // used by the peripheral
    volatile stream_t rx_stream;
};

/* ============================== GLOBAL VARS =============================== */

static struct uart uart;

/* ========================== FUNCTION PROTOTYPES =========================== */

static void interrupt_enable(void);
static void interrupt_disable(void);

static void stream_init(volatile stream_t* s);
static void stream_putc(volatile stream_t* s, uint8_t byte);
static uint32_t stream_getc(volatile stream_t* s);

/* =================== EXPORTED FUNCTION IMPLEMENTATIONS ==================== */

void uart_init(void)
{
    // initialize the rx_stream
    stream_init(&uart.rx_stream);

    // configure and enable UARTE peripheral
    nrf_uarte_baudrate_set(
        NRF_UARTE0,
        (nrf_uarte_baudrate_t)NRFX_UARTE_DEFAULT_CONFIG_BAUDRATE);
    nrf_uarte_configure(
        NRF_UARTE0,
        (nrf_uarte_parity_t)NRFX_UARTE_DEFAULT_CONFIG_PARITY,
        NRF_UARTE_HWFC_DISABLED);
    nrf_uarte_txrx_pins_set(
        NRF_UARTE0,
        NRF_UARTE_PSEL_DISCONNECTED,    // tx pin set on send
        NRF_UARTE_PSEL_DISCONNECTED);   // rx pin alternates
    nrf_uarte_enable(NRF_UARTE0);

    // enable UARTE interrupts
    nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ENDRX);
    nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_RXTO);
    nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_RXDRDY);
    interrupt_enable();
    NRFX_IRQ_PRIORITY_SET( UARTE_IRQ_NUMBER,
                           NRFX_UARTE_DEFAULT_CONFIG_IRQ_PRIORITY );
    NRFX_IRQ_ENABLE(UARTE_IRQ_NUMBER);

    // set up RX end
    interrupt_disable();
    nrf_uarte_rx_buffer_set(NRF_UARTE0, uart.rx_bufferA, RX_BUF_LEN);
    nrf_uarte_task_trigger(NRF_UARTE0, NRF_UARTE_TASK_STARTRX);
    nrf_uarte_rx_buffer_set(NRF_UARTE0, uart.rx_bufferB, RX_BUF_LEN);
    nrf_uarte_shorts_enable(NRF_UARTE0, NRF_UARTE_SHORT_ENDRX_STARTRX);
    interrupt_enable();
}


void uart_set_tx_pin(uint8_t pin)
{
    *uart_tx_pin_select_reg = pin;
}


void uart_disconnect_tx_pin()
{
    *uart_tx_pin_select_reg = NRF_UARTE_PSEL_DISCONNECTED;
}


void uart_set_rx_pin(uint8_t pin)
{
    *uart_rx_pin_select_reg = pin;
}


void uart_transmit(const uint8_t * data, uint32_t len)
{
    interrupt_disable();
    {
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ENDTX);
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_TXSTOPPED);
        nrf_uarte_tx_buffer_set(NRF_UARTE0, data, len);
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ENDTX);
        nrf_uarte_task_trigger(NRF_UARTE0, NRF_UARTE_TASK_STARTTX);
        while( !nrf_uarte_event_check(NRF_UARTE0, NRF_UARTE_EVENT_ENDTX) );
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ENDTX);

    }
    interrupt_enable();
}


uint32_t uart_getc(void)
{
    return stream_getc(&uart.rx_stream);
}


void UARTE0_UART0_IRQHandler(void)
{
    static uint8_t *readyBuffer = uart.rx_bufferA;
    if (nrf_uarte_event_check(NRF_UARTE0, NRF_UARTE_EVENT_ENDRX))
    {
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ENDRX);

        // handle readyBuffer contents; (read up to NRF_UARTE0->RXD.AMOUNT bytes)
        // remark -> expect quite a bunch of garbage due to pwm
        for(uint8_t i = 0; i < NRF_UARTE0->RXD.AMOUNT; i++)
        {
            stream_putc(&uart.rx_stream, readyBuffer[i]);
        }

        // For debugging
        // NRF_LOG_INFO("%d   %X %X %X %X", NRF_UARTE0->RXD.AMOUNT,
        //             readyBuffer[0], readyBuffer[1], readyBuffer[2], readyBuffer[3]);

        // reschedule the readybuffer after the current one
        nrf_uarte_rx_buffer_set(NRF_UARTE0, readyBuffer, RX_BUF_LEN);

        // next time the other buffer will be ready
        readyBuffer = ( readyBuffer == uart.rx_bufferA )
                        ? uart.rx_bufferB
                        : uart.rx_bufferA;
    }
    if (nrf_uarte_event_check(NRF_UARTE0, NRF_UARTE_EVENT_RXDRDY))
    {
        // on any byte received -> switch to next buffer immediately in order to
        // start handling the data asap. the _STOPRX task will trigger a _ENDRX
        // event which then allows us to process the buffer.
        nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_RXDRDY);
        nrf_uarte_task_trigger(NRF_UARTE0, NRF_UARTE_TASK_STOPRX);
    }
}


/* ==================== STATIC FUNCTION IMPLEMENTATIONS ===================== */

static void interrupt_enable()
{
    nrf_uarte_int_enable( NRF_UARTE0,
                          NRF_UARTE_INT_ENDRX_MASK |
                          NRF_UARTE_INT_RXTO_MASK  |
                          NRF_UARTE_INT_RXDRDY_MASK );
}


static void interrupt_disable()
{
    nrf_uarte_int_disable( NRF_UARTE0,
                           NRF_UARTE_INT_ENDRX_MASK |
                           NRF_UARTE_INT_RXTO_MASK  |
                           NRF_UARTE_INT_RXDRDY_MASK );
}


static void stream_init(volatile stream_t* s)
{
    s->next_write = 0;
    s->last_read = RX_BUF_LEN;
}


static void stream_putc(volatile stream_t* s, uint8_t byte)
{
    if(s->next_write != s->last_read)
    {
        s->buffer[s->next_write] = byte;
        s->next_write = (s->next_write + 1) % (RX_BUF_LEN + 1);
    }
    else
    {
        NRF_LOG_WARNING("uart stream overflow: ignored byte = %X", byte);
    }
}

static uint32_t stream_getc(volatile stream_t* s)
{
    uint8_t byte;

    uint32_t next_read = (s->last_read + 1) % (RX_BUF_LEN + 1);
    if(next_read != s->next_write)
    {
        byte = s->buffer[next_read];    // order is important!
        s->last_read = next_read;       // order is important!
        return byte;
    }
    else
    {
        return UART_EOF;
    }
}
