put the required sdk in this folder or link to it with the name nRF_SDK_xx/

eg. as of writing:
    unzip nRF5_SDK_15.2.0_9412b96.zip
    ln -s nRF5_SDK_15.2.0_9412b96 nRF5_SDK_15 

this should allow easily upgrading the SDK for minor and patch versions by
updating the link to point to the new sdk.

for major versions you'll need to update the build configuration files
