#!/usr/bin/env bash

# generates a dot graph from sourcecode
# it only looks at files in the current folder
# it assumes the include statements in the c file is formatted in a standard way
# it assumes one .c <-> one .h with the same name
# probably fails in many circumstances

echo digraph {
grep '#include' *.c | grep -F "$(ls *.c | sed -r 's/\.c/\.h/')" | sed 's/\(.*\)\.c:#include "\(.*\)\.h".*/\1 -> \2/' | grep -v "^\(.*\) -> \1$"
echo }
