from datetime import datetime
import sys

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

is_a_tty = sys.stdout.isatty()

def _output(*args, **kwargs):
    if kwargs["enabled"] == False:
        return
    fstring = ""
    if kwargs["color"] != None and is_a_tty:
        fstring += kwargs["color"]
    fstring += str(datetime.now().time()) + " "
    if kwargs["prefix"] != None:
        fstring += kwargs["prefix"] + " "
    fstring += args[0]
    if kwargs["color"] != None and is_a_tty:
        fstring += colors.ENDC

    for kw in ["enabled", "color", "prefix"]:
        kwargs.pop(kw)
    print(fstring, *args[1:], **kwargs)


def get(color = None, prefix = None, enabled = True):
    def log_f(*args, color = color, prefix = prefix, enabled = enabled, **kwargs):
        _output(*args, color = color, prefix = prefix, enabled = enabled, **kwargs)
    return log_f
