#!/usr/bin/env python3

from gi.repository import GLib

import beamservicelayer
import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(beam)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(beam)", color = lasiklog.colors.OKBLUE)
debug = lasiklog.get(prefix="DEBUG(beam)", enabled = False)

####################################################################################################

@layer.add_handlers
class BeamBridge(layer.LayerObject):
    delay_ms = 300
    def __init__(self, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("online", None), ("offline", None), ("probe", None)])

        self.matched_lasers = set()
        self.unmatched_online_lasers = set()
        self.restart_match_request = False
        self.reset_match = False
        self.remaining_match_lasers = []
        self.timeout_eventsource = None
        self.n_beams_created = 0

    @layer.handles_event("online")
    def online_handler(self, sender, data):
        if isinstance(sender, beamservicelayer.Laser):
            if sender not in self.matched_lasers:
                self.unmatched_online_lasers.add(sender)
                self.match_beams()
        elif isinstance(sender, beamservicelayer.Sensor):
            pass
        else:
            assert False

    @layer.handles_event("offline")
    def offline_handler(self, sender, data):
        if sender in self.unmatched_online_lasers:
            self.unmatched_online_lasers.discard(sender)

    @layer.handles_event("probe")
    def probe_handler(self, sender, data):
        is_on = data["is_on"]
        if is_on and len(self.remaining_match_lasers) == 0:
            warn("no beammatching in progress, unexpected probe signal received: {}".format(data))
        elif is_on:
            if self.timeout_eventsource != None:
                GLib.source_remove(self.timeout_eventsource)
                self.timeout_eventsource = None
            laser = self.remaining_match_lasers[0]
            sensor = sender
            Beam(laser, sensor, self.dispatcher)
            self.n_beams_created += 1
            self.matched_lasers.add(laser)
            log("amount of beams created so far = {}".format(self.n_beams_created))
            assert laser in self.unmatched_online_lasers
            self.unmatched_online_lasers.discard(laser)
            self.match_next()

    def on_timeout(self):
        debug("on_timeout")
        self.timeout_eventsource = None
        self.match_next()
        return False

    def match_next(self):
        # such a mess (>_<)
        # TODO clean up
        def disable_probe(probing):
            if probing.is_online():
                probing.set_probe(False)
        def enable_probe(probing):
            if probing.is_online():
                probing.set_probe(True)
        if self.restart_match_request:
            debug("restart")
            # restart requested
            self.restart_match_request = False
            self.reset_match = True
            if len(self.remaining_match_lasers) > 0:
                debug("restart -> len > 0   {}".format(len(self.remaining_match_lasers)))
                # a scan was in progress
                probing = self.remaining_match_lasers[0]
                # needs delay to prevent too fast writes (bluez InProgress error)
                GLib.timeout_add(BeamBridge.delay_ms, disable_probe, probing)
                # make sure its not run immediately after last set_probe(False)
                self.timeout_eventsource = GLib.timeout_add(2 * BeamBridge.delay_ms, self.on_timeout)
            else:
                debug("restart -> len == 0")
                self.match_next()
        else:
            debug("not restart")
            if self.reset_match:
                debug("not restart -> reset_match")
                self.reset_match = False
                self.remaining_match_lasers = list(self.unmatched_online_lasers)
                if len(self.remaining_match_lasers) > 0:
                    enable_probe(self.remaining_match_lasers[0])
                    self.timeout_eventsource = GLib.timeout_add(BeamBridge.delay_ms, self.on_timeout)
                else:
                    warn("no unmatched online lasers to match")
            else:
                assert len(self.remaining_match_lasers) > 0
                debug("not restart -> not reset_match: len = {}".format(
                        len(self.remaining_match_lasers)))
                probing = self.remaining_match_lasers[0]
                # needs delay to prevent bluez InProgress errors
                GLib.timeout_add(BeamBridge.delay_ms, disable_probe, probing)
                self.remaining_match_lasers = self.remaining_match_lasers[1:]
                if len(self.remaining_match_lasers) > 0:
                    enable_probe(self.remaining_match_lasers[0])
                    self.timeout_eventsource = (
                        GLib.timeout_add(BeamBridge.delay_ms, self.on_timeout))
                else:
                    debug("not restart -> not reset_match -> len == 0")
                    # we're done until more lasers or sensors appear
                    lasers_left = len(self.unmatched_online_lasers)
                    if lasers_left == 0:
                        log("finished matching, all lasers matched")
                    else:
                        warn("finished matching, but {} laser(s) left!".format(lasers_left))


    def match_beams(self):
        self.restart_match_request = True       
        if self.timeout_eventsource == None:
            log("matching beams...")
            self.match_next()
        return False

@layer.add_handlers
class Beam(layer.LayerObject):
    def __init__(self, laser, sensor, dispatcher):
        super().__init__(dispatcher, [("online", laser),  ("offline", laser),
                                      ("online", sensor), ("offline", sensor),
                                      ("sensor", sensor)])
        self.name = laser.get_name() + "-" + sensor.get_name()
        self.online = False
        self.is_on = False
        self.laser = laser
        self.sensor = sensor
        log("creating {} {}".format(type(self).__name__, self.name))
        self.emit_event("created")
        self._check_online()

    @layer.handles_event("sensor")
    def sensor_handler(self, sender, data):
        if not self.online:
            warn("received signal on Beam that is offline: {}".format(self.name))
        last_is_on = self.is_on
        self.is_on = data["is_on"]
        if self.is_on != last_is_on:
            self.emit_event("beam", {"is_on": self.is_on})

    @layer.handles_event("online")
    @layer.handles_event("offline")
    def online_offline_handler(self, sender, data):
        self._check_online()

    def _check_online(self):
        last_state = self.online
        self.online = self.laser.is_online() and self.sensor.is_online()
        if self.online and not last_state:
            self.emit_event("online")
        elif not self.online and last_state:
            self.emit_event("offline")

    def get_name(self):
        return self.name

    def set(self, sequence):
        self.laser.set(sequence)


####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    import dbus

    import dbuslayer
    import bluezlayer
    import lasikservicelayer
    import beamservicelayer
    import lasiklayer

    def sink(event_name, source, data):
        if "get_name" in source.__dir__():
            name = source.get_name()
        else:
            name = "a " + type(source).__name__
        msg = "event '{}' from {}".format(event_name, name)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        print(msg)

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    log("setting up dispatchers")
    # chain the layer together
    beamdispatcher = layer.Dispatcher(sink, name = "beam")
    beamservicedispatcher = layer.Dispatcher(beamdispatcher.on_event, name = "beamservice")
    lasikdispatcher = layer.Dispatcher(beamservicedispatcher.on_event, name = "lasik")
    lasikservicedispatcher = layer.Dispatcher(lasikdispatcher.on_event, name = "lasikservice")
    bluezdispatcher = layer.Dispatcher(lasikservicedispatcher.on_event, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first beamlayer object")
    _ = BeamBridge(beamdispatcher)
    log("set up first beamservicelayer object")
    _ = beamservicelayer.BeamServiceBridge(beamservicedispatcher)
    log("set up first lasiklayer object")
    _ = lasiklayer.LasikBridge(lasikdispatcher)
    log("set up first lasikservicelayer object")
    _ = lasikservicelayer.LasikServiceBridge(lasikservicedispatcher)
    log("set up first bluezlayer object")
    _ = bluezlayer.BluezBridge(bluezdispatcher)
    log("set up first dbuslayer object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dbusdispatcher.on_event("stop", external = True)

