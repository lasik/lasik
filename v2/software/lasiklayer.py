#!/usr/bin/env python3

import json

import lasikservicelayer
import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(lasik)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(lasik)", color = lasiklog.colors.OKBLUE)
debug = lasiklog.get(prefix="DEBUG(lasik)", enabled = False)

####################################################################################################

from lasikservicelayer import Index

class Config:
    with open("config.json") as f:
        cfg = json.load(f)
        aliases     = cfg["aliases"]

@layer.add_handlers
class LasikBridge(layer.LayerObject):
    def __init__(self, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("created", None)])
        self.lasiks = {} #path:Lasik()

    @layer.handles_event("created")
    def created_handler(self, sender, data):
        assert("path" in data)
        path = data["path"]
        if isinstance(sender, lasikservicelayer.LasikDevice):
            if path in self.lasiks:
                warn("previously forgotten lasik reappeard on {}".format(path))
                # connect the new LasikDevice to an old Lasik object
                lasik = self.lasiks[path]
                # warning: these subscribe_to_event calls form duplicate code with constructor
                lasik.subscribe_to_event("connected", sender)
                lasik.subscribe_to_event("disconnected", sender)
            else:
                # its a new one
                lasik = Lasik(sender, self.dispatcher)
                self.lasiks[path] = lasik
        elif isinstance(sender, lasikservicelayer.Sensor):
            assert("index" in data)
            index = data["index"]
            assert(index in [Index.SENSOR1, Index.SENSOR2])
            lasik = self._get_parent_lasik(path)
            lasik._set_sensor_service(index, sender)
            lasik.subscribe_to_event("sensor_state", sender)
            lasik.subscribe_to_event("destroyed", sender)
        elif isinstance(sender, lasikservicelayer.BatteryVoltage):
            lasik = self._get_parent_lasik(path)
            lasik._set_voltage_service(sender)
            lasik.subscribe_to_event("voltage", sender)
            lasik.subscribe_to_event("destroyed", sender)
        elif isinstance(sender, lasikservicelayer.Laser):
            assert("index" in data)
            index = data["index"]
            assert(index in [Index.LASER1, Index.LASER2])
            lasik = self._get_parent_lasik(path)
            lasik._set_laser_service(index, sender)
            lasik.subscribe_to_event("destroyed", sender)
        elif isinstance(sender, lasikservicelayer.SeenProbe):
            assert("index" in data)
            index = data["index"]
            assert(index in [Index.SENSOR1, Index.SENSOR2])
            lasik = self._get_parent_lasik(path)
            lasik._set_seen_probe_service(index, sender)
            lasik.subscribe_to_event("seen_probe", sender)
            lasik.subscribe_to_event("destroyed", sender)
        elif isinstance(sender, lasikservicelayer.SendProbe):
            assert("index" in data)
            index = data["index"]
            assert(index in [Index.LASER1, Index.LASER2])
            lasik = self._get_parent_lasik(path)
            lasik._set_send_probe_service(index, sender)
            lasik.subscribe_to_event("destroyed", sender)
        else:
            warn("'created' event received from unknown lasikservice '{}'".format(
                 type(sender).__name__))

    def _get_parent_lasik(self, path):
        matches = [lasik for p,lasik in self.lasiks.items() if path.startswith(p)]
        assert(len(matches) == 1)
        return matches[0]


####################################################################################################

@layer.add_handlers
class Lasik(layer.LayerObject):
    def __init__(self, sender, dispatcher):
        address = sender.get_address()
        if address in Config.aliases:
            name = Config.aliases[address]
            log("creating {} '{}' from '{}'".format(type(self).__name__, name, address))
        else:
            name = address
            log("creating {} '{}'".format(type(self).__name__, name))
        super().__init__(dispatcher, [("connected", sender), ("disconnected", sender)])
        self.name = name
        self.lasikDevice = sender
        self.sensors = [None, None]
        self.lasers = [None, None]
        self.seen_probes = [None, None]
        self.send_probes = [None, None]
        self.batteryVoltage = None
        self.connected = False
        self.online = False
        self.voltage_V = None

    @layer.handles_event("connected")
    def connected_handler(self, sender, data):
        self.connected = True
        self._check_online()

    @layer.handles_event("disconnected")
    def disconnected_handler(self, sender, data):
        assert(self.connected == True)
        self.connected = False
        self._check_online()

    @layer.handles_event("sensor_state")
    def sensor_state_handler(self, sender, data):
        assert("index" in data)
        assert("sensor_state" in data)
        index = data["index"]
        sensor_state = data["sensor_state"]
        debug("{} index{} sensor_state = {}".format(self.name, index, sensor_state))
        self.emit_event("sensor", {"index":index, "is_on":sensor_state})

    @layer.handles_event("voltage")
    def voltage_handler(self, sender, data):
        self.voltage_V = data
        debug("{} V_bat = {}V".format(self.name, self.voltage_V))
        if self.voltage_V <= 3.4:
            warn("{}: LOW BATTERY! v_bat={}V".format(self.name, self.voltage_V))

    @layer.handles_event("seen_probe")
    def seen_probe_handler(self, sender, data):
        assert("index" in data)
        assert("seen_probe" in data)
        index = data["index"]
        seen_probe = data["seen_probe"]
        debug("{} index{} probe = {}".format(self.name, index, seen_probe))
        self.emit_event("probe", {"index":index, "is_on":seen_probe})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        debug("destroying {} from {}".format(type(sender).__name__, self.name))
        if sender == self.sensors[Index.SENSOR1]:
            self.sensors[Index.SENSOR1] = None
        elif sender == self.sensors[Index.SENSOR2]:
            self.sensors[Index.SENSOR2] = None
        elif sender == self.lasers[Index.LASER1]:
            self.lasers[Index.LASER1] = None
        elif sender == self.lasers[Index.LASER2]:
            self.lasers[Index.LASER2] = None
        elif sender == self.seen_probes[Index.SENSOR1]:
            self.seen_probes[Index.SENSOR1] = None
        elif sender == self.seen_probes[Index.SENSOR2]:
            self.seen_probes[Index.SENSOR2] = None
        elif sender == self.send_probes[Index.LASER1]:
            self.send_probes[Index.LASER1] = None
        elif sender == self.send_probes[Index.LASER2]:
            self.send_probes[Index.LASER2] = None
        elif sender == self.batteryVoltage:
            self.batteryVoltage = None
        else:
            assert(False)
        self._check_online()

    def get_voltage_V(self):
        return self.voltage_V

    def get_name(self):
        return self.name

    def set_laser(self, index, value):
        # warning this might throw an error when called immediately after connection
        # org.bluez.Error.Failed: Not connected
        assert index in [Index.LASER1, Index.LASER2]
        assert self.online
        self.lasers[index].set(value)

    def set_probe(self, index, enabled):
        if enabled:
            self.send_probes[index].enable()
        else:
            self.send_probes[index].disable()

    def set_laser_sequence(self, index, sequence):
        # warning this might throw an error when called immediately after connection
        # org.bluez.Error.Failed: Not connected
        assert index in [Index.LASER1, Index.LASER2]
        assert self.online
        self.lasers[index].set_sequence(sequence)

    def _set_sensor_service(self, index, sensor_service):
        debug("adding SENSOR{} to Lasik '{}'".format(index, self.name))
        assert(index in [Index.SENSOR1, Index.SENSOR2])
        assert(self.sensors[index] == None)
        self.sensors[index] = sensor_service
        self._check_online()

    def _set_laser_service(self, index, laser_service):
        debug("adding LASER{} to Lasik '{}'".format(index, self.name))
        assert(index in [Index.LASER1, Index.LASER2])
        assert(self.lasers[index] == None)
        self.lasers[index] = laser_service
        self._check_online()

    def _set_seen_probe_service(self, index, seen_probe_service):
        debug("adding SEEN_PROBE{} to Lasik '{}'".format(index, self.name))
        assert(index in [Index.LASER1, Index.LASER2])
        assert(self.seen_probes[index] == None)
        self.seen_probes[index] = seen_probe_service
        self._check_online()

    def _set_send_probe_service(self, index, send_probe_service):
        debug("adding SEEN_PROBE{} to Lasik '{}'".format(index, self.name))
        assert(index in [Index.LASER1, Index.LASER2])
        assert(self.send_probes[index] == None)
        self.send_probes[index] = send_probe_service
        self._check_online()

    def _set_voltage_service(self, voltage_service):
        debug("adding V_bat to Lasik '{}'".format(self.name))
        assert(self.batteryVoltage == None)
        self.batteryVoltage = voltage_service
        self._check_online()

    def _check_online(self):
        result = ( self.connected                          and
                   self.sensors[Index.SENSOR1]     != None and
                   self.sensors[Index.SENSOR2]     != None and
                   self.lasers[Index.LASER1]       != None and
                   self.lasers[Index.LASER2]       != None and
                   self.seen_probes[Index.SENSOR1] != None and
                   self.seen_probes[Index.SENSOR2] != None and
                   self.send_probes[Index.SENSOR1] != None and
                   self.send_probes[Index.SENSOR2] != None and
                   self.batteryVoltage             != None )
        if self.online and not result:
            self.online = False
            self._on_offline()
        elif not self.online and result:
            self.online = True
            self._on_online()

    def _on_offline(self):
        log("{} OFFLINE".format(self.name))
        self.emit_event("offline")

    def _on_online(self):
        log("{} ONLINE".format(self.name))
        self.emit_event("online")
        for service in self.sensors + self.seen_probes + [self.batteryVoltage]:
            service.start_notify()

####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    import dbus
    import dbuslayer
    import bluezlayer

    def sink(event_name, source, data):
        msg = "fake_on_event() '{}' from a {}".format(event_name, type(source).__name__)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        print(msg)

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    log("setting up dispatchers")
    # chain the layers together
    lasikdispatcher = layer.Dispatcher(sink, name = "lasik")
    lasikservicedispatcher = layer.Dispatcher(lasikdispatcher.on_event, name = "lasikservice")
    bluezdispatcher = layer.Dispatcher(lasikservicedispatcher.on_event, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first lasik-layer object")
    _ = LasikBridge(lasikdispatcher)
    log("set up first lasikservice object")
    _ = lasikservicelayer.LasikServiceBridge(lasikservicedispatcher)
    log("set up first bluez object")
    _ = bluezlayer.BluezBridge(bluezdispatcher)
    log("set up first dbus object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dbusdispatcher.on_event("stop", external = True)
