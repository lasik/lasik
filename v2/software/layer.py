#!/usr/bin/env python3

import lasiklog
warn =  lasiklog.get(prefix="WARNING(layer)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(layer)", color = lasiklog.colors.OKBLUE, enabled = False)
debug = lasiklog.get(prefix="DEBUG(layer)", enabled = False)

class Dispatcher():
    def __init__(self, sink, name = ""):
        self._sink = sink
        self.name = name
        self._observers = {}

    def on_event(self, event_name, lower_object = None, data = None, external = False):
        # events should usually come from lower objects, except external events
        assert(external or (lower_object != None))
        # now check for subscribed layer objects
        subscribers = self._get_observers(event_name, lower_object)
        for sub in subscribers.copy(): # copy since the set will change
            sub._on_event(event_name, lower_object, data)

    def _get_observers(self, event_name, lower_object):
        # observers listening to all senders
        result = self._observers.get((event_name, None), set())
        # observers listening to a specific sender only
        if (lower_object != None):
            result = result | self._observers.get((event_name,lower_object), set())
        return result

    def _subscribe_to_event(self, layer_object, event_name, sender = None):
        assert(layer_object not in self._get_observers(event_name, sender))
        self._observers.setdefault((event_name, sender), set()).add(layer_object)

    def _register_object(self, layer_object, event_subscriptions):
        for event_name, sender in event_subscriptions:
            self._subscribe_to_event(layer_object, event_name, sender)

    def _unregister_object(self, layer_object):
        for event, listeners in self._observers.items():
            listeners.discard(layer_object)

    def emit_event(self, event_name, source, data):
        msg = "event '{}' from a {}".format(event_name, type(source).__name__)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        log(msg)
        self._sink(event_name, source, data)


class LayerObject():
    handlers = {}
    def __init__(self, dispatcher, event_subscriptions):
        log("creating new object of type {}".format(type(self).__name__))
        self.dispatcher = dispatcher
        self.dispatcher._register_object(self, event_subscriptions)
        if len(event_subscriptions) == 0:
            warn("created layer object without event inputs: lol")

    def _on_event(self, event_name, lower_object, data):
        log("{}._on_event() '{}' from {}".format(
            type(self).__name__, event_name, type(lower_object).__name__))
        if event_name in type(self).handlers:
            type(self).handlers[event_name](self, lower_object, data)
        else:
            warn("{}._on_event() no handler found for '{}'".format(type(self).__name__, event_name))

    def emit_event(self, event_name, data = None):
        self.dispatcher.emit_event(event_name, self, data = data)

    def subscribe_to_event(self, event, sender = None):
        log("{}.subscribe_to_event('{}') after creation".format(type(self).__name__, event))
        self.dispatcher._subscribe_to_event(self, event, sender)

    def _clean_up(self):
        self.dispatcher._unregister_object(self)

def add_handlers(bare_class):
    """class decorator to be used on LayerObject subclasses"""
    # inherit any existing handlers, im assuming this class always has a base class
    # and that each step in the inheritance chain has had this decorator applied
    handlers = bare_class.__base__.handlers.copy()

    # add new handlers in the current class
    for name, method in bare_class.__dict__.items():
        if hasattr(method, "_event_name_handled"):
            for event_name in method._event_name_handled:
                handlers[event_name] = method
    bare_class.handlers = handlers
    log("{} handlers added to {}".format(list(handlers.keys()), bare_class.__name__))
    return bare_class

def handles_event(event_name):
    """
    decorator to be used on methods added to LayerObject subclasses specifying the event_name
    as a string parameter
    """
    def decorator(func):
        def wrapper(self, lower_object, data):
            func(self, lower_object, data)

        if "_event_name_handled" not in func.__dict__:
            wrapper._event_name_handled = []
        else:
            wrapper._event_name_handled = func._event_name_handled
        wrapper._event_name_handled.append(event_name)
        return wrapper
    return decorator
