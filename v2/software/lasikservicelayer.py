#!/usr/bin/env python3

import json
import dbus

import bluezlayer
import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(lasikservice)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(lasikservice)", color = lasiklog.colors.OKBLUE, enabled = False)
debug = lasiklog.get(prefix="DEBUG(lasikservice)", enabled = False)

####################################################################################################

class Config:
    with open("config.json") as f:
        cfg = json.load(f)
        uuid_lasik  = cfg["UUID"]
        uuid_sensor1  = cfg["sensor_UUIDs"][0]
        uuid_sensor2  = cfg["sensor_UUIDs"][1]
        uuid_laser1 = cfg["laser_UUIDs"][0]
        uuid_laser2 = cfg["laser_UUIDs"][1]
        uuid_vbat   = cfg["vbat_UUID"]
        uuid_seen_probe1 = cfg["seen_probe_UUIDs"][0]
        uuid_seen_probe2 = cfg["seen_probe_UUIDs"][1]
        uuid_send_probe1 = cfg["send_probe_UUIDs"][0]
        uuid_send_probe2 = cfg["send_probe_UUIDs"][1]
    max_pwm_value = 4096

class Index:
    SENSOR1 = 0
    SENSOR2 = 1
    LASER1 = 0
    LASER2 = 1

@layer.add_handlers
class LasikServiceBridge(layer.LayerObject):
    def __init__(self, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("created", None)])
        self.has_adapter = False

    @layer.handles_event("created")
    def created_handler(self, sender, data):
        # check if the dbus object is on of interest
        if isinstance(sender, bluezlayer.Adapter):
            if self.has_adapter:
                warn("'created' event received for second adapter, ignoring this adapter'")
            else:
                self.has_adapter = True
                LSAdapter(sender, self.dispatcher)
        elif isinstance(sender, bluezlayer.Device):
            if sender.has_uuid(Config.uuid_lasik):
                LasikDevice(sender, self.dispatcher)
        elif isinstance(sender, bluezlayer.Characteristic):
            uuid = sender.get_uuid()
            if uuid == Config.uuid_sensor1:
                Sensor(Index.SENSOR1, sender, self.dispatcher)
            elif uuid == Config.uuid_sensor2:
                Sensor(Index.SENSOR2, sender, self.dispatcher)
            elif uuid == Config.uuid_laser1:
                Laser(Index.LASER1, sender, self.dispatcher)
            elif uuid == Config.uuid_laser2:
                Laser(Index.LASER2, sender, self.dispatcher)
            elif uuid == Config.uuid_vbat:
                BatteryVoltage(sender, self.dispatcher)
            elif uuid == Config.uuid_seen_probe1:
                SeenProbe(Index.SENSOR1, sender, self.dispatcher)
            elif uuid == Config.uuid_seen_probe2:
                SeenProbe(Index.SENSOR2, sender, self.dispatcher)
            elif uuid == Config.uuid_send_probe1:
                SendProbe(Index.LASER1, sender, self.dispatcher)
            elif uuid == Config.uuid_send_probe2:
                SendProbe(Index.LASER2, sender, self.dispatcher)
            else:
                warn("'created' event received from unknown characteristic (uuid='{}')".format(
                     uuid))
        else:
            warn("'created' event received from unknown class ({})".format(type(sender).__name__))
            assert(False)

    def _clean_up(self):
        if self.adapter != None:
            self.adapter.stop_discovery()
        super()._clean_up()

@layer.add_handlers
class LSAdapter(layer.LayerObject):
    def __init__(self, sender, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender)])
        self.bluez_adapter = sender
        sender.start_discovery()

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.bluez_adapter == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        sender.stop_discovery()
        self._clean_up()

@layer.add_handlers
class LasikDevice(layer.LayerObject):
    def __init__(self, sender, dispatcher):
        super().__init__(dispatcher, [("destroyed", sender),
                                      ("connected", sender),
                                      ("disconnected", sender)])
        self.address = sender.get_address()
        log("creating {} on '{}'".format(type(self).__name__, self.address))
        self.bluez_device = sender
        self.emit_event("created", {"path":sender.get_path()})
        self.bluez_device.connect()

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.bluez_device == sender) # just checkin
        debug("destroying {} on '{}'".format(type(self).__name__, self.address))
        self.emit_event("destroyed")
        self.bluez_device.disconnect()
        self._clean_up()

    @layer.handles_event("connected")
    def connected_handler(self, sender, data):
        assert(self.bluez_device == sender) # just checkin
        self.emit_event("connected")

    @layer.handles_event("disconnected")
    def disconnected_handler(self, sender, data):
        assert(self.bluez_device == sender) # just checkin
        self.emit_event("disconnected")
        self.bluez_device.connect()

    def get_address(self):
        return self.address

@layer.add_handlers
class Sensor(layer.LayerObject):
    def __init__(self, index, sender, dispatcher):
        debug("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender), ("value_notification",sender )])
        assert(index in [Index.SENSOR1, Index.SENSOR2])
        self.index = index
        self.gatt_char = sender
        self.emit_event("created", {"path":sender.get_path(), "index":self.index})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        self.emit_event("destroyed")
        self._clean_up()

    @layer.handles_event("value_notification")
    def value_notification_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        sensor_state = bool(data[0])
        debug("sensor_state {} = {}".format(self.index, sensor_state))
        self.emit_event("sensor_state", {"index":self.index, "sensor_state":sensor_state})

    def start_notify(self):
        self.gatt_char.start_notify()
        self.gatt_char.request_notification() # actively read it out


@layer.add_handlers
class BatteryVoltage(layer.LayerObject):
    def __init__(self, sender, dispatcher):
        debug("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender), ("value_notification", sender)])
        self.gatt_char = sender
        self.emit_event("created", {"path":sender.get_path()})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        self.emit_event("destroyed")
        self._clean_up()

    @layer.handles_event("value_notification")
    def value_notification_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        voltage = ((data[1] << 8) + data[0]) / 1000.0
        debug("v_bat = {}V".format(voltage))
        self.emit_event("voltage", voltage)

    def start_notify(self):
        self.gatt_char.start_notify()


@layer.add_handlers
class Laser(layer.LayerObject):
    def __init__(self, index, sender, dispatcher):
        debug("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender)])
        assert(index in [Index.LASER1, Index.LASER2])
        self.index = index
        self.gatt_char = sender
        self.emit_event("created", {"path":sender.get_path(), "index":self.index})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        self.emit_event("destroyed")
        self._clean_up()

    def set_sequence(self, sequence):
        assert all([repeat > 0 for value, repeat in sequence])
        self.gatt_char.write_without_response(self._seq_to_bytes(sequence))

    def set(self, value):
        assert((0 <= value) and (value <= Config.max_pwm_value))
        repeats = 1
        sequence = [(value, repeats)]
        self.set_sequence(sequence)

    def _seq_to_bytes(self, sequence):
        flat = [value for segment in sequence for value in segment]
        result = b''.join(map(lambda x: x.to_bytes(2, "little"), flat))
        return result


@layer.add_handlers
class SeenProbe(layer.LayerObject):
    def __init__(self, index, sender, dispatcher):
        debug("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender), ("value_notification", sender)])
        assert(index in [Index.SENSOR1, Index.SENSOR2])
        self.index = index
        self.gatt_char = sender
        self.emit_event("created", {"path":sender.get_path(), "index":self.index})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        self.emit_event("destroyed")
        self._clean_up()

    @layer.handles_event("value_notification")
    def value_notification_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        seen_probe = bool(data[0])
        debug("seen probe {} = {}".format(self.index, seen_probe))
        self.emit_event("seen_probe", {"index":self.index, "seen_probe":seen_probe})

    def start_notify(self):
        self.gatt_char.start_notify()


@layer.add_handlers
class SendProbe(layer.LayerObject):
    def __init__(self, index, sender, dispatcher):
        debug("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("destroyed", sender)])
        assert(index in [Index.LASER1, Index.LASER2])
        self.index = index
        self.gatt_char = sender
        self.emit_event("created", {"path":sender.get_path(), "index":self.index})

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(self.gatt_char == sender) # just checkin
        debug("destroying {}".format(type(self).__name__))
        self.emit_event("destroyed")
        self._clean_up()

    def enable(self):
        self.gatt_char.write_without_response(b'\x01')

    def disable(self):
        self.gatt_char.write_without_response(b'\x00')

####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    import dbuslayer

    def sink(event_name, source, data):
        msg = "fake_on_event() '{}' from a {}".format(event_name, type(source).__name__)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        print(msg)

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    log("setting up dispatchers")
    # chain the layers together
    lasikservicedispatcher = layer.Dispatcher(sink, name = "lasikservice")
    bluezdispatcher = layer.Dispatcher(lasikservicedispatcher.on_event, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first lasikservice object")
    _ = LasikServiceBridge(lasikservicedispatcher)
    log("set up first bluez object")
    _ = bluezlayer.BluezBridge(bluezdispatcher)
    log("set up first dbus object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dbusdispatcher.on_event("stop", external = True)
