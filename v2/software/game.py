#!/usr/bin/env python3

# WARNING this is intentionally rather hacky code

from gi.repository import GLib

import beamservicelayer
import layer
import random
import pygame
import os

import lasiklog
warn =  lasiklog.get(prefix="WARNING(game)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(game)", color = lasiklog.colors.OKGREEN)
debug = lasiklog.get(prefix="DEBUG(game)", enabled = False)

####################################################################################################

class GameLogic:
    t_bad = 5               # how long to pause after mistake
    t_good = 0.5            # how long to pause after point

    t_max = 20              # maximum time score you can build up
    start_reward = 1.0      # reward is (lacking time) * this
    t_half_reward = 30      # after this time reward gets halved

    def __init__(self, good_cb, bad_cb, score_tick_cb):
        self.initialised = False
        self.good_cb = good_cb
        self.bad_cb = bad_cb
        self.score_tick_cb = score_tick_cb

    def init(self, beams):
        self.beams = beams
        self.flasher_index = None
        self.flasher_cooldown = False
        self.burned_cooldown = False
        self.burned_index = None
        self.t = 0
        self.t_remain = type(self).t_max
        self.initialised = True

        self.all_on()
        self.set_random_flasher()
        GLib.timeout_add(1000, self.on_sec_tick)

    def on_sec_tick(self):
        self.t += 1
        self.t_remain -= 1
        log("t_remain = {}".format(self.t_remain))
        self.score_tick_cb(self.t_remain, self.t)
        if (self.t_remain < 0):
            self.bad_cb()
            log("")
            log("==============================")
            log("GAME OVER: you lasted for {:3}s".format(self.t))
            log("==============================")
            log("")
            self.all_off()
            return False    # kill the tick
        return True         # keep the tick alive

    def beam_on(self, index):
        log("beam_on {}".format(index))
        self.beams[index].set([(4096,1)])

    def all_on(self):
        for i in range(len(self.beams)):
            self.beam_on(i)

    def beam_off(self, index):
        log("beam_off {}".format(index))
        self.beams[index].set([(0,1)])

    def all_off(self):
        for i in range(len(self.beams)):
            self.beam_off(i)

    def beam_flash(self, index, period = 5):
        self.beams[index].set([(4096, period), (0, period)])

    def set_random_flasher(self):
        assert(len(self.beams) > 1)
        self.flasher_cooldown = False
        rnd = self.flasher_index
        while (rnd == self.flasher_index):
            rnd = random.randint(0, len(self.beams) - 1)
        if self.flasher_index != None:
            self.beam_on(self.flasher_index)
        self.flasher_index = rnd
        self.beam_flash(self.flasher_index)

    def restore_burned(self):
        self.burned_cooldown = False
        if self.burned_index != self.flasher_index:
            self.beam_on(self.burned_index)

    def on_beam(self, index, is_on):
        if not self.initialised:
            return
        elif self.t_remain < 0:
            # game is over
            pass
        elif not is_on:
            # it's an in-game beam being broken
            if self.burned_cooldown:
                pass
            elif index == self.flasher_index:
                if not self.flasher_cooldown:
                    self.on_good()
                    self.beam_off(self.flasher_index)
                    self.flasher_cooldown = True
                    GLib.timeout_add(type(self).t_good * 1000, self.set_random_flasher)
                else:
                    pass
            else:
                self.on_bad()
                self.burned_index = index
                self.beam_flash(self.burned_index, period = 20)
                self.burned_cooldown = True
                GLib.timeout_add(type(self).t_bad * 1000, self.restore_burned)

    def get_reward(self):
        base_reward = type(self).start_reward
        half_time = type(self).t_half_reward
        return base_reward * .5 ** (self.t/half_time)

    def on_good(self):
        self.good_cb()
        t_max = type(self).t_max
        bonus = int((t_max - self.t_remain) * self.get_reward())
        log("GOOD! bonus time = {}".format(bonus))
        self.t_remain += bonus
        self.score_tick_cb(self.t_remain, self.t)

    def on_bad(self):
        self.bad_cb()
        log("BAD")

class GameBeams:
    min_setup_time = 10000
    max_setup_time = 45000
    enough_beams = 4
    def __init__(self, game_logic):
        self.game_logic = game_logic
        self.setup_new_beams = True
        self.min_setup_time_passed = False
        GLib.timeout_add(GameBeams.max_setup_time, self._on_no_more_new_beams)
        GLib.timeout_add(GameBeams.min_setup_time, self._on_min_time_passed)
        self.beams = []

    def on_event(self, event_name, source, data):
        self.log_event(event_name, source, data)
        if event_name == "created":
            if self.setup_new_beams:
                assert source not in self.beams
                self.beams.append(source)
                source.set([(4096, 5),(0, 5)])
                self._check_enough_beams()
            else:
                warn("Discovered new beam after discovery period!")
        elif event_name == "online":
            if source in self.beams:
                warn("game beam got online again! ({})".format(source.get_name()))
        elif event_name == "offline":
            if source in self.beams:
                warn("game beam went offline! ({})".format(source.get_name()))
        elif event_name == "beam":
            if source in self.beams:
                is_on = data["is_on"]
                index = self.beams.index(source)
                self.game_logic.on_beam(index, is_on)

    def _on_no_more_new_beams(self):
        if self.setup_new_beams:
            log("_on_no_more_new_beams()")
            self.setup_new_beams = False
            self.game_logic.init(self.beams)
            self.rapport_beams()
        return False

    def _on_min_time_passed(self):
        log("_on_min_time_passed()")
        self.min_setup_time_passed = True
        self._check_enough_beams()

    def _check_enough_beams(self):
        if self.setup_new_beams and self.min_setup_time_passed:
            # after the minimum beam setup time...
            if len(self.beams) >= GameBeams.enough_beams:
                log("that's enough! ({} beams)".format(len(self.beams)))
                # if enoug beams are created
                # then limit further setup time
                GLib.timeout_add(1000, self._on_no_more_new_beams)

    def rapport_beams(self):
        log("Got a total of {} beams:".format(len(self.beams)))
        for b in self.beams:
            log("    {}".format(b.get_name()))

    def disable_beams(self):
        for b in self.beams:
            b.set([(0, 5)])

    def log_event(self, event_name, source, data):
        if "get_name" in source.__dir__():
            name = source.get_name()
        else:
            name = "a " + type(source).__name__
        msg = "event '{}' from {}".format(event_name, name)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        log(msg)
        
def load_sound(name):
    main_dir = os.path.split(os.path.abspath(__file__))[0]
    sound_dir = os.path.join(main_dir, 'sound')
    fullname = os.path.join(sound_dir, name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error:
        print ('Cannot load sound: %s' % fullname)
        raise SystemExit(str(geterror()))
    return sound

def setupSound():
    pygame.mixer.pre_init(22050, -16, 2, 256)
    pygame.mixer.init()

    good = load_sound("smb_coin.wav")
    good.set_volume(.8)

    bad = load_sound("smb_bowserfalls.wav")
    bad.set_volume(.8)
    return good, bad

def setupDisplay():
    os.environ["DISPLAY"] = ":0"
    pygame.mouse.set_visible(False)
    #screen = pygame.display.set_mode((0,0), FULLSCREEN)
    screen = pygame.display.set_mode((700,400))
    sw, sh = screen.get_size()

    pygame.font.init()
    font = pygame.font.SysFont('Comic Sans MS', sh // 3)
    return screen, font

class Display:
    def __init__(self):
        screen, font = setupDisplay()
        self.screen = screen
        self.font = font

    def displayIntro(self):
        # duplicate code
        self.screen.fill((0,0,0))
        sw, sh = self.screen.get_size()

        text = "Getting To"
        renderedtext = self.font.render(text, False, (255,100,0))
        w,h = self.font.size(text)
        self.screen.blit(renderedtext, (sw/2 - w/2, sh/3 - h/2))

        text = "Blinky"
        renderedtext = self.font.render(text, False, (255,100,0))
        w,h = self.font.size(text)
        self.screen.blit(renderedtext, (sw/2 - w/2, 2*sh/3 - h/2))

        pygame.display.flip()
        pygame.event.pump()

    def displayScore(self, t_remain, t_total):
        # duplicate code
        self.screen.fill((0,0,0))

        sw, sh = self.screen.get_size()

        if t_remain >= 0:
            text = "time {}".format(t_remain)
        else:
            text = "GAME OVER"
        renderedtext = self.font.render(text, False, (0,255,0))
        w,h = self.font.size(text)
        self.screen.blit(renderedtext, (sw/2 - w/2, sh/3 - h/2))

        text = "score {}".format(t_total)
        renderedtext = self.font.render(text, False, (0,255,255))
        w,h = self.font.size(text)
        self.screen.blit(renderedtext, (sw/2 - w/2, 2*sh/3 - h/2))

        pygame.display.flip()
        pygame.event.pump()


####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    import dbus

    import dbuslayer
    import bluezlayer
    import lasikservicelayer
    import lasiklayer
    import beamservicelayer
    import beamlayer

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    sound_good, sound_bad = setupSound()
    sound_good.play()

    pygame.init()
    display = Display()
    display.displayIntro()

    gamelogic = GameLogic(sound_good.play, sound_bad.play, display.displayScore)
    gamebeams = GameBeams(gamelogic)
    
    log("setting up dispatchers")
    # chain the layers together
    beamdispatcher = layer.Dispatcher(gamebeams.on_event, name = "beam")
    beamservicedispatcher = layer.Dispatcher(beamdispatcher.on_event, name = "beamservice")
    lasikdispatcher = layer.Dispatcher(beamservicedispatcher.on_event, name = "lasik")
    lasikservicedispatcher = layer.Dispatcher(lasikdispatcher.on_event, name = "lasikservice")
    bluezdispatcher = layer.Dispatcher(lasikservicedispatcher.on_event, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first beamlayer object")
    _ = beamlayer.BeamBridge(beamdispatcher)
    log("set up first beamservicelayer object")
    _ = beamservicelayer.BeamServiceBridge(beamservicedispatcher)
    log("set up first lasiklayer object")
    _ = lasiklayer.LasikBridge(lasikdispatcher)
    log("set up first lasikservicelayer object")
    _ = lasikservicelayer.LasikServiceBridge(lasikservicedispatcher)
    log("set up first bluezlayer object")
    _ = bluezlayer.BluezBridge(bluezdispatcher)
    log("set up first dbuslayer object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        gamebeams.disable_beams()
        dbusdispatcher.on_event("stop", external = True)

