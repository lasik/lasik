#!/usr/bin/env python3

import dbus

import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(dbus)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(dbus)", color = lasiklog.colors.OKBLUE, enabled = False)
debug = lasiklog.get(prefix="DEBUG(dbus)", enabled = False)


####################################################################################################

@layer.add_handlers
class DBusBridge(layer.LayerObject):
    def __init__(self, bus, bus_name, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("start", None), ("stop", None)])
        self.bus = bus
        self.bus_name = bus_name
        self.added_paths = [] # just to prevent adding twice

    def handle_InterfacesAdded(self, path, interfaces):
        log("{}.handle_InterfacesAdded() {}".format(type(self).__name__, path))
        if "org.freedesktop.DBus.Properties" in interfaces:
            if path not in self.added_paths:
                self.added_paths.append(path)
                proxy_object = self.bus.get_object(self.bus_name, path)
                DBusObject(proxy_object, interfaces, self.bus, self.bus_name, self.dispatcher)
            else:
                warn("dbus object already processed!")

    def handle_InterfacesRemoved(self, path, interfaces):
        log("{}.handle_InterfacesRemoved() {}".format(type(self).__name__, path))
        if path in self.added_paths:
            self.added_paths.remove(path)

    @layer.handles_event("start")
    def handle_start(self, sender, data):
        # subscribe to signals for new objects appearing
        self.interfaceAdd_rcv = self.bus.add_signal_receiver(
            bus_name = self.bus_name,
            path = "/",
            dbus_interface = "org.freedesktop.DBus.ObjectManager",
            signal_name = "InterfacesAdded",
            handler_function = self.handle_InterfacesAdded)
        # subscribe to signals for objects disappearing
        # the objects themselves also get this signal, but the bridge needs it to keep track
        # of self.added_paths
        self.interfaceRemove_rcv = self.bus.add_signal_receiver(
            bus_name = self.bus_name,
            path = "/",
            dbus_interface = "org.freedesktop.DBus.ObjectManager",
            signal_name = "InterfacesRemoved",
            handler_function = self.handle_InterfacesRemoved)
        # handle all already known objects and process
        manager = dbus.Interface(
            self.bus.get_object(self.bus_name, "/"),
            "org.freedesktop.DBus.ObjectManager")
        objects = manager.GetManagedObjects()
        for path, interfaces in objects.items():
            self.handle_InterfacesAdded(path, interfaces)
        # sometimes an InterfacesAdded signal occurs in addition to the object being reported
        # in the list above, for these cases this class need to check to prevent double adding

    @layer.handles_event("stop")
    def handle_stop(self, sender, data):
        self.interfaceAdd_rcv.remove()


@layer.add_handlers
class DBusObject(layer.LayerObject):
    def __init__(self, proxy_object, interfaces, bus, bus_name, manager):
        debug("creating {}".format(type(self).__name__))
        super().__init__(manager, [("stop", None)])
        self.proxy_object = proxy_object
        self.interfaces = interfaces
        self.bus = bus
        self.bus_name = bus_name

        self._Properties = self.create_proxy("org.freedesktop.DBus.Properties")
        self._ir_subscription = self.bus.add_signal_receiver(
            bus_name = self.bus_name,
            path = "/",
            dbus_interface = "org.freedesktop.DBus.ObjectManager",
            signal_name = "InterfacesRemoved",
            handler_function = self.handle_InterfacesRemoved)

        self.emit_event("created")

    def subscribeToPropertiesChanged(self):
        log("{} subscribeToPropertiesChanged()".format(type(self).__name__))
        self._pc_subscription = self.bus.add_signal_receiver(
            bus_name = self.bus_name,
            path = self.proxy_object.object_path,
            dbus_interface = "org.freedesktop.DBus.Properties",
            signal_name = "PropertiesChanged",
            handler_function = self.handle_PropertiesChanged)

    def get(self, interface_name, property_name):
        return self._Properties.Get(interface_name, property_name)

    def get_path(self):
        return self.proxy_object.object_path

    def hasInterface(self, interface_name):
        return interface_name in self.interfaces

    def create_proxy(self, interface_name):
        assert interface_name in self.interfaces
        return dbus.Interface(self.proxy_object, interface_name)

    def handle_PropertiesChanged(self, interface, changed_props, invalidated_props):
        self.emit_event("properties_changed", (interface, changed_props, invalidated_props))

    def handle_InterfacesRemoved(self, path, interfaces):
        # check if it is for me
        if path == self.proxy_object.object_path:
            self._clean_up()

    @layer.handles_event("stop")
    def handle_stop(self, sender, data):
        self._clean_up()

    def _clean_up(self):
        self._ir_subscription.remove()
        if "_pc_subscription" in dir(self):
            self._pc_subscription.remove()
        self.emit_event("destroyed")
        super()._clean_up()

####################################################################################################

def format_change_notification(data):
    interface, changes, invalidated = data
    result = "changed={"+", ".join([str(k)+":"+str(v) for k,v in changes.items()])+"}"
    result += " invalidated=[" + ", ".join([str(i) for i in invalidated])+"]"
    return result

####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    def sink(event_name, source, data):
        print("fake_on_event() '{}' from a {} with type(data)={}".format(
            event_name, type(source).__name__, type(data).__name__))

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    dispatcher = layer.Dispatcher(sink, name = "dbus")
    bridge = DBusBridge(dbus.SystemBus(), "org.bluez", dispatcher)

    log("starting main loop")
    dispatcher.on_event("start", external = True)

    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dispatcher.on_event("stop", external = True)
