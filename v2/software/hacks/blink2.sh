#!/usr/bin/env bash
# make sure to set the values in btctl_init.cmd right

{
    cat btctl_init.cmd;
    while true; do
        while read line; do
            echo $line
            sleep 0.5
        done < btctl_loop.cmd
    done
} | bluetoothctl
