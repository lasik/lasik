#!/usr/bin/env bash
#get it using 'sudo hcitool lescan' and look for "Lasik 2.0"

#device_address=EB:09:66:0C:28:0B
device_address=C2:F3:BF:46:3F:96

while true; do
    sleep 0.1
    gatttool -t random -b $device_address --char-write-req --handle=0x0018 --value=00000000 > /dev/null
    sleep 0.1
    gatttool -t random -b $device_address --char-write-req --handle=0x0018 --value=e8030000 > /dev/null
done;
