#!/usr/bin/env python3

import dbus
from gi.repository import GLib

import dbuslayer
import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(bluez)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(bluez)", color = lasiklog.colors.OKBLUE)
debug = lasiklog.get(prefix="DEBUG(bluez)", enabled = False)

####################################################################################################

@layer.add_handlers
class BluezBridge(layer.LayerObject):
    def __init__(self, dispatcher):
        super().__init__(dispatcher, [("created", None)])
        self.dbus_adapters = {}

    @layer.handles_event("created")
    def created_handler(self, sender, data):
        # check if the dbus object is on of interest
        if sender.hasInterface("org.bluez.Adapter1"):
            if sender.get_path() in self.dbus_adapters:
                assert(False)
            self.dbus_adapters[sender.get_path()] = sender
            Adapter(sender, self.dispatcher)
        elif sender.hasInterface("org.bluez.Device1"):
            adapter_path = sender.get("org.bluez.Device1", "Adapter")
            assert(adapter_path in self.dbus_adapters)
            adapter = self.dbus_adapters[adapter_path]
            device = Device(sender, adapter, self.dispatcher)
        elif sender.hasInterface("org.bluez.GattCharacteristic1"):
            Characteristic(sender, self.dispatcher)

@layer.add_handlers
class _BluezObject(layer.LayerObject):
    interface_name = "override me"
    def __init__(self, sender, dispatcher):
        super().__init__(dispatcher, [("destroyed", sender), ("properties_changed", sender)])
        self.dbus_object = sender
        self.dbus_object.subscribeToPropertiesChanged()
        self.path = self.dbus_object.get_path()

    @layer.handles_event("destroyed")
    def destroyed_handler(self, sender, data):
        assert(sender == self.dbus_object)
        self.emit_event("destroyed")
        self._clean_up()

    @layer.handles_event("properties_changed")
    def properties_changed_handler(self, sender, data):
        assert(sender == self.dbus_object)
        output = dbuslayer.format_change_notification(data)
        log("{}.properties_changed_handler() {}".format(type(self).__name__, output))

    def _generic_reply_handler(self, prefix):
        def handler(*args, self = self, prefix = prefix, **kwargs):
            debug("SUCCESS: " + prefix + " {} reply: {} {}".format(
                  type(self).__name__, args, kwargs))
        return handler

    def _generic_error_handler(self, prefix):
        def handler(error, self = self, prefix = prefix):
            warn("FAIL: " + prefix + " {} error: {}".format(type(self).__name__, error))
        return handler

    def _get_property(self, property_name):
        return self.dbus_object.get(type(self).interface_name, property_name)

    def get_path(self):
        return self.path


@layer.add_handlers
class Adapter(_BluezObject):
    interface_name = "org.bluez.Adapter1"
    def __init__(self, sender, dispatcher):
        super().__init__(sender, dispatcher)
        self.dbus_adapter = self.dbus_object.create_proxy(Adapter.interface_name)
        self.alias = self._get_property("Alias")
        self.address = self._get_property("Address")
        log("creating Adapter '{}' on '{}'".format(self.alias, self.address))
        self.emit_event("created")

    def start_discovery(self):
        msg = "starting discovery on '{}' on '{}'".format(self.alias, self.address)
        log(msg)
        self.dbus_adapter.StartDiscovery(
            reply_handler = self._generic_reply_handler(msg),
            error_handler = self._generic_error_handler(msg))

    def stop_discovery(self):
        msg = "stopping discovery on '{}' on '{}'".format(self.alias, self.address)
        log(msg)
        self.dbus_adapter.StopDiscovery(
            reply_handler = self._generic_reply_handler(msg),
            error_handler = self._generic_error_handler(msg))


@layer.add_handlers
class Device(_BluezObject):
    interface_name = "org.bluez.Device1"
    def __init__(self, sender, adapter, dispatcher):
        super().__init__(sender, dispatcher)
        self.dbus_adapter = adapter.create_proxy("org.bluez.Adapter1")
        self.dbus_device = self.dbus_object.create_proxy(Device.interface_name)
        self.alias = self._get_property("Alias")
        self.address = self._get_property("Address")
        debug("creating Device '{}' on '{}'".format(self.alias, self.address))

        self.emit_event("created")

    def connect(self, retry_delay_s = 20, max_retries = 1):
        # TODO not very sure how sound this all is
        msg = "{}.connect() to '{}' on '{}'".format(type(self).__name__, self.alias, self.address)
        log(msg)
        if self._get_property("ServicesResolved"):
            self._on_connected()
        else:
            def success():
                debug("SUCCESS: " + msg)
            def fail(error):
                log("FAIL: " + msg + " " + str(error))
                if max_retries == 0:
                    warn("算了！ out of retries: " + msg)
                    # make adapter forget device
                    # if it then shows up again it will trigger a create event
                    self.emit_event("destroyed")
                    self._clean_up()
                    self._suan_le()
                else:
                    log("{}.connect(): retrying in {} seconds, {} retries remaining".format(
                        type(self).__name__, retry_delay_s, max_retries))
                    GLib.timeout_add( retry_delay_s * 1000,
                                      self.connect,
                                      retry_delay_s, max_retries - 1)
            self.dbus_device.Connect(
                reply_handler = success,
                error_handler = fail)
        return False    # the callback passed to GLib.timeout_add must not return True

    def disconnect(self):
        msg = "disconnecting from '{}' on '{}'".format(self.alias, self.address)
        log(msg)
        self.dbus_device.Disconnect(
            reply_handler = self._generic_reply_handler(msg),
            error_handler = self._generic_error_handler(msg))

    def _suan_le(self):
        # request adapter to forget about this device
        msg = "requesting adapter to forget about {}".format(self.path)
        log(msg)
        self.dbus_adapter.RemoveDevice(self.path,
            reply_handler = self._generic_reply_handler(msg),
            error_handler = self._generic_error_handler(msg))

    def has_uuid(self, uuid):
        return uuid in self._get_property("UUIDs")

    def _on_connected(self):
        log("connected to '{}' on '{}'".format(self.alias, self.address))
        self.emit_event("connected")

    def _on_disconnected(self):
        log("disconnected from '{}' on '{}'".format(self.alias, self.address))
        self.emit_event("disconnected")

    @layer.handles_event("properties_changed")
    def properties_changed_handler(self, sender, data):
        assert(sender == self.dbus_object)
        interface, changes, invalidated = data
        if "ServicesResolved" in changes:
            connected = changes["ServicesResolved"]
            if connected == 0:
                self._on_disconnected()
            elif connected == 1:
                self._on_connected()
            else:
                warn("unexpected value for 'ServicesResolved' received")
                assert(False)

    def get_address(self):
        return self.address


@layer.add_handlers
class Characteristic(_BluezObject):
    interface_name = "org.bluez.GattCharacteristic1"
    write_retry_delay_ms = 150
    def __init__(self, sender, dispatcher):
        super().__init__(sender, dispatcher)
        self.uuid = self._get_property("UUID")
        debug("creating Characteristic with UUID='{}' on '{}'".format(
            self.uuid, self.path))
        self.dbus_gatt_char = self.dbus_object.create_proxy(Characteristic.interface_name)
        self.emit_event("created")

    @layer.handles_event("properties_changed")
    def properties_changed_handler(self, sender, data):
        assert(sender == self.dbus_object)
        interface, changes, invalidated = data
        if "Value" in changes:
            value = changes["Value"]
            self.emit_event("value_notification", value)

    def start_notify(self):
        msg = "StartNotify on Characteristic with UUID='{}' on {}".format(
              self.uuid, self.path)
        debug(msg)
        self.dbus_gatt_char.StartNotify(
            reply_handler = self._generic_reply_handler(msg),
            error_handler = self._generic_error_handler(msg))

    def get_uuid(self):
        return self.uuid

    def write_without_response(self, data_bytes, retries = 3):
        msg = "writing {} to Characteristic with UUID='{}' on '{}'".format(
              "[" + ",".join([hex(b) for b in data_bytes]) + "]",
              self.uuid,
              self.path)
        def retry(error, msg = msg, data_bytes = data_bytes, retries = retries - 1):
            warn("FAIL: " + msg + " {} error: {}".format(type(self).__name__, error))
            warn("{} retries left".format(retries))
            GLib.timeout_add( type(self).write_retry_delay_ms,
                              self.write_without_response,
                              data_bytes, retries)
        if retries > 0:
            error_handler = retry
        else:
            error_handler = self._generic_error_handler(msg)
        debug(msg)
        self.dbus_gatt_char.WriteValue(
            data_bytes,
            {"type":"command"},
            reply_handler = self._generic_reply_handler(msg),
            error_handler = error_handler)

    def request_notification(self):
        msg = "reading value from Characteristic with UUID='{}' on '{}'".format(
              self.uuid,
              self.path)
        debug(msg)
        self.dbus_gatt_char.ReadValue(
            {},
            reply_handler = self._report_value,
            error_handler = self._generic_error_handler(msg))

    def _report_value(self, value):
        debug("reporting requested value notificaitn")
        self.emit_event("value_notification", value)


####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    def sink(event_name, source, data):
        msg = "fake_on_event() '{}' from a {}".format(event_name, type(source).__name__)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        print(msg)

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    log("setting up dispatchers")
    # chain the layers together
    bluezdispatcher = layer.Dispatcher(sink, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first bluez object")
    _ = BluezBridge(bluezdispatcher)
    log("set up first dbus object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dbusdispatcher.on_event("stop", external = True)
