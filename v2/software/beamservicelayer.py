#!/usr/bin/env python3

import lasiklayer
import layer

import lasiklog
warn =  lasiklog.get(prefix="WARNING(beamservice)", color = lasiklog.colors.WARNING)
log =   lasiklog.get(prefix="(beamservice)", color = lasiklog.colors.OKBLUE, enabled = False)
debug = lasiklog.get(prefix="DEBUG(beamservice)", enabled = False)

####################################################################################################

@layer.add_handlers
class BeamServiceBridge(layer.LayerObject):
    def __init__(self, dispatcher):
        log("creating {}".format(type(self).__name__))
        super().__init__(dispatcher, [("online", None)])
        self.known_lasiks = set()

    @layer.handles_event("online")
    def online_handler(self, sender, data):
        assert isinstance(sender, lasiklayer.Lasik)
        if sender not in self.known_lasiks:
            self.known_lasiks.add(sender)
            for index in range(2):
                laser = Laser(sender, index, self.dispatcher)
                # propagate the current event to the new laser
                laser.online_handler(sender, data)
                sensor = Sensor(sender, index, self.dispatcher)
                # propagate the current event to the new sensor
                sensor.online_handler(sender, data)

@layer.add_handlers
class _BeamServiceLayerObject(layer.LayerObject):
    def __init__(self, lasik, index, dispatcher):
        super().__init__(dispatcher, [("online", lasik), ("offline", lasik)])
        self.lasik = lasik
        self.index = index
        self.online = False
        self.name = "override me!"

    @layer.handles_event("online")
    def online_handler(self, sender, data):
        self.online = True
        self.emit_event("online")

    @layer.handles_event("offline")
    def offline_handler(self, sender, data):
        self.online = False
        self.emit_event("offline")

    def is_online(self):
        return self.online

    def get_name(self):
        return self.name

@layer.add_handlers
class Laser(_BeamServiceLayerObject):
    def __init__(self, lasik, index, dispatcher):
        super().__init__(lasik, index, dispatcher)
        self.name = lasik.get_name() + "_L{}".format(index)
        self.sequence = None
        log("creating {} {}".format(type(self).__name__, self.name))

    @layer.handles_event("online")
    def online_handler(self, sender, data):
        if self.sequence != None:
            self.lasik.set_laser_sequence(self.index, self.sequence)
        super().online_handler(sender, data)

    def set(self, sequence):
        self.sequence = sequence
        if self.online:
            self.lasik.set_laser_sequence(self.index, sequence)

    def set_probe(self, is_on):
        self.lasik.set_probe(self.index, is_on)

@layer.add_handlers
class Sensor(_BeamServiceLayerObject):
    def __init__(self, lasik, index, dispatcher):
        super().__init__(lasik, index, dispatcher)
        self.subscribe_to_event("sensor", lasik)
        self.subscribe_to_event("probe", lasik)
        self.name = lasik.get_name() + "_S{}".format(index)
        log("creating {} {}".format(type(self).__name__, self.name))

    @layer.handles_event("sensor")
    def sensor_handler(self, sender, data):
        index = data["index"]
        is_on = data["is_on"]
        if index == self.index:
            self.emit_event("sensor", {"is_on": is_on})

    @layer.handles_event("probe")
    def probe_handler(self, sender, data):
        index = data["index"]
        is_on = data["is_on"]
        if index == self.index:
            self.emit_event("probe", {"is_on": is_on})


####################################################################################################

if __name__ == "__main__":

    from dbus.mainloop.glib import DBusGMainLoop
    from gi.repository import GLib

    import dbus

    import dbuslayer
    import bluezlayer
    import lasikservicelayer

    def sink(event_name, source, data):
        msg = "event '{}' from a {}".format(event_name, type(source).__name__)
        if data != None:
            datastr = str(data)
            if len(datastr) > 100:
                msg += "with data of type {}".format(type(data).__name__)
            else:
                msg += " with data='{}'".format(datastr)
        log(msg)

    DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    log("setting up dispatchers")
    # chain the layer together
    beamservicedispatcher = layer.Dispatcher(sink, name = "beamservice")
    lasikdispatcher = layer.Dispatcher(beamservicedispatcher.on_event, name = "lasik")
    lasikservicedispatcher = layer.Dispatcher(lasikdispatcher.on_event, name = "lasikservice")
    bluezdispatcher = layer.Dispatcher(lasikservicedispatcher.on_event, name = "bluez")
    dbusdispatcher = layer.Dispatcher(bluezdispatcher.on_event, name = "dbus")

    log("set up first beamservicelayer object")
    _ = BeamServiceBridge(beamservicedispatcher)
    log("set up first lasik-layer object")
    _ = lasiklayer.LasikBridge(lasikdispatcher)
    log("set up first lasikservice object")
    _ = lasikservicelayer.LasikServiceBridge(lasikservicedispatcher)
    log("set up first bluez object")
    _ = bluezlayer.BluezBridge(bluezdispatcher)
    log("set up first dbus object")
    _ = dbuslayer.DBusBridge(dbus.SystemBus(), "org.bluez", dbusdispatcher)

    log("start receiving events at the bottom (dbus) layer")
    dbusdispatcher.on_event("start", external = True)

    log("starting main loop")
    try:
        loop.run()
    except KeyboardInterrupt:
        log("KeyboardInterrupt, 拜拜！")
        dbusdispatcher.on_event("stop", external = True)
