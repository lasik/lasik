EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6950 2900 1    50   Input ~ 0
V_analog
Text HLabel 4050 3400 0    50   Input ~ 0
GND
Text HLabel 5200 2900 1    50   Input ~ 0
3V3
$Comp
L Regulator_Linear:TLV75533PDBV U1001
U 1 1 5C608D05
P 4600 3100
F 0 "U1001" H 4600 3442 50  0000 C CNN
F 1 "TLV75533PDBV" H 4600 3351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 4600 3400 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv755p.pdf" H 4600 3100 50  0001 C CNN
	1    4600 3100
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:TLV75533PDBV U1002
U 1 1 5C608E61
P 6250 3100
F 0 "U1002" H 6250 3442 50  0000 C CNN
F 1 "MIC94310" H 6250 3351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 6250 3400 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MIC94310.pdf" H 6250 3100 50  0001 C CNN
	1    6250 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3100 4300 3000
Wire Wire Line
	5950 3100 5950 3000
Wire Wire Line
	4150 3000 4300 3000
Connection ~ 4300 3000
Wire Wire Line
	4900 3000 5200 3000
Connection ~ 5950 3000
Wire Wire Line
	5200 2900 5200 3000
Connection ~ 5200 3000
Wire Wire Line
	5200 3000 5800 3000
$Comp
L Device:R_Small R1001
U 1 1 5C6093A9
P 6250 2600
F 0 "R1001" V 6054 2600 50  0000 C CNN
F 1 "R_Small" V 6145 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6250 2600 50  0001 C CNN
F 3 "~" H 6250 2600 50  0001 C CNN
	1    6250 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 2600 5950 2600
Wire Wire Line
	5950 2600 5950 3000
$Comp
L Device:C_Small C?
U 1 1 5C609713
P 4150 3200
AR Path="/5C5E4922/5C609713" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609713" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609713" Ref="C1001"  Part="1" 
F 0 "C1001" H 3800 3250 50  0000 L CNN
F 1 "C_Small" H 3750 3150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4150 3200 50  0001 C CNN
F 3 "~" H 4150 3200 50  0001 C CNN
F 4 "" H -650 0   50  0001 C CNN "Note"
	1    4150 3200
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5C60971A
P 4150 3450
AR Path="/5C5E4922/5C60971A" Ref="#PWR?"  Part="1" 
AR Path="/5C60014F/5C60971A" Ref="#PWR?"  Part="1" 
AR Path="/5C5FC5C6/5C60971A" Ref="#PWR01001"  Part="1" 
F 0 "#PWR01001" H 4150 3200 50  0001 C CNN
F 1 "Earth" H 4150 3300 50  0001 C CNN
F 2 "" H 4150 3450 50  0001 C CNN
F 3 "~" H 4150 3450 50  0001 C CNN
	1    4150 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3450 4150 3400
Wire Wire Line
	4150 3100 4150 3000
Wire Wire Line
	4600 3400 4150 3400
Connection ~ 4150 3400
Wire Wire Line
	4150 3400 4150 3300
$Comp
L Device:C_Small C?
U 1 1 5C609CA5
P 5200 3200
AR Path="/5C5E4922/5C609CA5" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609CA5" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609CA5" Ref="C1002"  Part="1" 
F 0 "C1002" H 4850 3200 50  0000 L CNN
F 1 "C_Small" H 4850 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5200 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
F 4 "" H -650 0   50  0001 C CNN "Note"
	1    5200 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C609CED
P 6600 3250
AR Path="/5C5E4922/5C609CED" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C609CED" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C609CED" Ref="C1003"  Part="1" 
F 0 "C1003" H 6300 3250 50  0000 L CNN
F 1 "C_Small" H 6250 3150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6600 3250 50  0001 C CNN
F 3 "~" H 6600 3250 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    6600 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5200 3100 5200 3000
Wire Wire Line
	5200 3300 5200 3400
Connection ~ 5200 3400
Wire Wire Line
	5200 3400 4600 3400
Connection ~ 6250 3400
Wire Wire Line
	4050 3400 4150 3400
Text HLabel 4050 3000 0    50   Input ~ 0
V_in
Wire Wire Line
	6950 2900 6950 3000
Wire Wire Line
	6950 3000 6600 3000
Connection ~ 4600 3400
Wire Wire Line
	6600 3400 6600 3350
Wire Wire Line
	6250 3400 6600 3400
Wire Wire Line
	6600 3150 6600 3000
Connection ~ 6600 3000
Wire Wire Line
	6600 3000 6550 3000
Wire Wire Line
	6600 2600 6600 3000
Wire Wire Line
	6350 2600 6600 2600
Wire Wire Line
	4150 3000 4050 3000
Connection ~ 4150 3000
$Comp
L Device:C_Small C?
U 1 1 5C68B5E3
P 5800 3200
AR Path="/5C5E4922/5C68B5E3" Ref="C?"  Part="1" 
AR Path="/5C60014F/5C68B5E3" Ref="C?"  Part="1" 
AR Path="/5C5FC5C6/5C68B5E3" Ref="C?"  Part="1" 
F 0 "C?" H 5450 3200 50  0000 L CNN
F 1 "C_Small" H 5450 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5800 3200 50  0001 C CNN
F 3 "~" H 5800 3200 50  0001 C CNN
F 4 "" H -50 0   50  0001 C CNN "Note"
	1    5800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3300 5800 3400
Connection ~ 5800 3400
Wire Wire Line
	5800 3100 5800 3000
Connection ~ 5800 3000
Wire Wire Line
	5800 3000 5950 3000
Wire Wire Line
	5950 3000 6000 3000
Wire Wire Line
	5800 3400 5200 3400
Wire Notes Line
	3650 3650 3650 2600
Wire Notes Line
	3650 2600 5350 2600
Wire Notes Line
	5350 2600 5350 3650
Wire Wire Line
	5800 3400 6250 3400
Wire Notes Line
	3650 3650 5350 3650
Text Notes 4200 2550 0    50   ~ 0
On motherboard
$EndSCHEMATC
