EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3400 1050 3450 1050
Wire Wire Line
	3550 1850 3550 1350
Wire Wire Line
	3550 1350 3400 1350
Wire Wire Line
	3550 1850 3550 2800
Wire Wire Line
	3550 2800 3400 2800
Connection ~ 3550 1850
Wire Wire Line
	3450 1050 3450 2500
Wire Wire Line
	3450 2500 3400 2500
Connection ~ 3450 1050
Wire Wire Line
	3400 2650 3500 2650
Wire Wire Line
	3500 2650 3500 1200
Wire Wire Line
	3400 1200 3500 1200
$Sheet
S 1150 2400 2250 1250
U 5C662123
F0 "sheet5C662102" 50
F1 "3-stage.sch" 50
F2 "3-stage_triggered-out" I R 3400 3550 50 
F3 "VAA" I R 3400 2500 50 
F4 "GND" I R 3400 2800 50 
F5 "V_analog" I R 3400 2650 50 
F6 "PV+" I L 1150 2500 50 
F7 "PV-" I L 1150 2600 50 
$EndSheet
$Sheet
S 1150 950  2250 1250
U 5C662129
F0 "sheet5C662103" 50
F1 "3-stage.sch" 50
F2 "3-stage_triggered-out" I R 3400 2100 50 
F3 "GND" I R 3400 1350 50 
F4 "VAA" I R 3400 1050 50 
F5 "V_analog" I R 3400 1200 50 
F6 "PV+" I L 1150 1050 50 
F7 "PV-" I L 1150 1150 50 
$EndSheet
Text HLabel 3800 1000 1    50   Input ~ 0
V_analog
Text HLabel 4500 1850 2    50   Input ~ 0
GND
Text HLabel 3800 2100 2    50   Input ~ 0
OUT1
Text HLabel 3800 3550 2    50   Input ~ 0
OUT2
Wire Wire Line
	3800 3550 3400 3550
Wire Wire Line
	3400 2100 3800 2100
Text HLabel 3650 1000 1    50   Input ~ 0
VAA
Wire Wire Line
	3450 1050 3650 1050
$Comp
L Comparator:LM393 U?
U 3 1 5C68804A
P 4350 1500
AR Path="/5C5EEFD0/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C5E4922/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C60014F/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C62C3B9/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DA8/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C661DAE/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662123/5C68804A" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C662129/5C68804A" Ref="U?"  Part="1" 
AR Path="/5C5FAFFD/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C666787/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C689EFA/5C68804A" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E1/5C68804A" Ref="U301"  Part="3" 
AR Path="/5C68A269/5C68A301/5C68804A" Ref="U601"  Part="3" 
F 0 "U601" H 4450 1550 50  0000 C CNN
F 1 "TLV3202" H 4450 1450 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4350 1500 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv3201.pdf" H 4350 1500 50  0001 C CNN
F 4 "" H 100 0   50  0001 C CNN "Note"
	3    4350 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4450 1850 4450 1800
Wire Wire Line
	4450 1200 4000 1200
Connection ~ 3500 1200
Wire Wire Line
	3800 1000 3800 1200
Connection ~ 3800 1200
Wire Wire Line
	3800 1200 3500 1200
Wire Wire Line
	3650 1050 3650 1000
Wire Wire Line
	4500 1850 4450 1850
Wire Wire Line
	3550 1850 4000 1850
Connection ~ 4450 1850
$Comp
L Device:C_Small C301
U 1 1 5C638DE9
P 4000 1500
AR Path="/5C68A269/5C68A2E1/5C638DE9" Ref="C301"  Part="1" 
AR Path="/5C68A269/5C68A301/5C638DE9" Ref="C601"  Part="1" 
F 0 "C601" H 4350 1550 50  0000 R CNN
F 1 "100n" H 4400 1450 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4000 1500 50  0001 C CNN
F 3 "~" H 4000 1500 50  0001 C CNN
F 4 "" H 100 0   50  0001 C CNN "Note"
	1    4000 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4000 1600 4000 1850
Connection ~ 4000 1850
Wire Wire Line
	4000 1850 4450 1850
Wire Wire Line
	4000 1400 4000 1200
Connection ~ 4000 1200
Wire Wire Line
	4000 1200 3800 1200
Text HLabel 950  1050 0    50   Input ~ 0
in1+
Text HLabel 950  1150 0    50   Input ~ 0
in1-
Text HLabel 950  2500 0    50   Input ~ 0
in2+
Text HLabel 950  2600 0    50   Input ~ 0
in2-
Wire Wire Line
	950  1050 1150 1050
Wire Wire Line
	1150 1150 950  1150
Wire Wire Line
	950  2500 1150 2500
Wire Wire Line
	1150 2600 950  2600
$EndSCHEMATC
