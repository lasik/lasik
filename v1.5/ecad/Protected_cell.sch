EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5C68A6FC
P 2200 2750
AR Path="/5C5FC5C6/5C68A6FC" Ref="BT?"  Part="1" 
AR Path="/5C67B796/5C68A6FC" Ref="BT?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C68A6FC" Ref="BT1101"  Part="1" 
F 0 "BT1101" H 1800 2900 50  0000 L CNN
F 1 "Battery_Cell" H 1700 2700 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_1042_1x18650" V 2200 2810 50  0001 C CNN
F 3 "https://www.mouser.be/datasheet/2/215/042-744829.pdf" V 2200 2810 50  0001 C CNN
F 4 "TBD" H 0   0   50  0001 C CNN "Note"
	1    2200 2750
	1    0    0    -1  
$EndComp
Text HLabel 4300 1950 2    50   Input ~ 0
V_bat
Text HLabel 4300 3250 2    50   Input ~ 0
GND
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5C7BE10C
P 3150 3150
AR Path="/5C602347/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C64728C/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C647A43/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C648B3B/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB623/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB628/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB633/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB640/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0E5/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0EA/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0EF/5C7BE10C" Ref="Q?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C7BE10C" Ref="Q1101"  Part="1" 
F 0 "Q1101" V 3050 2850 50  0000 L CNN
F 1 "DMG2302" V 3400 2950 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3350 3250 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMG2302UK.pdf" H 3150 3150 50  0001 C CNN
	1    3150 3150
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5C7BE185
P 3600 3150
AR Path="/5C602347/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C64728C/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C647A43/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C648B3B/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB623/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB628/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB633/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AB640/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0E5/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0EA/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C6A8974/5C6AC0EF/5C7BE185" Ref="Q?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C7BE185" Ref="Q1102"  Part="1" 
F 0 "Q1102" V 3500 3200 50  0000 L CNN
F 1 "DMG2302" V 3850 2950 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3800 3250 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMG2302UK.pdf" H 3600 3150 50  0001 C CNN
	1    3600 3150
	0    -1   1    0   
$EndComp
Wire Wire Line
	3400 3250 3350 3250
Wire Wire Line
	3150 2950 3150 2900
Wire Wire Line
	3150 2900 3250 2900
Wire Wire Line
	3600 2950 3600 2900
Wire Wire Line
	3600 2900 3500 2900
$Comp
L Device:C_Small C?
U 1 1 5C7BE4B8
P 2650 2700
AR Path="/5C696261/5C7BE4B8" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C7BE4B8" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C7BE4B8" Ref="C1101"  Part="1" 
F 0 "C1101" H 2700 2800 50  0000 L CNN
F 1 "100n" H 2700 2600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 2700 50  0001 C CNN
F 3 "~" H 2650 2700 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    2650 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2750 2600 2750 3250
Wire Wire Line
	2750 3250 2950 3250
Connection ~ 2750 3250
$Comp
L Device:R_Small R?
U 1 1 5C7BE80C
P 2400 2350
AR Path="/5C5FAFFD/5C7BE80C" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C7BE80C" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C7BE80C" Ref="R?"  Part="1" 
AR Path="/5C696261/5C7BE80C" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C7BE80C" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C7BE80C" Ref="R1101"  Part="1" 
F 0 "R1101" V 2300 2450 50  0000 R CNN
F 1 "330R" V 2500 2500 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2400 2350 50  0001 C CNN
F 3 "~" H 2400 2350 50  0001 C CNN
	1    2400 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	2200 2850 2200 3250
Wire Wire Line
	2200 2550 2200 2350
Wire Wire Line
	2200 2350 2300 2350
Wire Wire Line
	2500 2350 2650 2350
Wire Wire Line
	2200 3250 2650 3250
Wire Wire Line
	2650 2800 2650 3250
Connection ~ 2650 3250
Wire Wire Line
	2650 3250 2750 3250
Wire Wire Line
	2650 2600 2650 2350
Connection ~ 2650 2350
Wire Wire Line
	2650 2350 2750 2350
Wire Wire Line
	2200 2350 2200 1950
Wire Wire Line
	2200 1950 4300 1950
Connection ~ 2200 2350
Wire Wire Line
	3850 2450 3900 2450
Wire Wire Line
	3800 3250 3900 3250
$Comp
L Device:R_Small R?
U 1 1 5C7BFDFB
P 3900 2850
AR Path="/5C5FAFFD/5C7BFDFB" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C7BFDFB" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C7BFDFB" Ref="R?"  Part="1" 
AR Path="/5C696261/5C7BFDFB" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C7BFDFB" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C68A220/5C7BFDFB" Ref="R1102"  Part="1" 
F 0 "R1102" H 3850 2900 50  0000 R CNN
F 1 "2K2" H 3850 2800 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3900 2850 50  0001 C CNN
F 3 "~" H 3900 2850 50  0001 C CNN
	1    3900 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 2950 3900 3250
Connection ~ 3900 3250
Wire Wire Line
	3900 3250 4300 3250
Wire Wire Line
	3900 2750 3900 2450
$Comp
L Lasik:AP9101C-SOT26 U1101
U 1 1 5C67EF28
P 2600 2100
F 0 "U1101" H 3300 2115 50  0000 C CNN
F 1 "AP9101C-SOT26" H 3300 2024 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 2600 2100 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP9101C.pdf" H 2600 2100 50  0001 C CNN
	1    2600 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1101
U 1 1 5CCF8B85
P 1400 2750
F 0 "J1101" H 950 2350 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1200 2500 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 1400 2750 50  0001 C CNN
F 3 "~" H 1400 2750 50  0001 C CNN
	1    1400 2750
	1    0    0    1   
$EndComp
Wire Wire Line
	1600 2350 2200 2350
Wire Wire Line
	1600 2750 1600 3250
Wire Wire Line
	1600 3250 2200 3250
Connection ~ 2200 3250
Text Notes 1100 2850 0    50   ~ 0
Ext. cell
Wire Wire Line
	1600 2350 1600 2650
$EndSCHEMATC
