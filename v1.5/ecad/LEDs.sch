EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5C6DF1A5
P 2900 2300
AR Path="/5C5E4922/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C6DF1A5" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF1A5" Ref="R1701"  Part="1" 
F 0 "R1701" H 3100 2300 50  0000 C CNN
F 1 "1K" H 3100 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 2300 50  0001 C CNN
F 3 "~" H 2900 2300 50  0001 C CNN
	1    2900 2300
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF1AD
P 2900 2600
AR Path="/5C6DF1AD" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF1AD" Ref="D1701"  Part="1" 
F 0 "D1701" V 2938 2482 50  0000 R CNN
F 1 "LED_G" V 2847 2482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 2600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 2900 2600 50  0001 C CNN
F 4 "preferably amber" H -3250 -2000 50  0001 C CNN "Note"
	1    2900 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 2450 2900 2400
$Comp
L Device:R_Small R?
U 1 1 5C6DF221
P 3400 2300
AR Path="/5C5E4922/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C6DF221" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF221" Ref="R1702"  Part="1" 
F 0 "R1702" H 3600 2300 50  0000 C CNN
F 1 "1K" H 3600 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3400 2300 50  0001 C CNN
F 3 "~" H 3400 2300 50  0001 C CNN
	1    3400 2300
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF228
P 3400 2600
AR Path="/5C6DF228" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF228" Ref="D1702"  Part="1" 
F 0 "D1702" V 3438 2482 50  0000 R CNN
F 1 "LED" V 3347 2482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3400 2600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 3400 2600 50  0001 C CNN
F 4 "preferably amber" H -2750 -2000 50  0001 C CNN "Note"
	1    3400 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 2450 3400 2400
$Comp
L Device:R_Small R?
U 1 1 5C6DF32A
P 3900 2300
AR Path="/5C5E4922/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C6DF32A" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF32A" Ref="R1703"  Part="1" 
F 0 "R1703" H 4100 2300 50  0000 C CNN
F 1 "1K" H 4100 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3900 2300 50  0001 C CNN
F 3 "~" H 3900 2300 50  0001 C CNN
	1    3900 2300
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF331
P 3900 2600
AR Path="/5C6DF331" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF331" Ref="D1703"  Part="1" 
F 0 "D1703" V 3938 2482 50  0000 R CNN
F 1 "LED" V 3847 2482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3900 2600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 3900 2600 50  0001 C CNN
F 4 "preferably amber" H -2250 -2000 50  0001 C CNN "Note"
	1    3900 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 2450 3900 2400
$Comp
L Device:R_Small R?
U 1 1 5C6DF338
P 4400 2300
AR Path="/5C5E4922/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C60014F/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C62C3B9/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DA8/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C661DAE/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C666787/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C689EFA/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E1/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662123/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A301/5C662129/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C6DF338" Ref="R?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF338" Ref="R1704"  Part="1" 
F 0 "R1704" H 4600 2300 50  0000 C CNN
F 1 "1K" H 4600 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 2300 50  0001 C CNN
F 3 "~" H 4400 2300 50  0001 C CNN
	1    4400 2300
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C6DF33F
P 4400 2600
AR Path="/5C6DF33F" Ref="D?"  Part="1" 
AR Path="/5C6CC2A7/5C6DF33F" Ref="D1704"  Part="1" 
F 0 "D1704" V 4438 2482 50  0000 R CNN
F 1 "LED" V 4347 2482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 2600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5174586/LG%20R971_EN.pdf" H 4400 2600 50  0001 C CNN
F 4 "preferably amber" H -1750 -2000 50  0001 C CNN "Note"
	1    4400 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 2450 4400 2400
Text HLabel 2650 2850 0    50   Input ~ 0
GND
Wire Wire Line
	2650 2850 2900 2850
Wire Wire Line
	4400 2750 4400 2850
Wire Wire Line
	3900 2750 3900 2850
Connection ~ 3900 2850
Wire Wire Line
	3900 2850 4400 2850
Wire Wire Line
	3400 2750 3400 2850
Connection ~ 3400 2850
Wire Wire Line
	3400 2850 3900 2850
Wire Wire Line
	2900 2750 2900 2850
Connection ~ 2900 2850
Wire Wire Line
	2900 2850 3400 2850
Text HLabel 2900 2150 1    50   Input ~ 0
LED4
Text HLabel 3400 2150 1    50   Input ~ 0
LED3
Text HLabel 3900 2150 1    50   Input ~ 0
LED2
Text HLabel 4400 2150 1    50   Input ~ 0
LED1
Wire Wire Line
	2900 2150 2900 2200
Wire Wire Line
	3400 2150 3400 2200
Wire Wire Line
	3900 2150 3900 2200
Wire Wire Line
	4400 2150 4400 2200
Text Notes 2600 3150 0    50   ~ 0
Caution!\nthe marking for the green LEDs are the reverse of the red model!.
$EndSCHEMATC
