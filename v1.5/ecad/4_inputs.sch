EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1150 1000 2250 1250
U 5C68A2E1
F0 "sheet5C68A2D8" 50
F1 "2_inputs.sch" 50
F2 "GND" I R 3400 1250 50 
F3 "OUT1" I R 3400 2000 50 
F4 "OUT2" I R 3400 2150 50 
F5 "VAA" I R 3400 1100 50 
F6 "V_analog" I R 3400 1400 50 
F7 "in1+" I L 1150 1100 50 
F8 "in1-" I L 1150 1200 50 
F9 "in2+" I L 1150 2050 50 
F10 "in2-" I L 1150 2150 50 
$EndSheet
Wire Wire Line
	3400 1400 3500 1400
Wire Wire Line
	3600 2700 3400 2700
Wire Wire Line
	3600 1250 3400 1250
Wire Wire Line
	3500 2850 3400 2850
$Sheet
S 1150 2450 2250 1250
U 5C68A301
F0 "sheet5C68A2DA" 50
F1 "2_inputs.sch" 50
F2 "GND" I R 3400 2700 50 
F3 "OUT1" I R 3400 3450 50 
F4 "OUT2" I R 3400 3600 50 
F5 "VAA" I R 3400 2550 50 
F6 "V_analog" I R 3400 2850 50 
F7 "in1+" I L 1150 2550 50 
F8 "in1-" I L 1150 2650 50 
F9 "in2+" I L 1150 3500 50 
F10 "in2-" I L 1150 3600 50 
$EndSheet
Text HLabel 4150 1400 2    50   Input ~ 0
V_analog
Text HLabel 4150 2000 2    50   Input ~ 0
OUT1
Text HLabel 4150 2150 2    50   Input ~ 0
OUT2
Text HLabel 4150 2300 2    50   Input ~ 0
OUT3
Text HLabel 4150 2450 2    50   Input ~ 0
OUT4
Wire Wire Line
	4150 2450 4000 2450
Wire Wire Line
	4000 3600 3400 3600
Wire Wire Line
	3900 3450 3400 3450
Text HLabel 4150 1250 2    50   Input ~ 0
GND
Wire Wire Line
	3400 2550 3700 2550
Wire Wire Line
	3700 1100 3400 1100
Text HLabel 4150 1100 2    50   Input ~ 0
VAA
Wire Wire Line
	3500 1400 3500 2850
Wire Wire Line
	3600 1250 3600 2700
Wire Wire Line
	4150 1400 3500 1400
Connection ~ 3500 1400
Wire Wire Line
	4150 1250 3600 1250
Connection ~ 3600 1250
Wire Wire Line
	4150 1100 3700 1100
Connection ~ 3700 1100
Wire Wire Line
	3700 1100 3700 2550
Wire Wire Line
	3900 2300 4150 2300
Wire Wire Line
	3400 2000 4150 2000
Wire Wire Line
	3400 2150 4150 2150
Wire Wire Line
	4000 2450 4000 3600
Wire Wire Line
	3900 2300 3900 3450
Text HLabel 950  2050 0    50   Input ~ 0
in2+
Text HLabel 950  2150 0    50   Input ~ 0
in2-
Text HLabel 950  1100 0    50   Input ~ 0
in1+
Text HLabel 950  1200 0    50   Input ~ 0
in1-
Text HLabel 950  3500 0    50   Input ~ 0
in4+
Text HLabel 950  3600 0    50   Input ~ 0
in4-
Text HLabel 950  2550 0    50   Input ~ 0
in3+
Text HLabel 950  2650 0    50   Input ~ 0
in3-
Wire Wire Line
	950  3600 1150 3600
Wire Wire Line
	1150 3500 950  3500
Wire Wire Line
	950  2650 1150 2650
Wire Wire Line
	1150 2550 950  2550
Wire Wire Line
	950  2150 1150 2150
Wire Wire Line
	1150 2050 950  2050
Wire Wire Line
	950  1200 1150 1200
Wire Wire Line
	1150 1100 950  1100
$EndSCHEMATC
