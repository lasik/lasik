EESchema Schematic File Version 4
LIBS:power
LIBS:74xx
LIBS:rfm69hcw
LIBS:bluepill_breakouts
LIBS:Lasik2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:D_Photo D301
U 1 1 5C5EF355
P 3200 3800
F 0 "D301" V 3050 4150 50  0000 R CNN
F 1 "D_Photo" V 3300 4150 50  0000 R CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 3150 3800 50  0001 C CNN
F 3 "~" H 3150 3800 50  0001 C CNN
	1    3200 3800
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small R301
U 1 1 5C5EF35C
P 3600 3450
F 0 "R301" V 3800 3600 50  0000 C CNN
F 1 "R_Small" V 3700 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3600 3450 50  0001 C CNN
F 3 "~" H 3600 3450 50  0001 C CNN
	1    3600 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Core_Ferrite_Coupled L301
U 1 1 5C5EF363
P 3600 3750
F 0 "L301" V 3550 3450 50  0000 L CNN
F 1 "Balun" V 3650 3450 50  0000 L CNN
F 2 "" H 3600 3750 50  0001 C CNN
F 3 "~" H 3600 3750 50  0001 C CNN
	1    3600 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R302
U 1 1 5C5EF36A
P 3600 4050
F 0 "R302" V 3600 4300 50  0000 C CNN
F 1 "R_Small" V 3500 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3600 4050 50  0001 C CNN
F 3 "~" H 3600 4050 50  0001 C CNN
	1    3600 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C302
U 1 1 5C5EF371
P 3850 4300
F 0 "C302" H 3942 4346 50  0000 L CNN
F 1 "C_Small" H 3942 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3850 4300 50  0001 C CNN
F 3 "~" H 3850 4300 50  0001 C CNN
	1    3850 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R303
U 1 1 5C5EF378
P 3700 4300
F 0 "R303" H 3550 4350 50  0000 C CNN
F 1 "R_Small" H 3500 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3700 4300 50  0001 C CNN
F 3 "~" H 3700 4300 50  0001 C CNN
	1    3700 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C301
U 1 1 5C5EF37F
P 3800 3150
F 0 "C301" V 4000 3050 50  0000 L CNN
F 1 "C_Small" V 3900 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3800 3150 50  0001 C CNN
F 3 "~" H 3800 3150 50  0001 C CNN
	1    3800 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R304
U 1 1 5C5EF386
P 3800 3300
F 0 "R304" V 3900 3050 50  0000 C CNN
F 1 "R_Small" V 3800 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3800 3300 50  0001 C CNN
F 3 "~" H 3800 3300 50  0001 C CNN
	1    3800 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 3600 3200 3550
Wire Wire Line
	3200 3550 3500 3550
Wire Wire Line
	3200 3900 3200 3950
Wire Wire Line
	3200 3950 3500 3950
Wire Wire Line
	3500 3450 3500 3550
Connection ~ 3500 3550
Wire Wire Line
	3700 3450 3700 3550
Wire Wire Line
	3700 3950 3800 3950
Wire Wire Line
	3500 4050 3500 3950
Connection ~ 3500 3950
Wire Wire Line
	3700 4050 3700 3950
Connection ~ 3700 3950
Wire Wire Line
	3800 3850 3800 3950
Wire Wire Line
	3800 3650 3800 3550
Wire Wire Line
	3800 3550 3700 3550
Connection ~ 3700 3550
Wire Wire Line
	3900 3300 3900 3150
Wire Wire Line
	3700 3300 3700 3150
Wire Wire Line
	3850 4200 3700 4200
Wire Wire Line
	3850 4400 3700 4400
Wire Wire Line
	3700 4200 3700 4050
Connection ~ 3700 4200
Connection ~ 3700 4050
Wire Wire Line
	3700 3300 3700 3450
Connection ~ 3700 3300
Connection ~ 3700 3450
Wire Wire Line
	3900 3150 4400 3150
Wire Wire Line
	4400 3150 4400 3750
Connection ~ 3900 3150
$Comp
L Device:C_Small C303
U 1 1 5C5EF3B1
P 4700 3750
F 0 "C303" V 4900 3800 50  0000 L CNN
F 1 "C_Small" V 4800 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4700 3750 50  0001 C CNN
F 3 "~" H 4700 3750 50  0001 C CNN
	1    4700 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R305
U 1 1 5C5EF3B8
P 4500 3950
F 0 "R305" H 4350 4000 50  0000 C CNN
F 1 "R_Small" H 4300 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 4500 3950 50  0001 C CNN
F 3 "~" H 4500 3950 50  0001 C CNN
	1    4500 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R306
U 1 1 5C5EF3BF
P 4900 3500
F 0 "R306" H 4750 3550 50  0000 C CNN
F 1 "R_Small" H 4700 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 4900 3500 50  0001 C CNN
F 3 "~" H 4900 3500 50  0001 C CNN
	1    4900 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 3850 4500 3750
Wire Wire Line
	4500 3750 4600 3750
Wire Wire Line
	4400 3750 4500 3750
Connection ~ 4500 3750
Wire Wire Line
	4900 3750 4900 3600
$Comp
L Device:R_Small R307
U 1 1 5C5EF3FA
P 5200 3200
F 0 "R307" V 5400 3250 50  0000 C CNN
F 1 "R_Small" V 5300 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 5200 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
	1    5200 3200
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR0303
U 1 1 5C5EF404
P 4500 4150
F 0 "#PWR0303" H 4500 3900 50  0001 C CNN
F 1 "Earth" H 4500 4000 50  0001 C CNN
F 2 "" H 4500 4150 50  0001 C CNN
F 3 "~" H 4500 4150 50  0001 C CNN
	1    4500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4150 4500 4100
$Comp
L power:VAA #PWR0304
U 1 1 5C5EF43C
P 4900 3150
F 0 "#PWR0304" H 4900 3000 50  0001 C CNN
F 1 "VAA" H 4918 3323 50  0000 C CNN
F 2 "" H 4900 3150 50  0001 C CNN
F 3 "" H 4900 3150 50  0001 C CNN
	1    4900 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3150 4900 3200
$Comp
L Device:R_Small R308
U 1 1 5C5EFD14
P 5550 3200
F 0 "R308" V 5750 3250 50  0000 C CNN
F 1 "R_Small" V 5650 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 5550 3200 50  0001 C CNN
F 3 "~" H 5550 3200 50  0001 C CNN
	1    5550 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 3200 6100 3200
Wire Wire Line
	6100 3200 6100 3650
Wire Wire Line
	6100 3650 6000 3650
Wire Wire Line
	5450 3200 5400 3200
Wire Wire Line
	5400 3550 5400 3200
Connection ~ 5400 3200
Wire Wire Line
	5400 3200 5300 3200
Wire Wire Line
	4800 3750 4900 3750
Connection ~ 4900 3750
Wire Wire Line
	4900 3750 5400 3750
Wire Wire Line
	5100 3200 4900 3200
Connection ~ 4900 3200
Wire Wire Line
	4900 3200 4900 3400
Text HLabel 6200 3650 2    50   Input ~ 0
2-stage_triggered-out
Wire Wire Line
	6200 3650 6100 3650
Connection ~ 6100 3650
Text HLabel 4800 3200 0    50   Input ~ 0
VAA
Text HLabel 4600 4100 2    50   Input ~ 0
GND
$Comp
L power:VAA #PWR0301
U 1 1 5C5E9C76
P 3700 4450
F 0 "#PWR0301" H 3700 4300 50  0001 C CNN
F 1 "VAA" H 3718 4623 50  0000 C CNN
F 2 "" H 3700 4450 50  0001 C CNN
F 3 "" H 3700 4450 50  0001 C CNN
	1    3700 4450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 4450 3700 4400
Connection ~ 3700 4400
$Comp
L Amplifier_Operational:TL084 U?
U 4 1 5C5F016D
P 4100 3750
AR Path="/5C5E4922/5C5F016D" Ref="U?"  Part="3" 
AR Path="/5C5EEFD0/5C5F016D" Ref="U201"  Part="4" 
F 0 "U201" H 4100 4050 50  0000 C CNN
F 1 "TLV9064" H 4200 3950 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4050 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 4150 3950 50  0001 C CNN
	4    4100 3750
	1    0    0    -1  
$EndComp
Connection ~ 4400 3750
$Comp
L Comparator:LM393 U202
U 2 1 5C5F14D0
P 5700 3650
F 0 "U202" H 5750 3950 50  0000 C CNN
F 1 "TS3702" H 5800 3850 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5700 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5700 3650 50  0001 C CNN
	2    5700 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4100 4500 4100
Connection ~ 4500 4100
Wire Wire Line
	4500 4100 4500 4050
Wire Wire Line
	4900 3200 4800 3200
$EndSCHEMATC
