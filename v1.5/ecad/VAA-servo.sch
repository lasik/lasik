EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5C5FB07B
P 4300 3450
AR Path="/5C5FAFFD/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB07B" Ref="R?"  Part="1" 
AR Path="/5C696261/5C5FB07B" Ref="R902"  Part="1" 
F 0 "R902" H 4241 3404 50  0000 R CNN
F 1 "10K" H 4241 3495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4300 3450 50  0001 C CNN
F 3 "~" H 4300 3450 50  0001 C CNN
	1    4300 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C5FB16A
P 4300 3150
AR Path="/5C5FAFFD/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB16A" Ref="R?"  Part="1" 
AR Path="/5C696261/5C5FB16A" Ref="R901"  Part="1" 
F 0 "R901" H 4241 3104 50  0000 R CNN
F 1 "10K" H 4241 3195 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4300 3150 50  0001 C CNN
F 3 "~" H 4300 3150 50  0001 C CNN
	1    4300 3150
	1    0    0    1   
$EndComp
Wire Wire Line
	4500 3500 4500 3650
Wire Wire Line
	4500 3650 5100 3650
Wire Wire Line
	5100 3650 5100 3400
Wire Wire Line
	4500 3300 4300 3300
Wire Wire Line
	4300 3300 4300 3350
Wire Wire Line
	4300 3250 4300 3300
Connection ~ 4300 3300
$Comp
L power:Earth #PWR?
U 1 1 5C5FB3EB
P 4300 3750
AR Path="/5C5FAFFD/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C5FB3EB" Ref="#PWR?"  Part="1" 
AR Path="/5C696261/5C5FB3EB" Ref="#PWR0901"  Part="1" 
F 0 "#PWR0901" H 4300 3500 50  0001 C CNN
F 1 "Earth" H 4300 3600 50  0001 C CNN
F 2 "" H 4300 3750 50  0001 C CNN
F 3 "~" H 4300 3750 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2950 4300 3050
Text HLabel 5200 3400 2    50   Input ~ 0
VAA
Text HLabel 3900 2950 0    50   Input ~ 0
V_analog
Text HLabel 3900 3700 0    50   Input ~ 0
GND
Connection ~ 4300 2950
Connection ~ 4300 3700
Wire Wire Line
	4300 3550 4300 3700
$Comp
L Device:Opamp_Dual_Generic U?
U 1 1 5C633015
P 4800 3400
AR Path="/5C5FAFFD/5C633015" Ref="U?"  Part="2" 
AR Path="/5C661B09/5C66211D/5C633015" Ref="U?"  Part="2" 
AR Path="/5C68A269/5C68A2E7/5C633015" Ref="U?"  Part="1" 
AR Path="/5C696261/5C633015" Ref="U901"  Part="1" 
F 0 "U901" H 4800 3767 50  0000 C CNN
F 1 "TLV9062" H 4800 3676 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4800 3400 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Dual_Generic U?
U 2 1 5C633372
P 5050 4000
AR Path="/5C5FAFFD/5C633372" Ref="U?"  Part="3" 
AR Path="/5C661B09/5C66211D/5C633372" Ref="U?"  Part="3" 
AR Path="/5C68A269/5C68A2E7/5C633372" Ref="U?"  Part="2" 
AR Path="/5C696261/5C633372" Ref="U901"  Part="2" 
F 0 "U901" H 5150 3650 50  0000 C CNN
F 1 "TLV9062" H 5150 3550 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5050 4000 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 5050 4000 50  0001 C CNN
	2    5050 4000
	1    0    0    -1  
$EndComp
Connection ~ 5100 3400
Wire Wire Line
	4150 3700 4300 3700
Wire Wire Line
	3900 3700 4150 3700
Connection ~ 4150 3700
Wire Wire Line
	4300 3700 4300 3750
Wire Wire Line
	4150 3900 4150 3700
Wire Wire Line
	4750 3900 4650 3900
Wire Wire Line
	5350 4250 5350 4000
Wire Wire Line
	4750 4100 4750 4250
Wire Wire Line
	4750 4250 5350 4250
$Comp
L Device:Opamp_Dual_Generic U?
U 3 1 5C6333F1
P 5950 3350
AR Path="/5C5FAFFD/5C6333F1" Ref="U?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C6333F1" Ref="U?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C6333F1" Ref="U?"  Part="3" 
AR Path="/5C696261/5C6333F1" Ref="U901"  Part="3" 
F 0 "U901" H 6250 3400 50  0000 R CNN
F 1 "TLV9062" H 6250 3300 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5950 3350 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tlv9062.pdf" H 5950 3350 50  0001 C CNN
	3    5950 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2950 4300 2950
Wire Wire Line
	5850 3750 5600 3750
Wire Wire Line
	4650 3750 4650 3900
Connection ~ 4650 3900
Wire Wire Line
	4650 3900 4150 3900
Wire Wire Line
	5100 3400 5200 3400
Wire Wire Line
	5850 3050 5850 2950
Wire Wire Line
	5850 2950 5600 2950
Wire Wire Line
	5850 3650 5850 3750
$Comp
L Device:C_Small C901
U 1 1 5C63F419
P 5600 3150
F 0 "C901" H 5200 3200 50  0000 L CNN
F 1 "100n" H 5200 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5600 3150 50  0001 C CNN
F 3 "~" H 5600 3150 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    5600 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3050 5600 2950
Connection ~ 5600 2950
Wire Wire Line
	5600 2950 4300 2950
Wire Wire Line
	5600 3250 5600 3750
Connection ~ 5600 3750
Wire Wire Line
	5600 3750 4650 3750
$EndSCHEMATC
