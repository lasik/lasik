EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5200 2350 900  500 
U 5C6AB623
F0 "sheet5C6AB61B" 50
F1 "Laser.sch" 50
F2 "Laser_enable" I R 6100 2750 50 
F3 "GND" I R 6100 2600 50 
F4 "3V3" I R 6100 2450 50 
F5 "LD+" I L 5200 2450 50 
F6 "LD-" I L 5200 2550 50 
$EndSheet
$Sheet
S 5200 3050 900  500 
U 5C6AC0E5
F0 "sheet5C6AC0E5" 50
F1 "Laser.sch" 50
F2 "Laser_enable" I R 6100 3450 50 
F3 "GND" I R 6100 3300 50 
F4 "3V3" I R 6100 3150 50 
F5 "LD+" I L 5200 3150 50 
F6 "LD-" I L 5200 3250 50 
$EndSheet
$Sheet
S 5200 3750 900  500 
U 5C6AC0EA
F0 "sheet5C6AC0EA" 50
F1 "Laser.sch" 50
F2 "Laser_enable" I R 6100 4150 50 
F3 "GND" I R 6100 4000 50 
F4 "3V3" I R 6100 3850 50 
F5 "LD+" I L 5200 3850 50 
F6 "LD-" I L 5200 3950 50 
$EndSheet
$Sheet
S 5200 4450 900  500 
U 5C6AC0EF
F0 "sheet5C6AC0EF" 50
F1 "Laser.sch" 50
F2 "Laser_enable" I R 6100 4850 50 
F3 "GND" I R 6100 4700 50 
F4 "3V3" I R 6100 4550 50 
F5 "LD+" I L 5200 4550 50 
F6 "LD-" I L 5200 4650 50 
$EndSheet
Wire Wire Line
	6100 4700 6150 4700
Wire Wire Line
	6150 4700 6150 4000
Wire Wire Line
	6150 2600 6100 2600
Wire Wire Line
	6100 3300 6150 3300
Connection ~ 6150 3300
Wire Wire Line
	6150 3300 6150 2600
Wire Wire Line
	6100 4000 6150 4000
Connection ~ 6150 4000
Wire Wire Line
	6150 4000 6150 3300
Wire Wire Line
	6100 4550 6200 4550
Wire Wire Line
	6200 4550 6200 3850
Wire Wire Line
	6200 2450 6100 2450
Wire Wire Line
	6100 3150 6200 3150
Connection ~ 6200 3150
Wire Wire Line
	6200 3150 6200 2450
Wire Wire Line
	6100 3850 6200 3850
Connection ~ 6200 3850
Wire Wire Line
	6200 3850 6200 3150
Text HLabel 6350 2750 2    50   Input ~ 0
L1
Text HLabel 6350 3450 2    50   Input ~ 0
L2
Text HLabel 6350 4150 2    50   Input ~ 0
L3
Text HLabel 6350 4850 2    50   Input ~ 0
L4
Wire Wire Line
	6350 4850 6100 4850
Wire Wire Line
	6350 4150 6100 4150
Wire Wire Line
	6350 3450 6100 3450
Wire Wire Line
	6350 2750 6100 2750
Text HLabel 6350 2450 2    50   Input ~ 0
3V3
Text HLabel 6350 2600 2    50   Input ~ 0
GND
Wire Wire Line
	6350 2600 6150 2600
Connection ~ 6150 2600
Wire Wire Line
	6350 2450 6200 2450
Connection ~ 6200 2450
Text HLabel 5050 2450 0    50   Input ~ 0
LD1+
Text HLabel 5050 2550 0    50   Input ~ 0
LD1-
Text HLabel 5050 3150 0    50   Input ~ 0
LD2+
Text HLabel 5050 3250 0    50   Input ~ 0
LD2-
Text HLabel 5050 3850 0    50   Input ~ 0
LD3+
Text HLabel 5050 3950 0    50   Input ~ 0
LD3-
Text HLabel 5050 4550 0    50   Input ~ 0
LD4+
Text HLabel 5050 4650 0    50   Input ~ 0
LD4-
Wire Wire Line
	5050 2450 5200 2450
Wire Wire Line
	5200 2550 5050 2550
Wire Wire Line
	5050 3150 5200 3150
Wire Wire Line
	5200 3250 5050 3250
Wire Wire Line
	5050 3850 5200 3850
Wire Wire Line
	5200 3950 5050 3950
Wire Wire Line
	5050 4550 5200 4550
Wire Wire Line
	5200 4650 5050 4650
$EndSCHEMATC
