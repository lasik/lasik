# Lasik 2 - The ReMakening
## More wireless, less wires

## Installing footprint libraries
in Pcbnew, Preferences > Footprint Libraries Wizard > Files on my computer, next > browse to lasik2 root directory, select the Lasik.pretty folder, next > To the current project only (unless you want system wide), Finish

## Installing schematic symbols
in Eeschema, Preferences > Component Libraries > Add (top one, at component library files) > select wanted files in \*-lib folder, Open. You can now Place the Component.

Preferably in that order (footprint first) since the schematic symbol refers to the footprint
