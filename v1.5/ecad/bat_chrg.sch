EESchema Schematic File Version 4
LIBS:power
LIBS:Lasik1.5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J?
U 1 1 5C67BA90
P 3000 3450
AR Path="/5C5FC5C6/5C67BA90" Ref="J?"  Part="1" 
AR Path="/5C67B796/5C67BA90" Ref="J1001"  Part="1" 
F 0 "J1001" H 2350 3100 50  0000 L CNN
F 1 "USB_B_Micro" H 2300 3000 50  0000 L CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 3150 3400 50  0001 C CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/1050170001_IO_CONNECTORS.pdf" H 3150 3400 50  0001 C CNN
	1    3000 3450
	1    0    0    -1  
$EndComp
Text HLabel 6500 3600 2    50   Input ~ 0
V_in
Text HLabel 2650 4050 0    50   Input ~ 0
GND
$Comp
L Battery_Management:MCP73831-2-OT U1001
U 1 1 5C680F4D
P 4850 3700
F 0 "U1001" H 4650 4100 50  0000 R CNN
F 1 "MCP73831" H 4750 4000 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 4900 3450 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 4700 3650 50  0001 C CNN
	1    4850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4050 3000 4050
Wire Wire Line
	4850 4050 4850 4000
Wire Wire Line
	4850 3400 4850 3250
$Comp
L Device:R_Small R?
U 1 1 5C6816E8
P 4400 3900
AR Path="/5C5FAFFD/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C696261/5C6816E8" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C6816E8" Ref="R1001"  Part="1" 
F 0 "R1001" H 4250 3900 50  0000 R CNN
F 1 "2K2" H 4250 4000 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 3900 50  0001 C CNN
F 3 "~" H 4400 3900 50  0001 C CNN
	1    4400 3900
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C6816F9
P 4300 3650
AR Path="/5C696261/5C6816F9" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C6816F9" Ref="C1001"  Part="1" 
F 0 "C1001" H 3950 3700 50  0000 L CNN
F 1 "4u7" H 3950 3600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4300 3650 50  0001 C CNN
F 3 "~" H 4300 3650 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    4300 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4050 4400 4000
Connection ~ 4400 4050
Wire Wire Line
	4400 4050 4850 4050
Wire Wire Line
	4450 3800 4400 3800
$Comp
L power:Earth #PWR01001
U 1 1 5C681C94
P 4850 4100
F 0 "#PWR01001" H 4850 3850 50  0001 C CNN
F 1 "Earth" H 4850 3950 50  0001 C CNN
F 2 "" H 4850 4100 50  0001 C CNN
F 3 "~" H 4850 4100 50  0001 C CNN
	1    4850 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4850 4050 4850 4100
Connection ~ 4850 4050
$Comp
L Device:Thermistor_PTC TH1001
U 1 1 5C682570
P 4050 3250
F 0 "TH1001" V 3750 3150 50  0000 R CNN
F 1 "Thermistor_PTC" V 3850 3450 50  0000 R CNN
F 2 "Fuse:Fuse_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4100 3050 50  0001 L CNN
F 3 "https://belfuse.com/resources/Datasheets/CircuitProtection/ds-CP-0zck-series.pdf" H 4050 3250 50  0001 C CNN
	1    4050 3250
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1001
U 1 1 5C682962
P 5050 3250
F 0 "D1001" H 4850 2950 50  0000 C CNN
F 1 "LED" H 4800 3050 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5050 3250 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-5178061/LH%20R974_EN.pdf" H 5050 3250 50  0001 C CNN
F 4 "preferably amber" H 0   0   50  0001 C CNN "Note"
	1    5050 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5C682C2F
P 5300 3400
AR Path="/5C5FAFFD/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C696261/5C682C2F" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C682C2F" Ref="R1002"  Part="1" 
F 0 "R1002" H 5250 3450 50  0000 R CNN
F 1 "1K" H 5250 3350 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5300 3400 50  0001 C CNN
F 3 "~" H 5300 3400 50  0001 C CNN
	1    5300 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 3300 5300 3250
Wire Wire Line
	5300 3250 5200 3250
$Comp
L Device:C_Small C?
U 1 1 5C6836EF
P 5450 3750
AR Path="/5C696261/5C6836EF" Ref="C?"  Part="1" 
AR Path="/5C67B796/5C6836EF" Ref="C1002"  Part="1" 
F 0 "C1002" H 5050 3750 50  0000 L CNN
F 1 "4u7" H 5050 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5450 3750 50  0001 C CNN
F 3 "~" H 5450 3750 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    5450 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 3650 5450 3600
Connection ~ 5450 3600
Wire Wire Line
	5450 3600 5250 3600
$Comp
L Device:D_Schottky D1002
U 1 1 5C686FD9
P 6150 3100
F 0 "D1002" H 6000 2900 50  0000 C CNN
F 1 "D_Schottky" H 5950 3000 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6150 3100 50  0001 C CNN
F 3 "~" H 6150 3100 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    6150 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 3300 5900 3100
Wire Wire Line
	5900 3100 6000 3100
Wire Wire Line
	6300 3100 6350 3100
Wire Wire Line
	6350 3100 6350 3600
Wire Wire Line
	6350 3600 6100 3600
Wire Wire Line
	6500 3600 6350 3600
Connection ~ 6350 3600
Connection ~ 5900 3100
$Comp
L Device:R_Small R?
U 1 1 5C689860
P 5900 2900
AR Path="/5C5FAFFD/5C689860" Ref="R?"  Part="1" 
AR Path="/5C661B09/5C66211D/5C689860" Ref="R?"  Part="1" 
AR Path="/5C68A269/5C68A2E7/5C689860" Ref="R?"  Part="1" 
AR Path="/5C696261/5C689860" Ref="R?"  Part="1" 
AR Path="/5C67B796/5C689860" Ref="R1003"  Part="1" 
F 0 "R1003" H 5800 2950 50  0000 R CNN
F 1 "10K" H 5800 2850 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 2900 50  0001 C CNN
F 3 "~" H 5900 2900 50  0001 C CNN
	1    5900 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3250 4300 3250
Wire Wire Line
	3900 3250 3450 3250
Wire Wire Line
	3000 3850 3000 3900
Connection ~ 3000 4050
Wire Wire Line
	3000 4050 3800 4050
Wire Wire Line
	5450 4050 5450 4150
$Sheet
S 5350 4150 300  900 
U 5C68A220
F0 "Protected_cell" 50
F1 "Protected_cell.sch" 50
F2 "V_bat" I T 5550 4150 50 
F3 "GND" I T 5450 4150 50 
$EndSheet
Wire Wire Line
	5450 3600 5550 3600
Wire Wire Line
	4850 4050 5450 4050
Wire Wire Line
	5450 3850 5450 4050
Connection ~ 5450 4050
Wire Wire Line
	5550 3600 5550 4150
Connection ~ 5550 3600
Wire Wire Line
	5550 3600 5700 3600
$Comp
L Device:Q_PMOS_GSD Q1001
U 1 1 5C69506F
P 5900 3500
F 0 "Q1001" V 6150 3250 50  0000 C CNN
F 1 "DMP3099L-7" V 6250 3200 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 6100 3600 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMP3099L.pdf" H 5900 3500 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Note"
	1    5900 3500
	0    -1   1    0   
$EndComp
Wire Wire Line
	4300 3550 4300 3250
Connection ~ 4300 3250
Wire Wire Line
	4300 3250 4850 3250
Wire Wire Line
	4300 3750 4300 4050
Connection ~ 4300 4050
Wire Wire Line
	4300 4050 4400 4050
Wire Wire Line
	2900 3850 2900 3900
Wire Wire Line
	2900 3900 3000 3900
Connection ~ 3000 3900
Wire Wire Line
	3000 3900 3000 4050
Wire Wire Line
	4850 3250 4850 3100
Connection ~ 4850 3250
Wire Wire Line
	4900 3250 4850 3250
Wire Wire Line
	5250 3800 5300 3800
Wire Wire Line
	4850 3100 5900 3100
$Comp
L power:Earth #PWR01002
U 1 1 5C6A4A03
P 6050 2800
F 0 "#PWR01002" H 6050 2550 50  0001 C CNN
F 1 "Earth" H 6050 2650 50  0001 C CNN
F 2 "" H 6050 2800 50  0001 C CNN
F 3 "~" H 6050 2800 50  0001 C CNN
	1    6050 2800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 2800 5900 2750
Wire Wire Line
	5900 2750 6050 2750
Wire Wire Line
	6050 2750 6050 2800
Wire Wire Line
	5900 3000 5900 3100
Wire Wire Line
	5300 3500 5300 3800
NoConn ~ 3300 3650
NoConn ~ 3300 3550
NoConn ~ 3300 3450
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5CCFAD4D
P 3800 3650
AR Path="/5C67B796/5C68A220/5CCFAD4D" Ref="J?"  Part="1" 
AR Path="/5C67B796/5CCFAD4D" Ref="J1002"  Part="1" 
F 0 "J1002" V 4000 3050 50  0000 C CNN
F 1 "Conn_01x02_Male" V 4100 3250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3800 3650 50  0001 C CNN
F 3 "~" H 3800 3650 50  0001 C CNN
	1    3800 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 3850 3800 4050
Connection ~ 3800 4050
Wire Wire Line
	3800 4050 4300 4050
Wire Wire Line
	3700 3850 3700 3900
Wire Wire Line
	3700 3900 3450 3900
Wire Wire Line
	3450 3900 3450 3250
Connection ~ 3450 3250
Wire Wire Line
	3450 3250 3300 3250
Text Notes 3800 2500 0    50   ~ 0
Caution!\nthe marking for the red LED is the reverse of the green model!
$EndSCHEMATC
