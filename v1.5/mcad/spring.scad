rLaser = 3.0625; // radius of lasermodule
border = 1; // minimum material width

height = 50;
width = height;
depth = 1;

slot_width = 2;
bridge_width = 3;
center_size = 20;

laser_holder_width = 1;
laser_holder_height = 10;

r_stick = 2;
stick_extra = 2;
stick_ridge = 5;

r_screw = 1;
offset_screw = 5; // a measure to move the screw holes away from the corners

// =====================================================

$fn = 100;

// =====================================================

slot_length = center_size + 2 * slot_width;

// =====================================================

module base() {
    translate([0,0,depth/2])
    cube([width, height, depth], center = true );
}

module slot() {
    _depth = depth + 0.2;
    
    translate([-slot_width - bridge_width,0,0])
    translate([-slot_width - center_size / 2, -slot_width - center_size / 2,-0.1])
    {
        cube([slot_width, slot_length, _depth]);
        cube([slot_length, slot_width, _depth]);
    }
}

module spring() {
    difference(){
        base();

        slot();
        rotate([0,0,90]) slot();
        rotate([0,0,180]) slot();
        rotate([0,0,270]) slot();
    }
}

module laserHole() {
    translate([0,0,-.1]) cylinder(h=laser_holder_height + 0.2, r=rLaser);
}

module laserHolder() {
    cylinder(h=laser_holder_height, r=rLaser + laser_holder_width);
}

module joystick() {
    rotate([0,0,45])
    translate([r_stick + rLaser, 0, 0])
    union()
    {
        cylinder(h=laser_holder_height + stick_extra, r = r_stick);
        translate([0, -r_stick, 0])
        cube([stick_ridge, 2 * r_stick, laser_holder_height + stick_extra]);
    }
}

module screwHoles() {
    module corner() {
        translate([-offset_screw, -offset_screw, 0])
        translate([width / 2 - r_screw, height / 2 - r_screw, -.1])
        cylinder(h = depth + 0.2, r = r_screw);
    }

    _shift = r_stick / sqrt(2);
    translate([_shift, - _shift, 0]) corner();
    translate([-_shift, _shift, 0]) corner();
}

difference()
{
    union() {
        spring();
        joystick();
        rotate([0,0,90]) joystick();
        laserHolder();
    }
    laserHole();
    screwHoles();
    rotate([0,0,90]) screwHoles();
}

