a lasergame platform developed @ [Hackerspace.Gent](https://hackerspace.gent/)

# hardware
see [pcb/](pcb/)

# firmware
see [firmware/](firmware/)

# software
* see [software/LasikService](software/LasikService) [![PyPI version](https://badge.fury.io/py/lasikservice.svg)](https://badge.fury.io/py/lasikservice)
* see [software/Games](software/Games)

# cad
see [cad/](cad/) e.g. [cad/detector/detector.stl](cad/detector/detector.stl)

# pics
![A test setup and the Lasik board](images/board_and_room.jpg)
