import socket
import threading

def debug(msg):
    # overwrite with your callback for debug messages
    pass

def onSubThreadCrash():
    # overwrite with your callback in order to gracefully shut down other threads
    pass

# ==================================================================================================

class SocketServer:
    """Take line separated commands on a socket and pass them to a handler"""
    def __init__(self, addr, handler):
        self.addr = addr
        self.handler = handler
        self.sock = socket.socket()
        self.f = None

    def start(self):
        debug("setting up and starting socket handling thread")
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.addr)
        self.sock.listen(0) #not sure about backlog parameter
        threading.Thread(target = self._loop, name = "SocketServer", daemon = True).start()

    def send(self, msg):
        f = self.f
        if f and not f.closed:
            msg += "\n"
            try:
                f.write(msg)
                f.flush()
            except BrokenPipeError:
                # This can happen as a race condition when losing connection
                # but is not a problem
                debug("warning, failed to write msg to socket, maybe it just closed")

    def _loop(self):
        while True:
            conn, addr = self.sock.accept()
            debug("accepted connection from {}".format(addr))
            with conn:
                self.f = conn.makefile(
                            mode = 'rw', buffering = 1,
                            encoding = 'ascii', errors = 'replace')
                while True:
                    line = self.f.readline()
                    if not line or line[-1] != '\n': break
                    line = line[:-1]
                    self.handler(line)
                self.f = None
            debug("closed connection with {}".format(addr))

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.sock.__exit__(*args)

# ==================================================================================================

def demo():
    global debug

    import sys
    from os.path import basename

    if len(sys.argv) != 3:
        print("usage example: '{} HOST PORT'".format(basename(sys.argv[0])), file = sys.stderr)
        exit(1)
    host, port = sys.argv[1], int(sys.argv[2])

    debug = lambda msg: print("-- {} --".format(msg), file = sys.stderr)
    handler = lambda msg: print("someone says '{}'".format(msg))

    with SocketServer((host, port), handler) as ss:
        ss.start()

        for line in sys.stdin:
            ss.send("master says '{}'".format(line))

if __name__ == "__main__":
    demo()
