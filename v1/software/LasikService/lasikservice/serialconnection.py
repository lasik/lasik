#!/usr/bin/env python3

import re
import serial
import threading
import sys
from serial.threaded import ReaderThread, LineReader

def debug(msg):
    # overwrite with your callback for debug messages
    pass

def onSubThreadCrash():
    # overwrite with your callback in order to gracefully shut down other threads
    pass

# ==================================================================================================

class AbstractProtocol(LineReader):
    TERMINATOR = b'\n'
    ENCODING = 'ascii'
    UNICODE_HANDLING = 'replace'
    recvHandler = None

    def __init__(self, *args):
        super().__init__(*args)
        self.awaitEvent = None
        self.awaitReqResp = ""

    def handle_line(self, line):
        if self.awaitEvent and not self.awaitEvent.is_set():  # waiting for matching response
            m = re.match(self.awaitReqResp, line)
            if m:
                self.awaitReqResp = line
                self.awaitEvent.set() # signal to waiting thread the response is ready
        type(self).recvHandler(line)

    def awaitResponse(self, line, response_regex):
        self.awaitReqResp = response_regex
        self.awaitEvent = threading.Event() # signals we are waiting for a response
        self.write_line(line)
        self.awaitEvent.wait() # blocks until _t_listen thread released it
        self.awaitEvent = None
        return self.awaitReqResp # handle_line wrote the answer into this var

    def connection_made(self, transport):
        super().connection_made(transport)
        debug('port opened')

    def connection_lost(self, exception):
        debug('port closed')
        super().connection_lost(exception) # can raise an exception

    def getPath(self):
        return type(self).path

    def close(self):
        self.transport.close()

# ==================================================================================================

def demo():
    """
    Example usage, and a basic tool to send/receive line separated messages on a serial device
    """
    global debug

    import sys
    from os.path import basename

    if len(sys.argv) != 2:
        print("usage example: '{} /dev/ttyACM0'".format(basename(sys.argv[0])), file=sys.stderr)
        exit(1)
    path = sys.argv[1]

    debug = lambda msg: print("-- {} --".format(msg), file = sys.stderr)


    class Protocol(AbstractProtocol):
        pass

    Protocol.recvHandler = lambda line: print("--> {}".format(line))
    Protocol.path = path
    ser = serial.serial_for_url(path)

    with ReaderThread(ser, Protocol) as l:
        for line in sys.stdin:
            l.write_line(line.rstrip('\n'))

if __name__ == "__main__":
    demo()
