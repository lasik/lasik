#!/usr/bin/env python3

import time
from killswitch import Killswitch
from lasikclient import LasikClient
import threading
import re

# ==============================================================================
class StaticAnimator:
    """Sends the pwm value once"""
    def __init__(self, value):
        self.value = value
        self.done = False
    def getPwm(self):
        if self.done:       # already set the value once
            return None     # None means no update
        else:
            self.done = True
            return self.value

class Beam:
    # this class is to be passed and handled by GameLogic
    # it will drive GameLogic (call its eh_s), be configured by GameLogic
    # and be driven by LaserRoom (LaserRoom will call its eh_s)
    def __init__(self, beam_state, debug = lambda x: None):
        self.beam_state = beam_state
        self._debug = debug
        self.animator = StaticAnimator(255)
        self._listeners = {}

    def eh_break(self):
        self.beam_state = 0
        #TODO : some beam mode stuff
        self._event("break")

    def eh_clear(self):
        self.beam_state = 1
        #TODO : some beam mode stuff
        self._event("clear")

    def eh_tick(self):
        #TODO : some beam mode stuff, but no listeners
        pass

    def _event(self, event_name, event_data = None):
        self._debug("event: '{}'".format(event_name))
        if event_name in self._listeners:
            for handler, listener_data in self._listeners[event_name]:
                handler(self, listener_data, event_data)

    def subscribe(self, event_name, handler, listener_data = None):
        self._listeners.setdefault(event_name,[]).append((handler,listener_data))

    def unsubscribeAll(self):
        self._listeners = {}

class LaserRoom:
    def __init__(self, killswitch, debug = lambda x: None):
        self._debug = debug
        self.lc = LasikClient(
                killswitch,
                debug = lambda x: debug("(lc) {}".format(x)))
        self.lc.connect()
        self.lc.run()

        with killswitch:
            self.beams = self._initBeams()

        # start the beam animation thread
        anim_thread = killswitch.looped_thread(name = "animate", debug = debug)(self._animate)
        anim_thread.start()

    def getBeams(self):
        return self.beams

    def handleLasikEvents(self):
        while True:
            msg = self.lc.getMsg(block = False)
            if msg == None: break
            m = re.match("([0-9]{1,2}) ([01])$", msg)
            if m:
                laser_i, state = map(int, m.groups())
                if state == 0:
                    self.beams[laser_i].eh_break()
                else:
                    self.beams[laser_i].eh_clear()

    def handleTick(self):
        for b in self.beams:
            b.eh_tick()

    def _initBeams(self):
        self.lc.sendMsg("get all")
        m = False
        while not m:
            msg = self.lc.getMsg(block = True)
            if msg == None:     # timed out
                # warning this error will not be raised when other msgs keep coming
                raise Exception("timeout waiting for reply to 'get all'")
            m = re.match("all ([01]*)$", msg)
        allbeambits, = m.groups()
        beams = []
        for i,bs_string in enumerate(allbeambits):
            beam_state = int(bs_string)
            debug = lambda msg, i = i: self._debug("(beam{}) {}".format(i,msg))
            beams.append(Beam(beam_state = beam_state, debug = debug))
        return beams

    def _animate(self):
        # used within a in threaded infinite loop
        for beam_i, b in enumerate(self.beams):
            if b.animator:
                pwm = b.animator.getPwm()
                if pwm:
                    self.lc.sendMsg("set {} {}".format(beam_i, pwm))
        time.sleep(0.02)

class GameLogic: #TODO
    def __init__(self, beams, dashboard, audio, debug = lambda x: None):
        self.beams = beams
        self.dashboard = dashboard
        self.audio = audio
        self._debug = debug

        self._dummySetup()

    def handleTick(self):
        pass

    def _dummySetup(self):
        for beam_i, b in enumerate(self.beams):
            b.unsubscribeAll()
            b.subscribe("break", self._eh_break, beam_i)

    def _eh_break(self, sender, listener_data, event_data):
        beam_i = listener_data
        self._debug("broke beam {}".format(beam_i))

class Audio: #TODO
    def __init__(self):
        pass

class Dashboard: #TODO
    def __init__(self):
        pass

    def draw(self):
        time.sleep(0.1) #FIXME

def main():
    debugLock = threading.Lock()
    def debug(msg):
        with debugLock:
            print(msg)

    killswitch = Killswitch(debug = debug)

    laserRoom = LaserRoom(                              # connect to lasik service
            killswitch = killswitch,
            debug = lambda msg: debug("(lr) {}".format(msg)))
    beams = laserRoom.getBeams()                        # get the available beams
    dashboard = Dashboard()
    audio = Audio()
    gameLogic = GameLogic(                              # init game logic
            beams, dashboard, audio,
            debug = lambda msg: debug("(gl) {}".format(msg)))

    with killswitch as k:
        while not k.is_set():
            laserRoom.handleLasikEvents()   # -> lasik msgs -> Beam's -> gameLogic
            laserRoom.handleTick()          # -> Beam's -> gameLogic
            gameLogic.handleTick()          # -> gameLogic
            # draw should wait for tick
            dashboard.draw()                # relies on bindings to game state set by gameLogic

if __name__ == "__main__":
    main()
