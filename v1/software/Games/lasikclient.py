#!/usr/bin/env python3

import socket
import queue
import threading

class LasikClient:
    def __init__(self, killswitch, timeout = 1, debug = lambda x: None):
        self.killswitch = killswitch
        self.socket = None
        self.timeout = timeout
        self.debug = debug
        self.debugLock = threading.Lock()
        self.fromLasik = queue.Queue(maxsize = 1000)
        self.toLasik = queue.Queue(maxsize = 1000)

    def connect(self, host = "localhost", port = 1337):
        with self.killswitch:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((host,port))
                self.socket.settimeout(self.timeout)
            except ConnectionError:
                raise ConnectionError("could not connect to server socket, is the server running?")

    def run(self):
        with self.killswitch:
            assert self.socket
        self.killswitch.looped_thread("_listen", self.debug)(self._listen).start()
        self.killswitch.looped_thread("_send", self.debug)(self._send).start()

    def sendMsg(self, msg):
        self.toLasik.put(msg, block = False) # raises Full exception

    def getMsg(self, block):
        try:
            result = self.fromLasik.get(block = block, timeout = self.timeout)
        except queue.Empty:
            result = None
        return result

    def _listen(self): #TODO framing thing see _t_socketConnListener (socketserver.py)
        try:
            msg = self.socket.recv(1024).decode('ascii').strip() # TODO errorhandling
        except socket.timeout:
            return
        self.fromLasik.put(msg, block = False) # raises Full exception

    def _send(self):
        try:
            msg = self.toLasik.get(timeout = self.timeout)
        except queue.Empty:
            return
        msg += '\n'
        self.socket.sendall(msg.encode('ascii')) # exception if blocks longer than timeout

def demo():
    from killswitch import Killswitch
    import sys

    killswitch = Killswitch(debug = print)

    lc = LasikClient(killswitch, debug = print)
    lc.connect()
    lc.run()

    @killswitch.looped_thread(name = "echo", debug = print)
    def echo(lc = lc):
        msg = lc.getMsg(block = True)
        if msg: print(msg)

    echo.start()

    for line in sys.stdin:
        lc.sendMsg(line.strip())
    killswitch.set()

if __name__ == "__main__":
    demo()
