arduino based firmware for the lasik board

## serial communication protocol examples:

#### messages pushed from lasik to host
```
    A 1
    B 0
    error <some error text>
```

#### messages accepted by lasik
* in order to set brightness:
```
    set A 255
    set B 0
    set all 255
```
* to configure beam detection:
```
    conf all <nPre> <preLength> <nPulse> <pulseLength> <tolerance> <beamcheckInterval> <debounce>
    conf all 5 10 7 30 0 25 0
    conf A 5 10 7 30 0 25 1
    conf B 5 10 7 30 0 25 1
```
* to get all beams statuses:
```
    get all
```
response:
```
    all 111000101111
```
* to see which lasers match which sensor
```
    get match
```
* to get firmware version:
```
    version
```
