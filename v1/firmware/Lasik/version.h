// versioning according to https://semver.org/spec/v1.0.0.html
// for 0.y.z, the minor component (y) gets increased for any functionality
// change regardless of backwards compatibility, the patch (z) for any
// change except the trivial stuff

#define FIRMWARE_VERSION_STRING "0.2.9"
