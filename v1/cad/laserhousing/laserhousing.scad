rWire = 1.7; // radius of the wire
rLaser = 3.0625; // radius of lasermodule
border = 1; // minimum material width

height = 50;
width = height;
depth = 20;

inner_h = height / 1.5;
inner_w = inner_h;
inner_d = 7;

// =====================================================

$fn = 100;

// =====================================================
module enclosure() {
    translate([-width/2, -height/2, 0]) difference() {
        cube([width, height, depth]);
        
        translate([(width - inner_w)/2, (height - inner_h)/2, inner_d])
        cube([inner_w, inner_h, depth - inner_d - border]);
    }
    
    translate([-inner_w/2, -inner_h/2, border])
    cube([inner_w, inner_h, inner_d]);
}

module laserTube(length) {
    translate([0,0,-length/2])
    translate([0,0,-1]) cylinder(h=length, r=rLaser);
}

module tubeArray() {
    n = 8; // n tubes
    maxSlope = 60;
    for(i = [0 : n-1]) {
        angle = i * 360 / n;
        slope = i * maxSlope / (n - 1);
        rotate([0,0,angle]) translate([width/5, 0, 0])
        rotate([0,-slope,0]) laserTube(depth * 3);
    }
}

module full() {
    difference() {
        enclosure();
        n = 4;
        for(i = [0 : n-1]) {
            angle = i * 360 / n;
            rotate([0,0,angle])
            translate([0,0,depth - rWire - 2 * border]) rotate([90,0,0]) cylinder(h = width, r = rWire);
        }
    }
}

module topSlicer() {
    notch = (width - inner_w) / 3;
    translate([-width/2 - 1, -height/2 - 1, depth - 2*border - rWire])
    cube([width+2, height+2, depth]);
    for(angle = [0,90,180,270]){
        rotate([0,0,angle])
        translate([width/2 - notch+1, height/2 - notch+1, depth - 2*border - rWire - notch])
        cube(notch+1);
    }
}

module asbak() intersection() {
    full();
    topSlicer();
}

module bottom() difference() {
    full();
    topSlicer();
    translate([0,0,inner_d]) tubeArray();
}

translate([0,0,height]) asbak();
bottom();