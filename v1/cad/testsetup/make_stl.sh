#!/usr/bin/env bash

for item in da_back da_front la_full; do
    scad_fn=tmp_${item}.scad
    stl_fn=${item}.stl
    cat << EOF > ${scad_fn}
use <array.scad>
${item}();
EOF
    (
        echo "--> starting build" ${stl_fn}
        openscad -o ${stl_fn} ${scad_fn}
        rm ${scad_fn};
        echo "<== finished build" ${stl_fn}
    ) &
    pids[$i]=$!
    i=$((i+1))
done;

for pid in ${pids[*]}; do
    wait $pid
done

echo all sub-processes finished
