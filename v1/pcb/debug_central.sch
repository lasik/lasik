EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 41 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6050 5200 3    60   Input ~ 0
bp_pwm_debug
Text GLabel 6350 5200 3    60   Input ~ 0
laser_current_debug
Text GLabel 4950 5250 3    60   Input ~ 0
amped_sense_debug
Text GLabel 5250 5250 3    60   Input ~ 0
otto_RC_debug
Text GLabel 5550 5250 3    60   Input ~ 0
otto_triggered_debug
$Comp
L power:+12V #PWR?
U 1 1 5AFF8EEF
P 6200 5200
F 0 "#PWR?" H 6200 5050 50  0001 C CNN
F 1 "+12V" V 6200 5450 50  0000 C CNN
F 2 "" H 6200 5200 50  0000 C CNN
F 3 "" H 6200 5200 50  0000 C CNN
	1    6200 5200
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5AFF8EF5
P 5400 5250
F 0 "#PWR?" H 5400 5100 50  0001 C CNN
F 1 "+3.3V" V 5400 5500 50  0000 C CNN
F 2 "" H 5400 5250 50  0000 C CNN
F 3 "" H 5400 5250 50  0000 C CNN
	1    5400 5250
	-1   0    0    1   
$EndComp
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFF8EFB
P 4800 5250
F 0 "#PWR?" H 4800 5050 50  0001 C CNN
F 1 "GNDPWR" V 4800 5000 50  0000 C CNN
F 2 "" H 4800 5200 50  0000 C CNN
F 3 "" H 4800 5200 50  0000 C CNN
	1    4800 5250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5AFF8F1C
P 5100 5250
F 0 "#PWR?" H 5100 5100 50  0001 C CNN
F 1 "+5V" V 5100 5450 50  0000 C CNN
F 2 "" H 5100 5250 50  0000 C CNN
F 3 "" H 5100 5250 50  0000 C CNN
	1    5100 5250
	-1   0    0    1   
$EndComp
Text GLabel 6350 2950 1    60   Input ~ 0
stm32_reset_debug
$Comp
L power:+12V #PWR?
U 1 1 5AFFC275
P 6100 3000
F 0 "#PWR?" H 6100 2850 50  0001 C CNN
F 1 "+12V" V 6100 3250 50  0000 C CNN
F 2 "" H 6100 3000 50  0000 C CNN
F 3 "" H 6100 3000 50  0000 C CNN
	1    6100 3000
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFFC2C8
P 6200 3000
F 0 "#PWR?" H 6200 2800 50  0001 C CNN
F 1 "GNDPWR" V 6200 2750 50  0000 C CNN
F 2 "" H 6200 2950 50  0000 C CNN
F 3 "" H 6200 2950 50  0000 C CNN
	1    6200 3000
	-1   0    0    1   
$EndComp
$Comp
L lasik-rescue:CONN_01X03 P4102
U 1 1 5B07677D
P 6200 3250
AR Path="/5B07677D" Ref="P4102"  Part="1" 
AR Path="/5AFF8C1E/5B07677D" Ref="P4102"  Part="1" 
F 0 "P4102" H 6200 3450 50  0000 C CNN
F 1 "CONN_01X03" V 6300 3250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6200 3250 50  0001 C CNN
F 3 "" H 6200 3250 50  0000 C CNN
	1    6200 3250
	0    1    1    0   
$EndComp
$Comp
L lasik-rescue:CONN_01X03 P4103
U 1 1 5B076878
P 6200 4900
AR Path="/5B076878" Ref="P4103"  Part="1" 
AR Path="/5AFF8C1E/5B076878" Ref="P4103"  Part="1" 
F 0 "P4103" H 6200 5100 50  0000 C CNN
F 1 "CONN_01X03" V 6300 4900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6200 4900 50  0001 C CNN
F 3 "" H 6200 4900 50  0000 C CNN
	1    6200 4900
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:CONN_01X06 P4101
U 1 1 5B0769A2
P 5200 4900
AR Path="/5B0769A2" Ref="P4101"  Part="1" 
AR Path="/5AFF8C1E/5B0769A2" Ref="P4101"  Part="1" 
F 0 "P4101" H 5200 5250 50  0000 C CNN
F 1 "CONN_01X06" V 5300 4900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 5200 4900 50  0001 C CNN
F 3 "" H 5200 4900 50  0000 C CNN
	1    5200 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 3000 6100 3050
Wire Wire Line
	6200 3000 6200 3050
Wire Wire Line
	6300 3000 6300 3050
Wire Wire Line
	6200 5100 6200 5200
Wire Wire Line
	6100 5100 6100 5150
Wire Wire Line
	6100 5150 6050 5150
Wire Wire Line
	6050 5150 6050 5200
Wire Wire Line
	6300 5100 6300 5150
Wire Wire Line
	6300 5150 6350 5150
Wire Wire Line
	6350 5150 6350 5200
Wire Wire Line
	5100 5250 5100 5150
Wire Wire Line
	5100 5150 5150 5150
Wire Wire Line
	5150 5150 5150 5100
Wire Wire Line
	5250 5250 5250 5100
Wire Wire Line
	5400 5250 5400 5150
Wire Wire Line
	5400 5150 5350 5150
Wire Wire Line
	5350 5150 5350 5100
Wire Wire Line
	5550 5250 5550 5150
Wire Wire Line
	5550 5150 5450 5150
Wire Wire Line
	5450 5150 5450 5100
Wire Wire Line
	4950 5250 4950 5150
Wire Wire Line
	4950 5150 5050 5150
Wire Wire Line
	5050 5150 5050 5100
Wire Wire Line
	4800 5250 4800 5100
Wire Wire Line
	4800 5100 4950 5100
Wire Wire Line
	6350 3000 6300 3000
Wire Wire Line
	6350 2950 6350 3000
$EndSCHEMATC
