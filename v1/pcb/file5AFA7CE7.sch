EESchema Schematic File Version 2
LIBS:lasik-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32duino_boards
LIBS:bluepill_breakouts
LIBS:lasik-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 51
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4200 1100 2000 2400
U 5AFA9BD5
F0 "sense amplifier 2" 60
F1 "sense_amp.sch" 60
F2 "photo_amplified" I R 6200 1350 60 
F3 "sense_voltage" I R 6200 3300 60 
$EndSheet
$Sheet
S 6550 1100 1850 2400
U 5AFA9BD8
F0 "sense amplifier 3" 60
F1 "sense_amp.sch" 60
F2 "photo_amplified" I R 8400 1300 60 
F3 "sense_voltage" I R 8400 3300 60 
$EndSheet
$Sheet
S 1650 4900 2350 2250
U 5AFA9BE9
F0 "sense voltage supply" 60
F1 "file5AFA9BE8.sch" 60
F2 "sense_voltage" I R 4000 5100 60 
$EndSheet
Wire Wire Line
	8400 3300 8600 3300
Wire Wire Line
	8600 3300 8600 3750
Wire Wire Line
	8600 3750 3900 3750
Wire Wire Line
	6350 3750 6350 3300
Wire Wire Line
	6350 3300 6200 3300
Wire Wire Line
	3750 3350 3900 3350
Wire Wire Line
	3900 3350 3900 3750
Connection ~ 6350 3750
Wire Wire Line
	4000 5100 4200 5100
Wire Wire Line
	4200 5100 4200 3750
Connection ~ 4200 3750
Text HLabel 10050 1150 2    60   Input ~ 0
sensor_1
Text HLabel 10050 1400 2    60   Input ~ 0
Sensor_2
Text HLabel 10050 1650 2    60   Input ~ 0
sensor_3
$Sheet
S 1550 1100 2200 2450
U 5AFA819D
F0 "sense amplifier 1" 60
F1 "sense_amp.sch" 60
F2 "photo_amplified" I R 3750 1350 60 
F3 "sense_voltage" I R 3750 3350 60 
$EndSheet
Wire Wire Line
	8400 1300 8500 1300
Wire Wire Line
	8500 1300 8500 1650
Wire Wire Line
	8500 1650 10050 1650
Wire Wire Line
	6200 1350 6300 1350
Wire Wire Line
	6300 1350 6300 950 
Wire Wire Line
	6300 950  8600 950 
Wire Wire Line
	8600 950  8600 1400
Wire Wire Line
	8600 1400 10050 1400
Wire Wire Line
	3750 1350 3850 1350
Wire Wire Line
	3850 1350 3850 800 
Wire Wire Line
	3850 800  8750 800 
Wire Wire Line
	8750 800  8750 1150
Wire Wire Line
	8750 1150 10050 1150
$EndSCHEMATC
