EESchema Schematic File Version 2
LIBS:lasik-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32duino_boards
LIBS:bluepill_breakouts
LIBS:lasik-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 35 36
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1550 2700 0    60   Input ~ 0
amplified_sensor1
Text HLabel 1550 2950 0    60   Input ~ 0
amplified_sensor2
Text HLabel 1550 3200 0    60   Input ~ 0
amplified_sensor3
Text HLabel 1550 3600 0    60   Input ~ 0
amplified_sensor4
Text HLabel 1550 3850 0    60   Input ~ 0
amplified_sensor5
Text HLabel 1550 4100 0    60   Input ~ 0
amplified_sensor6
Text HLabel 1550 4500 0    60   Input ~ 0
amplified_sensor7
Text HLabel 1550 4750 0    60   Input ~ 0
amplified_sensor8
Text HLabel 1550 5000 0    60   Input ~ 0
amplified_sensor9
Text HLabel 1550 5400 0    60   Input ~ 0
amplified_sensor10
Text HLabel 1550 5650 0    60   Input ~ 0
amplified_sensor11
Text HLabel 1550 5900 0    60   Input ~ 0
amplified_sensor12
$EndSCHEMATC
