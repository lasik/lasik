EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 24 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1550 2700 0    60   Input ~ 0
amplified_sensor1
Text HLabel 1550 2950 0    60   Input ~ 0
amplified_sensor2
Text HLabel 1550 3200 0    60   Input ~ 0
amplified_sensor3
Text HLabel 1550 3600 0    60   Input ~ 0
amplified_sensor4
Text HLabel 1550 3850 0    60   Input ~ 0
amplified_sensor5
Text HLabel 1550 4100 0    60   Input ~ 0
amplified_sensor6
Text HLabel 1550 4500 0    60   Input ~ 0
amplified_sensor7
Text HLabel 1550 4750 0    60   Input ~ 0
amplified_sensor8
Text HLabel 1550 5000 0    60   Input ~ 0
amplified_sensor9
Text HLabel 1550 5400 0    60   Input ~ 0
amplified_sensor10
Text HLabel 1550 5650 0    60   Input ~ 0
amplified_sensor11
Text HLabel 1550 5900 0    60   Input ~ 0
amplified_sensor12
$Sheet
S 1750 2650 1600 550 
U 5AFCC2D5
F0 "otto4-1" 60
F1 "otto4.sch" 60
F2 "amped_sense1" I L 1750 2700 60 
F3 "amped_sense2" I L 1750 2850 60 
F4 "amped_sense3" I L 1750 3000 60 
F5 "amped_sense4" I L 1750 3150 60 
F6 "triggered_out1" I R 3350 2700 60 
F7 "triggered_out2" I R 3350 2850 60 
F8 "triggered_out3" I R 3350 3000 60 
F9 "triggered_out4" I R 3350 3150 60 
$EndSheet
Text HLabel 3600 2700 2    60   Input ~ 0
triggered_out1
Text HLabel 3600 2950 2    60   Input ~ 0
triggered_out2
Text HLabel 3600 3200 2    60   Input ~ 0
triggered_out3
Text HLabel 3600 3600 2    60   Input ~ 0
triggered_out4
$Sheet
S 1750 3800 1650 550 
U 5AFCF0A3
F0 "otto4-2" 60
F1 "otto4.sch" 60
F2 "amped_sense1" I L 1750 3850 60 
F3 "amped_sense2" I L 1750 4000 60 
F4 "amped_sense3" I L 1750 4150 60 
F5 "amped_sense4" I L 1750 4300 60 
F6 "triggered_out1" I R 3400 3850 60 
F7 "triggered_out2" I R 3400 4000 60 
F8 "triggered_out3" I R 3400 4150 60 
F9 "triggered_out4" I R 3400 4300 60 
$EndSheet
Text HLabel 3600 3850 2    60   Input ~ 0
triggered_out5
Text HLabel 3600 4100 2    60   Input ~ 0
triggered_out6
Text HLabel 3600 4500 2    60   Input ~ 0
triggered_out7
Text HLabel 3600 4750 2    60   Input ~ 0
triggered_out8
$Sheet
S 1750 4950 1650 550 
U 5AFD08C6
F0 "otto4-3" 60
F1 "otto4-debug.sch" 60
F2 "amped_sense1" I L 1750 5000 60 
F3 "amped_sense2" I L 1750 5150 60 
F4 "amped_sense3" I L 1750 5300 60 
F5 "amped_sense4" I L 1750 5450 60 
F6 "triggered_out1" I R 3400 5000 60 
F7 "triggered_out2" I R 3400 5150 60 
F8 "triggered_out3" I R 3400 5300 60 
F9 "triggered_out4" I R 3400 5450 60 
$EndSheet
Text HLabel 3600 5000 2    60   Input ~ 0
triggered_out9
Text HLabel 3600 5400 2    60   Input ~ 0
triggered_out10
Text HLabel 3600 5650 2    60   Input ~ 0
triggered_out11
Text HLabel 3600 5900 2    60   Input ~ 0
triggered_out12
Wire Wire Line
	1750 2700 1550 2700
Wire Wire Line
	1750 2850 1600 2850
Wire Wire Line
	1600 2850 1600 2950
Wire Wire Line
	1600 2950 1550 2950
Wire Wire Line
	1750 3000 1600 3000
Wire Wire Line
	1600 3000 1600 3200
Wire Wire Line
	1600 3200 1550 3200
Wire Wire Line
	1750 3150 1650 3150
Wire Wire Line
	1650 3150 1650 3600
Wire Wire Line
	1650 3600 1550 3600
Wire Wire Line
	3350 2700 3600 2700
Wire Wire Line
	3350 2850 3550 2850
Wire Wire Line
	3550 2850 3550 2950
Wire Wire Line
	3550 2950 3600 2950
Wire Wire Line
	3350 3000 3550 3000
Wire Wire Line
	3550 3000 3550 3200
Wire Wire Line
	3550 3200 3600 3200
Wire Wire Line
	3350 3150 3500 3150
Wire Wire Line
	3500 3150 3500 3600
Wire Wire Line
	3500 3600 3600 3600
Wire Wire Line
	1750 3850 1550 3850
Wire Wire Line
	1750 4000 1600 4000
Wire Wire Line
	1600 4000 1600 4100
Wire Wire Line
	1600 4100 1550 4100
Wire Wire Line
	1750 4150 1600 4150
Wire Wire Line
	1600 4150 1600 4500
Wire Wire Line
	1600 4500 1550 4500
Wire Wire Line
	1750 4300 1650 4300
Wire Wire Line
	1650 4300 1650 4750
Wire Wire Line
	1650 4750 1550 4750
Wire Wire Line
	3600 3850 3400 3850
Wire Wire Line
	3400 4000 3550 4000
Wire Wire Line
	3550 4000 3550 4100
Wire Wire Line
	3550 4100 3600 4100
Wire Wire Line
	3400 4150 3550 4150
Wire Wire Line
	3550 4150 3550 4500
Wire Wire Line
	3550 4500 3600 4500
Wire Wire Line
	3400 4300 3500 4300
Wire Wire Line
	3500 4300 3500 4750
Wire Wire Line
	3500 4750 3600 4750
Wire Wire Line
	1750 5000 1550 5000
Wire Wire Line
	1750 5150 1600 5150
Wire Wire Line
	1600 5150 1600 5400
Wire Wire Line
	1600 5400 1550 5400
Wire Wire Line
	1750 5300 1650 5300
Wire Wire Line
	1650 5300 1650 5650
Wire Wire Line
	1650 5650 1550 5650
Wire Wire Line
	1750 5450 1700 5450
Wire Wire Line
	1700 5450 1700 5900
Wire Wire Line
	1700 5900 1550 5900
Wire Wire Line
	3400 5000 3600 5000
Wire Wire Line
	3400 5150 3550 5150
Wire Wire Line
	3550 5150 3550 5400
Wire Wire Line
	3550 5400 3600 5400
Wire Wire Line
	3400 5300 3500 5300
Wire Wire Line
	3500 5300 3500 5650
Wire Wire Line
	3500 5650 3600 5650
Wire Wire Line
	3400 5450 3450 5450
Wire Wire Line
	3450 5450 3450 5900
Wire Wire Line
	3450 5900 3600 5900
$EndSCHEMATC
