EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L bluepill_breakouts:BluePill_STM32F103C U101
U 1 1 5AFBE599
P 5650 3250
F 0 "U101" H 5200 1900 50  0000 C CNN
F 1 "BluePill_STM32F103C" H 5700 4100 50  0000 C CNN
F 2 "BluePill_breakouts:BluePill_STM32F103C" H 5700 1650 50  0001 C CNN
F 3 "www.rogerclark.net" H 5650 1750 50  0001 C CNN
	1    5650 3250
	-1   0    0    1   
$EndComp
NoConn ~ 4750 2750
NoConn ~ 5600 5150
NoConn ~ 5700 5150
$Comp
L lasik-rescue:R_Small R101
U 1 1 5AFBFC57
P 4600 2850
F 0 "R101" V 4700 2850 50  0000 L CNN
F 1 "1.8k" V 4700 3100 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4600 2850 50  0001 C CNN
F 3 "" H 4600 2850 50  0001 C CNN
	1    4600 2850
	0    -1   -1   0   
$EndComp
Text HLabel 6550 3550 2    60   Input ~ 0
pwm_out1
Text HLabel 6550 3450 2    60   Input ~ 0
pwm_out2
Text HLabel 6550 3350 2    60   Input ~ 0
pwm_out3
Text HLabel 6550 3250 2    60   Input ~ 0
pwm_out4
Text HLabel 6550 2950 2    60   Input ~ 0
pwm_out5
Text HLabel 6550 2850 2    60   Input ~ 0
pwm_out6
Text HLabel 6550 2750 2    60   Input ~ 0
pwm_out7
Text HLabel 6550 2650 2    60   Input ~ 0
pwm_out8
Text HLabel 4700 3650 0    60   Input ~ 0
pwm_out9
Text HLabel 4700 3550 0    60   Input ~ 0
pwm_out10
Text HLabel 4700 3450 0    60   Input ~ 0
pwm_out11
Text HLabel 4700 3350 0    60   Input ~ 0
pwm_out12
Text HLabel 4700 2050 0    60   Input ~ 0
5v_in1
Text HLabel 4700 2150 0    60   Input ~ 0
5v_in2
Text HLabel 4700 2250 0    60   Input ~ 0
5v_in3
Text HLabel 4700 2350 0    60   Input ~ 0
5v_in4
Text HLabel 4700 2450 0    60   Input ~ 0
5v_in5
Text HLabel 4700 2550 0    60   Input ~ 0
5v_in6
Text HLabel 4700 2650 0    60   Input ~ 0
5v_in7
Text HLabel 4700 2950 0    60   Input ~ 0
5v_in8
Text HLabel 4700 3050 0    60   Input ~ 0
5v_in9
Text HLabel 4700 3150 0    60   Input ~ 0
5v_in10
Text HLabel 6550 2450 2    60   Input ~ 0
5v_in11
Text HLabel 6550 2550 2    60   Input ~ 0
5v_in12
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFC1EF3
P 4650 3850
F 0 "#PWR?" H 4650 3650 50  0001 C CNN
F 1 "GNDPWR" V 4650 3600 50  0000 C CNN
F 2 "" H 4650 3800 50  0000 C CNN
F 3 "" H 4650 3800 50  0000 C CNN
	1    4650 3850
	0    1    1    0   
$EndComp
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFC21ED
P 6550 2050
F 0 "#PWR?" H 6550 1850 50  0001 C CNN
F 1 "GNDPWR" V 6450 1900 50  0000 C CNN
F 2 "" H 6550 2000 50  0000 C CNN
F 3 "" H 6550 2000 50  0000 C CNN
	1    6550 2050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5AFC264A
P 6550 2250
F 0 "#PWR?" H 6550 2100 50  0001 C CNN
F 1 "+3.3V" V 6550 2500 50  0000 C CNN
F 2 "" H 6550 2250 50  0000 C CNN
F 3 "" H 6550 2250 50  0000 C CNN
	1    6550 2250
	0    1    1    0   
$EndComp
Text HLabel 6550 3150 2    60   Input ~ 0
3v_io1
Text HLabel 6550 3050 2    60   Input ~ 0
3v_io2
Text HLabel 6550 3850 2    60   Input ~ 0
bp_led
Text HLabel 6550 3750 2    60   Input ~ 0
3v_nosource_io1
Text HLabel 6550 3650 2    60   Input ~ 0
3v_nosource_io2
Text HLabel 4700 3250 0    60   Input ~ 0
3v_io3
NoConn ~ 6500 3950
$Comp
L power:+5V #PWR?
U 1 1 5AFC467F
P 4700 3750
F 0 "#PWR?" H 4700 3600 50  0001 C CNN
F 1 "+5V" V 4700 3950 50  0000 C CNN
F 2 "" H 4700 3750 50  0000 C CNN
F 3 "" H 4700 3750 50  0000 C CNN
	1    4700 3750
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5AFC4BD1
P 4450 2850
F 0 "#PWR?" H 4450 2700 50  0001 C CNN
F 1 "+3.3V" V 4450 3100 50  0000 C CNN
F 2 "" H 4450 2850 50  0000 C CNN
F 3 "" H 4450 2850 50  0000 C CNN
	1    4450 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5AFC50B4
P 4700 3950
F 0 "#PWR?" H 4700 3800 50  0001 C CNN
F 1 "+3.3V" V 4650 3800 50  0000 C CNN
F 2 "" H 4700 3950 50  0000 C CNN
F 3 "" H 4700 3950 50  0000 C CNN
	1    4700 3950
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:CONN_01X12 P104
U 1 1 5AFC6A99
P 11050 2700
F 0 "P104" H 11050 3350 50  0000 C CNN
F 1 "CONN_01X12" V 11150 2700 50  0000 C CNN
F 2 "Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_12pol" H 11050 2700 50  0001 C CNN
F 3 "" H 11050 2700 50  0000 C CNN
	1    11050 2700
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:CONN_01X12 P105
U 1 1 5AFC6B62
P 11050 4000
F 0 "P105" H 11050 4650 50  0000 C CNN
F 1 "CONN_01X12" V 11150 4000 50  0000 C CNN
F 2 "Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_12pol" H 11050 4000 50  0001 C CNN
F 3 "" H 11050 4000 50  0000 C CNN
	1    11050 4000
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:CONN_01X12 P101
U 1 1 5AFC724C
P 650 2700
F 0 "P101" H 650 3350 50  0000 C CNN
F 1 "CONN_01X12" V 750 2700 50  0000 C CNN
F 2 "Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_12pol" H 650 2700 50  0001 C CNN
F 3 "" H 650 2700 50  0000 C CNN
	1    650  2700
	-1   0    0    -1  
$EndComp
$Comp
L lasik-rescue:CONN_01X12 P102
U 1 1 5AFC7252
P 650 4000
F 0 "P102" H 650 4650 50  0000 C CNN
F 1 "CONN_01X12" V 750 4000 50  0000 C CNN
F 2 "Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_12pol" H 650 4000 50  0001 C CNN
F 3 "" H 650 4000 50  0000 C CNN
	1    650  4000
	-1   0    0    -1  
$EndComp
$Sheet
S 1000 1900 1550 2900
U 5AFCE970
F0 "sensor_frontend12" 60
F1 "sensor_frontend12.sch" 60
F2 "+sensor1" I L 1000 2150 60 
F3 "-sensor1" I L 1000 2250 60 
F4 "+sensor2" I L 1000 2350 60 
F5 "-sensor2" I L 1000 2450 60 
F6 "+sensor3" I L 1000 2550 60 
F7 "-sensor3" I L 1000 2650 60 
F8 "+sensor4" I L 1000 2750 60 
F9 "-sensor4" I L 1000 2850 60 
F10 "+sensor5" I L 1000 2950 60 
F11 "-sensor5" I L 1000 3050 60 
F12 "+sensor6" I L 1000 3150 60 
F13 "-sensor6" I L 1000 3250 60 
F14 "+sensor7" I L 1000 3450 60 
F15 "-sensor7" I L 1000 3550 60 
F16 "+sensor8" I L 1000 3650 60 
F17 "-sensor8" I L 1000 3750 60 
F18 "+sensor9" I L 1000 3850 60 
F19 "-sensor9" I L 1000 3950 60 
F20 "+sensor10" I L 1000 4050 60 
F21 "-sensor10" I L 1000 4150 60 
F22 "+sensor11" I L 1000 4250 60 
F23 "-sensor11" I L 1000 4350 60 
F24 "+sensor12" I L 1000 4450 60 
F25 "-sensor12" I L 1000 4550 60 
F26 "triggered_out1" I R 2550 2200 60 
F27 "triggered_out2" I R 2550 2400 60 
F28 "triggered_out3" I R 2550 2600 60 
F29 "triggered_out4" I R 2550 2800 60 
F30 "triggered_out5" I R 2550 3000 60 
F31 "triggered_out6" I R 2550 3200 60 
F32 "triggered_out7" I R 2550 3500 60 
F33 "triggered_out8" I R 2550 3700 60 
F34 "triggered_out9" I R 2550 3900 60 
F35 "triggered_out10" I R 2550 4100 60 
F36 "triggered_out11" I R 2550 4300 60 
F37 "triggered_out12" I R 2550 4500 60 
$EndSheet
$Sheet
S 9550 1850 1150 3150
U 5AFCF4A2
F0 "laser_drivers12" 60
F1 "laser_drivers12.sch" 60
F2 "+laser1" I R 10700 3250 60 
F3 "-laser1" I R 10700 3150 60 
F4 "+laser2" I R 10700 3050 60 
F5 "-laser2" I R 10700 2950 60 
F6 "+laser3" I R 10700 2850 60 
F7 "-laser3" I R 10700 2750 60 
F8 "+laser4" I R 10700 2650 60 
F9 "-laser4" I R 10700 2550 60 
F10 "+laser5" I R 10700 2450 60 
F11 "-laser5" I R 10700 2350 60 
F12 "+laser6" I R 10700 2250 60 
F13 "-laser6" I R 10700 2150 60 
F14 "+laser7" I R 10700 4550 60 
F15 "-laser7" I R 10700 4450 60 
F16 "+laser8" I R 10700 4350 60 
F17 "-laser8" I R 10700 4250 60 
F18 "+laser9" I R 10700 4150 60 
F19 "-laser9" I R 10700 4050 60 
F20 "+laser10" I R 10700 3950 60 
F21 "-laser10" I R 10700 3850 60 
F22 "+laser11" I R 10700 3750 60 
F23 "-laser11" I R 10700 3650 60 
F24 "+laser12" I R 10700 3550 60 
F25 "-laser12" I R 10700 3450 60 
F26 "pwm_out1" I L 9550 2200 60 
F27 "pwm_out2" I L 9550 2400 60 
F28 "pwm_out3" I L 9550 2600 60 
F29 "pwm_out4" I L 9550 2800 60 
F30 "pwm_out5" I L 9550 3000 60 
F31 "pwm_out6" I L 9550 3200 60 
F32 "pwm_out7" I L 9550 3500 60 
F33 "pwm_out8" I L 9550 3700 60 
F34 "pwm_out9" I L 9550 3900 60 
F35 "pwm_out10" I L 9550 4100 60 
F36 "pwm_out11" I L 9550 4300 60 
F37 "pwm_out12" I L 9550 4500 60 
$EndSheet
Text HLabel 2700 2800 2    60   Input ~ 0
5v_in1
Text HLabel 2700 2600 2    60   Input ~ 0
5v_in2
Text HLabel 2700 2200 2    60   Input ~ 0
5v_in3
Text HLabel 2700 2400 2    60   Input ~ 0
5v_in4
Text HLabel 2700 3000 2    60   Input ~ 0
5v_in5
Text HLabel 2700 3200 2    60   Input ~ 0
5v_in6
Text HLabel 2700 4300 2    60   Input ~ 0
5v_in7
Text HLabel 2700 4100 2    60   Input ~ 0
5v_in8
Text HLabel 2700 4500 2    60   Input ~ 0
5v_in9
Text HLabel 2700 3900 2    60   Input ~ 0
5v_in10
Text HLabel 2700 3700 2    60   Input ~ 0
5v_in11
Text HLabel 2700 3500 2    60   Input ~ 0
5v_in12
Text HLabel 9350 4300 0    60   Input ~ 0
pwm_out1
Text HLabel 9350 3900 0    60   Input ~ 0
pwm_out2
Text HLabel 9350 3500 0    60   Input ~ 0
pwm_out3
Text HLabel 9350 3000 0    60   Input ~ 0
pwm_out4
Text HLabel 9350 2800 0    60   Input ~ 0
pwm_out5
Text HLabel 9350 2600 0    60   Input ~ 0
pwm_out6
Text HLabel 9350 2400 0    60   Input ~ 0
pwm_out7
Text HLabel 9350 2200 0    60   Input ~ 0
pwm_out8
Text HLabel 9350 4500 0    60   Input ~ 0
pwm_out9
Text HLabel 9350 4100 0    60   Input ~ 0
pwm_out10
Text HLabel 9350 3700 0    60   Input ~ 0
pwm_out11
Text HLabel 9350 3200 0    60   Input ~ 0
pwm_out12
Text GLabel 6550 2350 2    60   Input ~ 0
stm32_reset_debug
$Sheet
S 5050 600  1150 600 
U 5AFF8C1E
F0 "debug_central" 60
F1 "debug_central.sch" 60
$EndSheet
$Comp
L power:+12V #PWR?
U 1 1 5AFF9E81
P 8150 1000
F 0 "#PWR?" H 8150 850 50  0001 C CNN
F 1 "+12V" H 8100 1150 50  0000 C CNN
F 2 "" H 8150 1000 50  0000 C CNN
F 3 "" H 8150 1000 50  0000 C CNN
	1    8150 1000
	-1   0    0    1   
$EndComp
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFFA053
P 7650 1000
F 0 "#PWR?" H 7650 800 50  0001 C CNN
F 1 "GNDPWR" H 7550 850 50  0000 C CNN
F 2 "" H 7650 950 50  0000 C CNN
F 3 "" H 7650 950 50  0000 C CNN
	1    7650 1000
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:CONN_01X02 P103
U 1 1 5AFFDA7F
P 7900 800
F 0 "P103" V 7900 1000 50  0000 C CNN
F 1 "CONN_01X02" V 8000 800 50  0000 C CNN
F 2 "Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_2pol" H 7900 800 50  0001 C CNN
F 3 "" H 7900 800 50  0000 C CNN
	1    7900 800 
	0    1    -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C101
U 1 1 5B004131
P 7900 1100
F 0 "C101" V 7700 1150 50  0000 L CNN
F 1 "22u" V 7800 950 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 7900 1100 50  0001 C CNN
F 3 "" H 7900 1100 50  0001 C CNN
	1    7900 1100
	0    -1   -1   0   
$EndComp
Connection ~ 8000 1000
Wire Wire Line
	8000 1100 8000 1000
Connection ~ 7800 1000
Wire Wire Line
	7800 1100 7800 1000
Wire Wire Line
	7650 1000 7800 1000
Wire Wire Line
	7950 1000 8000 1000
Wire Wire Line
	9350 4500 9550 4500
Wire Wire Line
	9350 4300 9550 4300
Wire Wire Line
	9350 4100 9550 4100
Wire Wire Line
	9350 3900 9550 3900
Wire Wire Line
	9350 3700 9550 3700
Wire Wire Line
	9350 3500 9550 3500
Wire Wire Line
	9350 3200 9550 3200
Wire Wire Line
	9350 3000 9550 3000
Wire Wire Line
	9350 2800 9550 2800
Wire Wire Line
	9350 2600 9550 2600
Wire Wire Line
	9350 2400 9550 2400
Wire Wire Line
	9350 2200 9550 2200
Wire Wire Line
	2550 2200 2700 2200
Wire Wire Line
	2550 2400 2700 2400
Wire Wire Line
	2550 2600 2700 2600
Wire Wire Line
	2700 2800 2550 2800
Wire Wire Line
	2550 3000 2700 3000
Wire Wire Line
	2700 3200 2550 3200
Wire Wire Line
	2550 3500 2700 3500
Wire Wire Line
	2700 3700 2550 3700
Wire Wire Line
	2700 3900 2550 3900
Wire Wire Line
	2550 4100 2700 4100
Wire Wire Line
	2700 4300 2550 4300
Wire Wire Line
	2700 4500 2550 4500
Wire Wire Line
	10700 2150 10850 2150
Wire Wire Line
	10700 2250 10850 2250
Wire Wire Line
	10700 2350 10850 2350
Wire Wire Line
	10700 2450 10850 2450
Wire Wire Line
	10700 2550 10850 2550
Wire Wire Line
	10700 2650 10850 2650
Wire Wire Line
	10700 2750 10850 2750
Wire Wire Line
	10700 2850 10850 2850
Wire Wire Line
	10700 2950 10850 2950
Wire Wire Line
	10700 3050 10850 3050
Wire Wire Line
	10700 3150 10850 3150
Wire Wire Line
	10700 3250 10850 3250
Wire Wire Line
	10700 3450 10850 3450
Wire Wire Line
	10700 3550 10850 3550
Wire Wire Line
	10700 3650 10850 3650
Wire Wire Line
	10700 3750 10850 3750
Wire Wire Line
	10700 3850 10850 3850
Wire Wire Line
	10700 3950 10850 3950
Wire Wire Line
	10700 4050 10850 4050
Wire Wire Line
	10700 4150 10850 4150
Wire Wire Line
	10850 4250 10700 4250
Wire Wire Line
	10700 4350 10850 4350
Wire Wire Line
	10850 4450 10700 4450
Wire Wire Line
	10700 4550 10850 4550
Wire Wire Line
	850  4550 1000 4550
Wire Wire Line
	850  4450 1000 4450
Wire Wire Line
	850  4350 1000 4350
Wire Wire Line
	850  4250 1000 4250
Wire Wire Line
	850  4150 1000 4150
Wire Wire Line
	850  4050 1000 4050
Wire Wire Line
	850  3950 1000 3950
Wire Wire Line
	850  3850 1000 3850
Wire Wire Line
	850  3750 1000 3750
Wire Wire Line
	1000 3650 850  3650
Wire Wire Line
	850  3550 1000 3550
Wire Wire Line
	850  3450 1000 3450
Wire Wire Line
	850  3250 1000 3250
Wire Wire Line
	1000 3150 850  3150
Wire Wire Line
	850  3050 1000 3050
Wire Wire Line
	1000 2950 850  2950
Wire Wire Line
	850  2850 1000 2850
Wire Wire Line
	1000 2750 850  2750
Wire Wire Line
	850  2650 1000 2650
Wire Wire Line
	1000 2550 850  2550
Wire Wire Line
	850  2450 1000 2450
Wire Wire Line
	850  2350 1000 2350
Wire Wire Line
	850  2250 1000 2250
Wire Wire Line
	850  2150 1000 2150
Wire Wire Line
	4750 3950 4700 3950
Wire Wire Line
	4500 2850 4450 2850
Wire Wire Line
	4700 3750 4750 3750
Wire Wire Line
	6550 2350 6500 2350
Wire Wire Line
	6500 2450 6550 2450
Wire Wire Line
	6550 2550 6500 2550
Wire Wire Line
	6500 2650 6550 2650
Wire Wire Line
	6550 2750 6500 2750
Wire Wire Line
	6500 2850 6550 2850
Wire Wire Line
	6550 2950 6500 2950
Wire Wire Line
	6500 3050 6550 3050
Wire Wire Line
	6550 3150 6500 3150
Wire Wire Line
	6500 3250 6550 3250
Wire Wire Line
	6550 3350 6500 3350
Wire Wire Line
	6500 3450 6550 3450
Wire Wire Line
	6550 3550 6500 3550
Wire Wire Line
	6500 3650 6550 3650
Wire Wire Line
	6550 3750 6500 3750
Wire Wire Line
	6500 3850 6550 3850
Wire Wire Line
	4750 3650 4700 3650
Wire Wire Line
	4750 3550 4700 3550
Wire Wire Line
	4700 3450 4750 3450
Wire Wire Line
	4750 3350 4700 3350
Wire Wire Line
	4700 3250 4750 3250
Wire Wire Line
	4750 3150 4700 3150
Wire Wire Line
	4700 3050 4750 3050
Wire Wire Line
	4750 2950 4700 2950
Wire Wire Line
	4700 2650 4750 2650
Wire Wire Line
	4750 2550 4700 2550
Wire Wire Line
	4700 2450 4750 2450
Wire Wire Line
	4750 2350 4700 2350
Wire Wire Line
	4700 2250 4750 2250
Wire Wire Line
	4750 2150 4700 2150
Wire Wire Line
	4700 2050 4750 2050
Wire Wire Line
	6550 2250 6500 2250
Connection ~ 6500 2050
Wire Wire Line
	6550 2050 6500 2050
Wire Wire Line
	6500 2050 6500 2150
Wire Wire Line
	4650 3850 4750 3850
Wire Wire Line
	4700 2850 4750 2850
Wire Wire Line
	8000 1000 8150 1000
Wire Wire Line
	7800 1000 7850 1000
$EndSCHEMATC
