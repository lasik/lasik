EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 37 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lasik-rescue:LM339 U3601
U 2 1 5B0025EC
P 5400 3150
F 0 "U3601" H 5500 3400 50  0000 C CNN
F 1 "TS3704CDT" H 5550 2950 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5350 3250 50  0001 C CNN
F 3 "https://eu.mouser.com/datasheet/2/389/ts3704-957321.pdf" H 5450 3350 50  0001 C CNN
	2    5400 3150
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:R_Small R3702
U 1 1 5B0025ED
P 5000 3150
F 0 "R3702" H 5150 3200 50  0000 L CNN
F 1 "4.7k" H 5050 3300 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5000 3150 50  0001 C CNN
F 3 "" H 5000 3150 50  0001 C CNN
	1    5000 3150
	-1   0    0    1   
$EndComp
$Comp
L lasik-rescue:R_Small R3704
U 1 1 5B0025EE
P 5600 2500
F 0 "R3704" V 5700 2450 50  0000 L CNN
F 1 "470k" V 5700 2750 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5600 2500 50  0001 C CNN
F 3 "" H 5600 2500 50  0001 C CNN
	1    5600 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 3250 5000 3250
Wire Wire Line
	5000 3050 5000 3050
Connection ~ 5000 3050
Connection ~ 5000 3250
Wire Wire Line
	5750 2500 5700 2500
$Comp
L power:+3.3V #PWR?
U 1 1 5B0025F3
P 5300 2700
F 0 "#PWR?" H 5300 2550 50  0001 C CNN
F 1 "+3.3V" H 5300 2840 50  0000 C CNN
F 2 "" H 5300 2700 50  0000 C CNN
F 3 "" H 5300 2700 50  0000 C CNN
	1    5300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2700 5300 2850
Text HLabel 4800 3050 0    60   Input ~ 0
amped_sense
Text HLabel 5900 3150 2    60   Input ~ 0
triggered_out
Wire Wire Line
	5700 3150 5900 3150
Connection ~ 5750 3150
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFF0A76
P 5300 3750
F 0 "#PWR?" H 5300 3550 50  0001 C CNN
F 1 "GNDPWR" H 5300 3620 50  0000 C CNN
F 2 "" H 5300 3700 50  0000 C CNN
F 3 "" H 5300 3700 50  0000 C CNN
	1    5300 3750
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:R_Small R3703
U 1 1 5AFF0A7C
P 5150 3550
F 0 "R3703" V 4950 3350 50  0000 L CNN
F 1 "10k" V 5050 3350 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5150 3550 50  0001 C CNN
F 3 "" H 5150 3550 50  0001 C CNN
	1    5150 3550
	0    1    -1   0   
$EndComp
Wire Wire Line
	5000 3250 5000 3550
Wire Wire Line
	4950 3550 5050 3550
Connection ~ 5000 3550
Wire Wire Line
	5300 3450 5300 3750
Wire Wire Line
	5250 3550 5300 3550
Connection ~ 5300 3550
$Comp
L lasik-rescue:R_Small R3701
U 1 1 5AFF0A89
P 4850 3550
F 0 "R3701" V 4650 3250 50  0000 L CNN
F 1 "10k" V 4750 3250 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4850 3550 50  0001 C CNN
F 3 "" H 4850 3550 50  0001 C CNN
	1    4850 3550
	0    1    -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5AFF0A90
P 4700 3550
F 0 "#PWR?" H 4700 3400 50  0001 C CNN
F 1 "+3.3V" H 4700 3690 50  0000 C CNN
F 2 "" H 4700 3550 50  0000 C CNN
F 3 "" H 4700 3550 50  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3550 4750 3550
Wire Wire Line
	4800 2500 5500 2500
Wire Wire Line
	5750 2250 5750 3150
Text GLabel 4800 2500 0    60   Input ~ 0
otto_RC_debug
Text GLabel 4800 2250 0    60   Input ~ 0
otto_triggered_debug
Connection ~ 5750 2500
Connection ~ 5000 2500
Wire Wire Line
	5000 2500 5000 3050
Wire Wire Line
	4800 3050 5100 3050
Wire Wire Line
	4800 2250 5750 2250
$EndSCHEMATC
